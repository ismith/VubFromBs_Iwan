#include <iostream>

#include "TFile.h"
#include "TH1F.h"
#include "THStack.h"

#include "TCanvas.h"

#include "TFractionFitter.h"




void FractionFitter(){
	
	
	TFile* f_in = TFile::Open("Histos.root");
	
	TH1F* h_Data	= (TH1F*)f_in->Get("Data");
	TH1F* h_SS		= (TH1F*)f_in->Get("SameSign");
	TH1F* h_Ds		= (TH1F*)f_in->Get("Ds");
	TH1F* h_DsStar	= (TH1F*)f_in->Get("DsStar");
	
	TObjArray *mc = new TObjArray(3);
	mc->Add(h_SS);
	mc->Add(h_Ds);
	mc->Add(h_DsStar);
	
	TFractionFitter* fit = new TFractionFitter(h_Data, mc);
	fit->Constrain(0, 0.0, 1.0); 
	fit->Constrain(1, 0.0, 1.0); 
	fit->Constrain(2, 0.0, 1.0);
	
	Int_t status = fit->Fit();
	
	std::cout << "fit status: " << status << std::endl;
	
	TH1F* result = (TH1F*) fit->GetPlot();

	
	TH1F* h_sol_SS		= (TH1F*)fit->GetMCPrediction(0);
	TH1F* h_sol_Ds		= (TH1F*)fit->GetMCPrediction(1);
	TH1F* h_sol_DsStar	= (TH1F*)fit->GetMCPrediction(2);
	
	double res_SS, res_SS_err;
	double res_Ds, res_Ds_err;
	double res_DsStar, res_DsStar_err;
	
	fit->GetResult(0, res_SS, res_SS_err);
	fit->GetResult(1, res_Ds, res_Ds_err);
	fit->GetResult(2, res_DsStar, res_DsStar_err);
	
	double TotalMCEvents = res_SS * h_sol_SS->Integral() + res_Ds * h_sol_Ds->Integral() + res_DsStar * h_sol_DsStar->Integral();
	double TotalDatEvents= result->Integral();
	
	h_sol_SS->Scale( res_SS * TotalDatEvents / TotalMCEvents );
	h_sol_Ds->Scale( res_Ds * TotalDatEvents / TotalMCEvents );
	h_sol_DsStar->Scale( res_DsStar * TotalDatEvents / TotalMCEvents );
	
	h_sol_SS->Sumw2(false);
	h_sol_Ds->Sumw2(false);
	h_sol_DsStar->Sumw2(false);
	
	h_sol_SS->SetFillColor(1);
	h_sol_Ds->SetFillColor(2);
	h_sol_DsStar->SetFillColor(3);
	
	THStack *hs = new THStack("hs","Stacked 1D histograms");
	hs->Add( h_sol_SS );
	hs->Add( h_sol_Ds );
	hs->Add( h_sol_DsStar );
	
	hs->Draw();
	
	result->Draw("Ep same");

	
}
