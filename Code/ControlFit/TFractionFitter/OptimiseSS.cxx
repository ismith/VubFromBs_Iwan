#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1.h"
#include "TH2F.h"
#include "TCanvas.h"

#include "RooDataHist.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooPolynomial.h"
#include "RooGaussian.h"

void FitSS(TH1*, double*, double*,double*);


void OptimiseSS(){
	
	TFile* F_dat = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012SS_trimmed_norm.root");
	TTree* T_dat = (TTree*)F_dat->Get("reducedTree");
		
	double PIDK_l = -5;  double PIDK_h = 40;   double PIDK_s = 1;
	double PIDpi_l = 20; double PIDpi_h = -20; double PIDpi_s = -1;
		
		
	int n_K = 0;
	int n_pi = 0;
	double a_n_sig[100][100];
	double a_n_bg[100][100];
	double a_n_srb[100][100];
	
	
	for (double PIDK = PIDK_l; PIDK < PIDK_h; PIDK += PIDK_s){
		n_pi = 0;
		for (double PIDpi = PIDpi_l; PIDpi > PIDpi_h; PIDpi += PIDpi_s){
			std::string cut_str = "kaon_m_PIDK > " + std::to_string(PIDK) + " && K_p_PIDK > " + std::to_string(PIDK) + " && pi_p_PIDK < " + std::to_string(PIDpi);
			
			std::cout << cut_str << std::endl;

			TH1* h_Ds_M = new TH1F(("h_Ds_M" + to_string(n_pi) + "_" + to_string(n_K)).c_str(), "h_Ds_M", 200, 1968.3-25, 1968.3+25);
			T_dat->Draw(("Ds_M>>h_Ds_M" + to_string(n_pi) + "_" + to_string(n_K)).c_str(), cut_str.c_str() );
			
			FitSS(h_Ds_M, &a_n_sig[n_K][n_pi], &a_n_bg[n_K][n_pi], &a_n_srb[n_K][n_pi]);		
					
			//delete h_Ds_M;
			n_pi++;
		}
		n_K++;

	}
	
	TH2F* h_optim = new TH2F("h_optim", "h_optim", n_K, PIDK_l, PIDK_h, n_pi, PIDpi_h, PIDpi_l);
	
	n_K = 0;
	for (double PIDK = PIDK_l; PIDK < PIDK_h; PIDK += PIDK_s){
		n_pi = 0;
		for (double PIDpi = PIDpi_l; PIDpi > PIDpi_h; PIDpi += PIDpi_s){
			std::cout << PIDK << ":" << PIDpi << ": " << a_n_srb[n_K][n_pi] << "\t- ";
			
			h_optim->SetBinContent(n_K+1, n_pi+1, a_n_srb[n_K][n_pi]);
			
			n_pi++;
		}
		n_K++;
		std::cout << std::endl;
	}

	TCanvas* c1 = new TCanvas("c1", "c1", 1920, 1080);
	
	h_optim->Draw();
	
}


void FitSS(TH1* Hist, double* n_sig_out, double* n_bg_out, double* n_srb_out){
	
	
	
	double low_mass = 1968.3 - 25;
	double high_mass = 1968.3 + 25;  // needs to be lower than 2010 D* ->D0 pi D0->K K  resonance
	
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", low_mass, high_mass);
	
	RooDataHist* data = new RooDataHist("data", "data", *D_M, Hist);
	
	std::cout << Hist->Integral() << "  " << data->sumEntries();
	
	//Ds mass distribution	
	RooRealVar* D_DG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 1970, 1960, 1980);
	RooRealVar* D_DG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 5, 1, 9);
	RooRealVar* D_DG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 5, 1, 9);
	RooRealVar* D_DG_s3 = new RooRealVar("D_s3", "Ds Double Gauss sigma 3", 20, 9, 20);
	RooRealVar* D_DG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.8, 0.0, 1.0);
	RooRealVar* D_DG_f2 = new RooRealVar("D_f2", "Ds Double Gauss fraction 2", 0.1, 0.0, 1.0);
	
	RooGaussian* D_Gauss1_pdf = new RooGaussian("D_Gauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_DG_mu, *D_DG_s1);
	RooGaussian* D_Gauss2_pdf = new RooGaussian("D_Gauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s2);
	RooGaussian* D_Gauss3_pdf = new RooGaussian("D_Gauss3_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s3);

	RooAddPdf* D_DoubleGauss_pdf = new RooAddPdf("D_DoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Gauss1_pdf, *D_Gauss2_pdf), RooArgList( *D_DG_f1) );
	
	//Background distribution
	RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 2.75 ,2,  3.5 );
	RooFormulaVar* s_m_f2=  new RooFormulaVar("s_m_f2", "s_m_f2", "Ds_M * 2.75 - 4408", RooArgList(*D_M));
	
	RooRealVar* pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 231363, 200000,  300000 );
	RooRealVar* pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", -72.2, -100,  -50 );
	
	RooPolynomial* poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));
	
	RooRealVar* s_nev = new RooRealVar("s_nev", "s_nev", data->sumEntries()*0.17, 0, data->sumEntries());
	RooRealVar* b_nev = new RooRealVar("b_nev", "b_nev", data->sumEntries()*0.83, 0, data->sumEntries());

	
	RooRealVar* f_sig = new RooRealVar("f_sig", "f_sig", 0.2, 0, 1);
	RooFormulaVar* f_sigb = new RooFormulaVar("f_sigb", "f_sigb", "1-f_sig", RooArgList(*f_sig));

	RooAddPdf* data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_DoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
		
	data_dist->fitTo(*data);
	
	
	*n_sig_out = s_nev->getValV() ;
	*n_bg_out  = b_nev->getValV() ;
	*n_srb_out = *n_sig_out / sqrt( *n_sig_out + *n_bg_out );
}
