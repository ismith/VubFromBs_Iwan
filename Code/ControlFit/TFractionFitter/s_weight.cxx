#ifndef __IINT
#include "RooGlobalFunc.h"
#endif
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include <sstream>
#include "TH1F.h"
#include "TH1.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPolynomial.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooBinning.h"
#include "RooPlot.h"
#include "TLatex.h"
#include "TAxis.h"
#include "RooStats/SPlot.h"
#include "RooConstVar.h"
using namespace RooStats;
using namespace RooFit;

void HistFit(std::string filename = "/media/ismith/2.0TB_Irive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_Prescale0.01.root", std::string treename = "DecayTree"){
	
	TFile* File_MC = TFile::Open(filename.c_str(), "UPDATE");
	TTree* Tree_MC = (TTree*)File_MC->Get(treename.c_str());
	
	double low_mass = 2230;
	double high_mass = 2350;  // needs to be lower than 2010 D* ->D0 pi D0->K K  resonance
	
	TH1* h_Ds = new TH1F("h_Ds", "h_Ds", 200, low_mass, high_mass);
	Tree_MC->Draw("D_M>>h_Ds");
	

	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", low_mass, high_mass);
	
	RooDataHist* data = new RooDataHist("data", "Monte Carlo Data", RooArgSet(*D_M), h_Ds);

	int nev_I = Tree_MC->GetEntries();
	int nev = data->sumEntries();
	std::cout << "Number of entries:  " << nev  << "  " << nev_I << std::endl;
	
	
	//Ds mass distribution	
	RooRealVar* D_IG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 2288, 2280, 2295);
	RooRealVar* D_IG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 5, 1, 9);
	RooRealVar* D_IG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 6, 1, 9);
	RooRealVar* D_IG_s3 = new RooRealVar("D_s3", "Ds Double Gauss sigma 3", 7, 1, 9);
	RooRealVar* D_IG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.8, 0.0, 1.0);
	RooRealVar* D_IG_f2 = new RooRealVar("D_f2", "Ds Double Gauss fraction 2", 0.1, 0.0, 1.0);
	
	RooGaussian* D_Iauss1_pdf = new RooGaussian("D_Iauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_IG_mu, *D_IG_s1);
	RooGaussian* D_Iauss2_pdf = new RooGaussian("D_Iauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s2);
	RooGaussian* D_Iauss3_pdf = new RooGaussian("D_Iauss3_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s3);

	RooAddPdf* D_IoubleGauss_pdf = new RooAddPdf("D_IoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Iauss1_pdf, *D_Iauss2_pdf), RooArgList( *D_IG_f1) );
	
	//Background distribution
	
	RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 2.75 ,2,  3.5 );
	RooFormulaVar* s_m_f2=  new RooFormulaVar("s_m_f2", "s_m_f2", "Ds_M * 2.75 - 4408", RooArgList(*D_M));
	
	RooRealVar* pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 1000, -100000,  1000000 );
	RooRealVar* pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", 0, -100,  100 );
	
	RooPolynomial* poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));
	
	RooRealVar* s_nev = new RooRealVar("s_nev", "s_nev", data->sumEntries()*0.17, 0, data->sumEntries());
	RooRealVar* b_nev = new RooRealVar("b_nev", "b_nev", data->sumEntries()*0.83, 0, data->sumEntries());

	
	RooRealVar* f_sig = new RooRealVar("f_sig", "f_sig", 0.2, 0, 1);
	RooFormulaVar* f_sigb = new RooFormulaVar("f_sigb", "f_sigb", "1-f_sig", RooArgList(*f_sig));

	RooAddPdf* data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_IoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
		
	data_dist->fitTo(*data, NumCPU(8));
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1920, 1080);
	
	RooPlot* frame = D_M->frame(Title("Imported TH1 with Poisson error bars")) ;
	data->plotOn(frame);
	data_dist->plotOn(frame);
	
	frame->Draw();
	
	
}

void FitSig_Dat(std::string filename = "/media/ismith/2.0TB_Irive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_Prescale0.01.root", std::string treename = "DecayTree", bool save_plot = true, bool do_sweight = true){
	
	
	
	TFile* File_MC = TFile::Open(filename.c_str(), "UPDATE");
	TTree* Tree_MC = (TTree*)File_MC->Get(treename.c_str());
	
	double low_mass = 2230;
	double high_mass = 2350;  // needs to be lower than 2010 D* ->D0 pi D0->K K  resonance
	
	RooRealVar* D_M = new RooRealVar("D_M", "D Mass", low_mass, high_mass);
	
	RooDataSet* data = new RooDataSet("data", "Monte Carlo Data", RooArgSet(*D_M), Import(*Tree_MC));

	int nev_I = Tree_MC->GetEntries();
	int nev = data->numEntries();
	std::cout << "Number of entries:  " << nev  << "  " << nev_I << std::endl;
	
	
	//Ds mass distribution	
	RooRealVar* D_IG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 2288, 2280, 2295);
	RooRealVar* D_IG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 5, 1, 9);
	RooRealVar* D_IG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 10, 9, 20);
	RooRealVar* D_IG_s3 = new RooRealVar("D_s3", "Ds Double Gauss sigma 3", 20, 9, 20);
	RooRealVar* D_IG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.8, 0.0, 1.0);
	RooRealVar* D_IG_f2 = new RooRealVar("D_f2", "Ds Double Gauss fraction 2", 0.1, 0.0, 1.0);
	
	RooGaussian* D_Iauss1_pdf = new RooGaussian("D_Iauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_IG_mu, *D_IG_s1);
	RooGaussian* D_Iauss2_pdf = new RooGaussian("D_Iauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s2);
	RooGaussian* D_Iauss3_pdf = new RooGaussian("D_Iauss3_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s3);

	RooAddPdf* D_IoubleGauss_pdf = new RooAddPdf("D_IoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Iauss1_pdf, *D_Iauss2_pdf), RooArgList( *D_IG_f1) );
	
	//Background distribution
	
	//RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 10000 ,0,  10000000000000 );
	//RooPolynomial*  s_m_f2 = new RooPolynomial("b_m__exp", "b_m_exp", *D_M, *s_m_f1);
	RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 2.75 ,2,  3.5 );
	RooFormulaVar* s_m_f2=  new RooFormulaVar("s_m_f2", "s_m_f2", "Ds_M * 2.75 - 4408", RooArgList(*D_M));
	
	RooRealVar* pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 1000, -100000,  1000000 );
	RooRealVar* pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", 0, -100,  100 );
	
	RooPolynomial* poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));
	
	RooRealVar* s_nev = new RooRealVar("s_nev", "s_nev", data->numEntries()*0.17, 0, data->numEntries());
	RooRealVar* b_nev = new RooRealVar("b_nev", "b_nev", data->numEntries()*0.83, 0, data->numEntries());

	
	RooRealVar* f_sig = new RooRealVar("f_sig", "f_sig", 0.2, 0, 1);
	RooFormulaVar* f_sigb = new RooFormulaVar("f_sigb", "f_sigb", "1-f_sig", RooArgList(*f_sig));

	RooAddPdf* data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_IoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
		
	data_dist->fitTo(*data, NumCPU(8));
	
	TCanvas* c1 = new TCanvas("Dfit_I",  "Mass Fit to D and B", 3200, 1600);
	c1->Divide(1,2);
	
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe, Binning(100));
	data_dist->plotOn(Dframe, Components( *D_IoubleGauss_pdf ), LineColor(kRed));
	data_dist->plotOn(Dframe, Components( *poly ), LineStyle(kDashed));
	data_dist->plotOn(Dframe);

	RooPlot* Dframe2 = D_M->frame();
	Dframe2->addPlotable(Dframe->residHist(), "P")  ; 
	Dframe2->SetMinimum(-80);
	Dframe2->SetMaximum(+80);
		
	c1->cd(1);
	Dframe->Draw();
	c1->cd(2);
	Dframe2->Draw();
	if (save_plot )
		c1->Write();
	
	if (do_sweight){
		SPlot* sData = new SPlot("sData","An SPlot", *data, data_dist, RooArgList(*s_nev, *b_nev) );



		double Nsig_sw, Nbkg_sw, RF_Is_M; 
		TBranch*  b_Nsig_sw = Tree_MC->Branch("nsig_sw", &Nsig_sw,"nsig_sw/D");  
		TBranch*  b_Nbkg_sw  = Tree_MC->Branch("nback_sw", &Nbkg_sw,"nback_sw/D") ;

		double MC_Is_M; Tree_MC->SetBranchAddress("D_M", &MC_Is_M);
		int i_RF = 0;
		
		for (int i_MC = 0; i_MC < nev_I; ++i_MC) {
			
			Tree_MC->GetEntry(i_MC);
			Nsig_sw = Nbkg_sw = RF_Is_M = 0;

			const RooArgSet* row;
			if (i_RF < nev ){
				row = data->get(i_RF);
				RF_Is_M = row->getRealValue("D_M");
			
				Nsig_sw =  row->getRealValue("s_nev_sw");
				Nbkg_sw =  row->getRealValue("b_nev_sw");
			}
			
			if (MC_Is_M == RF_Is_M){
				i_RF++;
		}
			else{
				Nsig_sw = 0;
				Nbkg_sw = 0;
			}


			b_Nsig_sw->Fill();
			b_Nbkg_sw->Fill();

		}
	}
	Tree_MC->Write();
	File_MC->Close();  

}





void FitSig_SSDat(std::string filename = "/media/ismith/2.0TB_Irive/NTuples/BsKmuNu/Bs_KMuNu/data_2012SS_trimmed_norm.root", std::string treename = "reducedTree", bool save_plot = true, bool do_sweight = true){
	
	
	
	TFile* File_MC = TFile::Open(filename.c_str(), "UPDATE");
	TTree* Tree_MC = (TTree*)File_MC->Get(treename.c_str());
	
	
	double low_mass = 1968.3-26;
	double high_mass = 1968.3+26;  // needs to be lower than 2010 D* ->D0 pi D0->K K  resonance
	
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", low_mass, high_mass);
	
	RooDataSet* data = new RooDataSet("data", "Monte Carlo Data", RooArgSet(*D_M), Import(*Tree_MC));

	int nev_I = Tree_MC->GetEntries();
	int nev = data->numEntries();
	std::cout << "Number of entries:  " << nev  << "  " << nev_I << std::endl;
	
	
	//Ds mass distribution	
	RooRealVar* D_IG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 1970, 1960, 1980);
	RooRealVar* D_IG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 5, 1, 9);
	RooRealVar* D_IG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 5, 1, 9);
	RooRealVar* D_IG_s3 = new RooRealVar("D_s3", "Ds Double Gauss sigma 3", 20, 9, 20);
	RooRealVar* D_IG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.8, 0.0, 1.0);
	RooRealVar* D_IG_f2 = new RooRealVar("D_f2", "Ds Double Gauss fraction 2", 0.1, 0.0, 1.0);
	
	RooGaussian* D_Iauss1_pdf = new RooGaussian("D_Iauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_IG_mu, *D_IG_s1);
	RooGaussian* D_Iauss2_pdf = new RooGaussian("D_Iauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s2);
	RooGaussian* D_Iauss3_pdf = new RooGaussian("D_Iauss3_pdf", "Ds Gaussian 2 pdf", *D_M, *D_IG_mu, *D_IG_s3);

	RooAddPdf* D_IoubleGauss_pdf = new RooAddPdf("D_IoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Iauss1_pdf, *D_Iauss2_pdf), RooArgList( *D_IG_f1) );
	
	//Background distribution
	
	//RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 10000 ,0,  10000000000000 );
	//RooPolynomial*  s_m_f2 = new RooPolynomial("b_m__exp", "b_m_exp", *D_M, *s_m_f1);
	RooRealVar* s_m_f1 =  new RooRealVar("s_m_f1", "s_m_f1", 2.75 ,2,  3.5 );
	RooFormulaVar* s_m_f2=  new RooFormulaVar("s_m_f2", "s_m_f2", "Ds_M * 2.75 - 4408", RooArgList(*D_M));
	
	RooRealVar* pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 231363, 200000,  300000 );
	RooRealVar* pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", -72.2, -100,  -50 );
	
	RooPolynomial* poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));
	
	RooRealVar* s_nev = new RooRealVar("s_nev", "s_nev", data->numEntries()*0.17, 0, data->numEntries());
	RooRealVar* b_nev = new RooRealVar("b_nev", "b_nev", data->numEntries()*0.83, 0, data->numEntries());

	
	RooRealVar* f_sig = new RooRealVar("f_sig", "f_sig", 0.2, 0, 1);
	RooFormulaVar* f_sigb = new RooFormulaVar("f_sigb", "f_sigb", "1-f_sig", RooArgList(*f_sig));

	RooAddPdf* data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_IoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
		
	data_dist->fitTo(*data, NumCPU(8));
	
	TCanvas* c1 = new TCanvas("Dfit_I",  "Mass Fit to D and B", 3200, 1600);
	c1->Divide(1,2);
	
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe, Binning(100));
	data_dist->plotOn(Dframe, Components( *D_IoubleGauss_pdf ), LineColor(kRed));
	data_dist->plotOn(Dframe, Components( *poly ), LineStyle(kDashed));
	data_dist->plotOn(Dframe);

	RooPlot* Dframe2 = D_M->frame();
	Dframe2->addPlotable(Dframe->residHist(), "P")  ; 
	Dframe2->SetMinimum(-80);
	Dframe2->SetMaximum(+80);
		
	c1->cd(1);
	Dframe->Draw();
	c1->cd(2);
	Dframe2->Draw();
	
	std::cout << "Number of Entries: " << nev << " Number of Signal: " << s_nev->getValV() << " Number of Background: " << b_nev->getValV() << std::endl;
	
	if (save_plot )
		c1->Write();
	delete c1;
	
	if ( do_sweight ){
		SPlot* sData = new SPlot("sData","An SPlot", *data, data_dist, RooArgList(*s_nev, *b_nev) );



		double Nsig_sw, Nbkg_sw, RF_Is_M; 
		TBranch*  b_Nsig_sw = Tree_MC->Branch("nsig_sw_I", &Nsig_sw,"nsig_sw_I/D");  
		TBranch*  b_Nbkg_sw  = Tree_MC->Branch("nback_sw_I", &Nbkg_sw,"nback_sw_I/D") ;

		double MC_Is_M; Tree_MC->SetBranchAddress("Ds_M", &MC_Is_M);
		int i_RF = 0;
		
		for (int i_MC = 0; i_MC < nev_I; ++i_MC) {
			
			Tree_MC->GetEntry(i_MC);
			Nsig_sw = Nbkg_sw = RF_Is_M = 0;

			const RooArgSet* row;
			if (i_RF < nev ){
				row = data->get(i_RF);
				RF_Is_M = row->getRealValue("Ds_M");
			
				Nsig_sw =  row->getRealValue("s_nev_sw");
				Nbkg_sw =  row->getRealValue("b_nev_sw");
			}
			
			if (MC_Is_M == RF_Is_M){
				i_RF++;
		}
			else{
				Nsig_sw = 0;
				Nbkg_sw = 0;
			}


			b_Nsig_sw->Fill();
			b_Nbkg_sw->Fill();

		}

		Tree_MC->Write();
	}
	
	/*
	int n_bins = 40;
	double bin_low = 3000, bin_high = 6000;

	double n_sig = s_nev->getValV();

	TH1F* h_IM_sig  =  new TH1F("h_IM_sig", "h_IM_sig", n_bins, bin_low, bin_high);
	TH1F* h_IM_high =  new TH1F("h_IM_high","h_IM_high",n_bins, bin_low, bin_high);
	TH1F* h_IM_low  =  new TH1F("h_IM_low", "h_IM_low", n_bins, bin_low, bin_high);
	
	Tree_MC->Draw("Bs_MCORR>>h_IM_sig", "Ds_MM> 1954 && Ds_MM < 1982"); 
	Tree_MC->Draw("Bs_MCORR>>h_IM_high", "Ds_MM> 1982 && Ds_MM < 2004"); 
	Tree_MC->Draw("Bs_MCORR>>h_IM_low", "Ds_MM> 1932 && Ds_MM < 1954"); 
	
	TH1F* h_IM_SB = new TH1F("h_IM_SB", "h_IM_SB", n_bins, bin_low, bin_high);
	*h_IM_SB = *h_IM_high + *h_IM_low;
	
	double n_h_SB = h_IM_SB->Integral();
	double n_h_IG_S = h_IM_sig->Integral() - n_sig;
	
	h_IM_SB ->Scale( n_h_IG_S / n_h_SB );
	
	TH1F* h_IM_SIGmBG = new TH1F("h_IM_SIGmBG", "h_IM_SIGmBG", n_bins, bin_low, bin_high);
	*h_IM_SIGmBG = *h_IM_sig - *h_IM_SB;
	
	std::cout << "Number of events in Histogram: " << h_IM_SIGmBG->Integral() << std::endl;
	
	TH1F* h_Sig_s = new TH1F("h_Sig_s", "h_Sig_s", n_bins, bin_low, bin_high);
	Tree_MC->Draw("Bs_MCORR>>h_Sig_s", "nsig_sw");
	
	
	TCanvas* c2 = new TCanvas("c2", "c2", 1600, 1600);
	h_IM_SIGmBG->Draw();
	h_Sig_s->Draw("SAME");
	c1->Update();
	//Now try a simple Histogram subtraction for combinatorial.
	
	*/
	File_MC->Close();  
	
}


