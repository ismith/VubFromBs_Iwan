#ifndef __IINT
#include "RooGlobalFunc.h"
#endif
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1.h"
#include "TH1F.h"

#include "TCanvas.h"

#include "RooDataHist.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooPolynomial.h"
#include "RooGaussian.h"
#include "RooPlot.h"
void FitSS(TH1*, double*, double*, TCanvas*, bool)

void DsYield(){
	
	TFile* F_dat = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_Tight.root");
	TTree* T_dat = (TTree*)F_dat->Get("DecayTree");
	
	
	int n_bins = 40;
	double b_low = 3000;
	double b_high = 6000;
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	int ncol = int (sqrt(n_bins) + 0.9999999 );
	
	if (ncol * (ncol-1 ) >= n_bins )
		c1->Divide(ncol, ncol-1 );
	else
		c1->Divide(ncol, ncol );
	
		TCanvas* c2 = new TCanvas("c2", "c2", 1600, 1600);
		TH1F* h_Ds = new TH1F("h_Ds", "h_Ds", 100, 1920, 1995);
		T_dat->Draw("Ds_M>>h_Ds");
		delete c2;
		
		double n_sig, n_err;
		
		FitSS(h_Ds, &n_sig, &n_err, c1, true);

		return;
	
	
	
	TH1F* h_BMCORR = new TH1F("h_BMCORR", "Ds yield Vs BMCORR", n_bins, b_low, b_high);
	h_BMCORR->Sumw2();
	
	for ( int bin = 0; bin < n_bins; bin++ ){
		double bb_low =  b_low + double( bin     ) / double( n_bins ) * ( b_high - b_low );
		double bb_high = b_low + double( bin + 1 ) / double( n_bins ) * ( b_high - b_low );
		
		std::string cut_string = "Bs_MCORR > " + std::to_string( bb_low ) + " && Bs_MCORR < " + std::to_string( bb_high );
		std::cout << cut_string << std::endl;
		TCanvas* c2 = new TCanvas("c2", "c2", 1600, 1600);
		TH1F* h_Ds = new TH1F("h_Ds", "h_Ds", 100, 1920, 1995);
		T_dat->Draw("Ds_M>>h_Ds", cut_string.c_str() );
		delete c2;
		
		double n_sig, n_err;
		
		c1->cd(bin+1);
		FitSS(h_Ds, &n_sig, &n_err, c1, true);

		h_BMCORR->SetBinContent( bin+1, n_sig);
		h_BMCORR->SetBinError(   bin+1, n_err);
		
		std::cout << "********************************\n\n\n" << bin+1 << "    " << n_sig << "    " << n_err<< "\n\n\n********************************\n";
		delete h_Ds;
		
		
	}	
	
}




void FitSS(TH1* Hist, double* n_sig_out, double* n_sig_err, TCanvas* c1, bool fixed){
	
	
	
	double low_mass = 1920;
	double high_mass = 1995;  // needs to be lower than 2010 D* ->D0 pi D0->K K  resonance
	
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", low_mass, high_mass);
	
	RooDataHist* data = new RooDataHist("data", "data", *D_M, Hist);
	
	std::cout << Hist->Integral() << "  " << data->sumEntries();
	
	//Ds mass distribution	
	RooRealVar* D_DG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 1969.72, 1965.3, 1977.3);
	
	RooRealVar* D_DG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 10.7097, 1, 13);
	RooRealVar* D_DG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 5.51790, 1, 9);
	
	RooGaussian* D_Gauss1_pdf = new RooGaussian("D_Gauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_DG_mu, *D_DG_s1);
	RooGaussian* D_Gauss2_pdf = new RooGaussian("D_Gauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s2);
	
	RooRealVar* D_DG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.237746, 0.0, 1.0);
	RooAddPdf* D_DoubleGauss_pdf = new RooAddPdf("D_DoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Gauss1_pdf, *D_Gauss2_pdf), RooArgList( *D_DG_f1) );

	
	RooRealVar* pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 11200, -1000000, 1000000 );
	RooRealVar* pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", 0, -100, 100 );
	
	RooPolynomial* poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));
	
	RooRealVar* s_nev = new RooRealVar("s_nev", "s_nev", data->sumEntries()*0.17, 0, data->sumEntries());
	RooRealVar* b_nev = new RooRealVar("b_nev", "b_nev", data->sumEntries()*0.83, 0, data->sumEntries());

	
	RooRealVar* f_sig = new RooRealVar("f_sig", "f_sig", 0.2, 0, 1);
	RooFormulaVar* f_sigb = new RooFormulaVar("f_sigb", "f_sigb", "1-f_sig", RooArgList(*f_sig));

	RooAddPdf* data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_DoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
		
	data_dist->fitTo(*data);
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe);
	//data_dist->plotOn(Dframe, Components( *D_DoubleGauss_pdf ), LineColor(kRed));
	//data_dist->plotOn(Dframe, Components( *poly ), LineStyle(kDashed));
	data_dist->plotOn(Dframe);

	Dframe->Draw();
	
	
	*n_sig_out = s_nev->getValV() ;
	*n_sig_err = b_nev->getError() ;
	
}
