#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include <sstream>
#include <iostream>

#include "HistFitterFunc.h"
void HistFit(std::string s_branch = "Bs_M", double min_bin = 0, double max_bin = 0){


	TFile* f_data	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_trimmed_norm_s.root"); // This contains Data and combinatorial
	TFile* f_dataSS	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012SS_trimmed_norm_tightDs.root"); // This contains Data and combinatorial
	TFile* f_signal	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL.root"); // This contains Bs->Ds(*)
	TFile* f_BG1	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/DTT_Bs2DsstDsst.root"); // BsToDsDs*
	
	TTree* T_data 		= (TTree*)f_data->Get("reducedTree");
	TTree* T_dataSS 	= (TTree*)f_dataSS->Get("reducedTree");
	TTree* T_signal 	= (TTree*)f_signal->Get("Bs2DsMuNuTuple/DecayTree");
	TTree* T_BG1	 	= (TTree*)f_signal->Get("Bs2DsMuNuTuple/DecayTree");

	// We can add more later
	 
	double low = 3000;
	double high = 6000;
	int n_bins = 60;
	vector<TH1F*> histos; vector<std::string> names;
	vector< vector<double>> parameters;
	
	
	//histos.push_back( new TH1F("h_combH", "Combinatorial High", n_bins, low, high) );		names.push_back("Combinatorial. High Mass");				T_data->Draw("Bs_MCORR>>h_combH", "Ds_MM > 2000");
	//histos.push_back( new TH1F("h_combL", "Combinatorial Low", n_bins, low, high) );		names.push_back("Combinatorial. Low Mass");					T_data->Draw("Bs_MCORR>>h_combL", "Ds_MM < 1940");
	//histos.push_back( new TH1F("h_comb", "Combinatorial", n_bins, low, high) );			names.push_back("Combinatorial");						T_data->Draw("Bs_MCORR>>h_comb", "Ds_MM < 1940||Ds_MM > 2000");

	histos.push_back( new TH1F("h_Dsst", "Dsst", n_bins, low, high) );					parameters.push_back({0.5, 0.0, 0.48});	names.push_back("B_{s} #rightarrow D_{s}* #mu #nu");		T_signal->Draw("Bs_MCORR>>h_Dsst", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433");	
	histos.push_back( new TH1F("h_signal", "Signal", n_bins, low, high) );				parameters.push_back({0.5, 0.0, 0.48});	names.push_back("B_{s} #rightarrow D_{s} #mu #nu");			T_signal->Draw("Bs_MCORR>>h_signal", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 531");
	
	//histos.push_back( new TH1F("h_AllSig", "Dsst", n_bins, low, high) );				parameters.push_back({0.715, 0.0, 1.0});	names.push_back("B_{s} #rightarrow D_{s}^{(*)} #mu #nu");	T_signal->Draw("Bs_MCORR>>h_AllSig");

	//histos.push_back( new TH1F("h_DsDs", "DsDs", n_bins, low, high) );					parameters.push_back({0.715, 0.0, 0.3});	names.push_back("B_{s} #rightarrow D_{s}^{(*)} D_{s}^{(*)}");		T_signal->Draw("Bs_MCORR>>h_DsDs");
	
	histos.push_back( new TH1F("h_DsSS", "DsSS", n_bins, low, high) );					parameters.push_back({0.715, 0.0, 0.5/*0.039448*0.9, 0.039448*1.1*/ });	names.push_back("Same Sign D_{s} + #mu");				T_dataSS->Draw("Bs_MCORR>>h_DsSS", "nsig_sw_H");
	
	histos.push_back( new TH1F("h_data", "Data", n_bins, low, high) );					names.push_back("Data");									T_data->Draw("Bs_MCORR>>h_data", "nsig_sw_E");


	for ( auto const& histoit: histos )
		histoit->GetXaxis()->SetTitle("B_{s} Corrected Mass");

	TCanvas *c1 = new TCanvas("c1", "c1", 1920, 1080);
	TCanvas *c2 = new TCanvas("c2", "c2", 1920, 1080);
	HistFitter(histos, parameters, names, c1, c2);
	
}
