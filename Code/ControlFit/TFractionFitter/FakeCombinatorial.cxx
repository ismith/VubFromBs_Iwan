#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1F.h"
#include "TCanvas.h"

#include "TVector3.h"
#include "TLorentzVector.h"

void FakeCombinatorial(){
	
	TFile* F_data = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012.root");
	TFile* F_MC = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL.root");
	
	TTree* T_data = (TTree*)F_data->Get("Bs2DsMuNuTuple/DecayTree");
	TTree* T_MC = (TTree*)F_MC->Get("Bs2DsMuNuTuple/DecayTree");
	
	//Values to be taken from MC
	double Ds_E, Ds_PX, Ds_PY, Ds_PZ;
	double B_PV_X, B_PV_Y, B_PV_Z;
	double B_SV_X, B_SV_Y, B_SV_Z;
	
	//Values to be taken from data
	double mu_E, mu_PX, mu_PY, mu_PZ, Ds_M;
	
	T_MC->SetBranchAddress("Ds_PE", &Ds_E);
	T_MC->SetBranchAddress("Ds_PX", &Ds_PX);
	T_MC->SetBranchAddress("Ds_PY", &Ds_PY);
	T_MC->SetBranchAddress("Ds_PZ", &Ds_PZ);
	
	T_MC->SetBranchAddress("Bs_ENDVERTEX_X", &B_SV_X);
	T_MC->SetBranchAddress("Bs_ENDVERTEX_Y", &B_SV_Y);
	T_MC->SetBranchAddress("Bs_ENDVERTEX_Z", &B_SV_Z);

	T_MC->SetBranchAddress("Bs_OWNPV_X", &B_PV_X);
	T_MC->SetBranchAddress("Bs_OWNPV_Y", &B_PV_Y);
	T_MC->SetBranchAddress("Bs_OWNPV_Z", &B_PV_Z);
	
	T_data->SetBranchAddress("muon_p_PE", &mu_E);
	T_data->SetBranchAddress("muon_p_PX", &mu_PX);
	T_data->SetBranchAddress("muon_p_PY", &mu_PY);
	T_data->SetBranchAddress("muon_p_PZ", &mu_PZ);	
	T_data->SetBranchAddress("Ds_M", &Ds_M);	
	
	
	TH1F* h_MCORR = new TH1F("h_MCORR", "h_MCORR", 100, 3000, 10000);
	
	int nev = min( T_data->GetEntries(), T_MC->GetEntries() );
	
	for (int it = 0; it < 100000; it++){
		T_data->GetEntry(it);
		T_MC->GetEntry(it);
		if ( Ds_M > 1950 and Ds_M < 1990 )
			continue;
			
		TLorentzVector TLV_D( Ds_PX, Ds_PY, Ds_PZ, Ds_E );
		TLorentzVector TLV_mu( mu_PX, mu_PY, mu_PZ, mu_E );
		
		TVector3 B_dirn( B_SV_X - B_PV_X, B_SV_Y - B_PV_Y, B_SV_Z - B_PV_Z );
		
		double m_Dmu = ( TLV_D + TLV_mu ).M();
		double PT_Dmu = (TLV_D + TLV_mu ).Vect().Perp(B_dirn);
	
		double MCORR = sqrt( m_Dmu * m_Dmu + PT_Dmu * PT_Dmu ) + PT_Dmu;
		
		h_MCORR->Fill(MCORR);
		if ( it%1000 == 0)
			std::cout << MCORR << "\t" << m_Dmu << "\t" << PT_Dmu <<  std::endl;
		
		
	}
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	h_MCORR->Draw();
}
