#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include <sstream>
#include <iostream>

#include "TFractionFitter.h"
void HistFitter(vector<TH1F*> histos, vector<vector<double>> parameters, vector<std::string> descrips, TCanvas* c1, TCanvas* c2){



	int n_MC = histos.size() - 1;
	
	TObjArray *mc = new TObjArray(n_MC);
	
		
	for ( int x = 0; x < n_MC; x++ ){
		histos.at( x )->Scale( 10000000.0 / histos.at( x )->Integral() );
		mc->Add( histos.at( x ) );
	}
	TFractionFitter* fit = new TFractionFitter(histos.back(), mc, "V");
	
	for ( int x = 0; x < n_MC; x++ )
		fit->Constrain(x, parameters.at(x).at(1), parameters.at(x).at(2));
		
	Int_t status = fit->Fit();   
	std::cout << "fit status: " << status << std::endl;
	
	THStack* stack =  new THStack("stack", "Solutions to fit");
	THStack* stack2 =  new THStack("stack", "ModifiedSolutions to fit");

	vector<TH1F*> v_hist = histos;
	v_hist.pop_back();
	
	vector<TH1*> v_hist2;
	for ( int x = 0; x < n_MC; x++ )
		v_hist2.push_back( fit->GetMCPrediction(x) );
	
	for ( int x = 0; x< v_hist.size(); x++)
		std::cout << "Histogram " << x << " Input size " << v_hist.at(x)->Integral() << " Output Size " << v_hist2.at(x)->Integral() <<  std::endl;
	
		std::cout << "Histogram Data size"  << histos.back()->Integral() << std::endl;
	
				
	double result = 0, res_err = 0;

	double int_MC  = 0, int_MC2 = 0;
	double int_Dat = histos.back()->Integral(); 

	for ( int x = 0; x < n_MC; x++ ){
		fit->GetResult(x, result, res_err);
		histos.at( x ) -> Scale( result );
		v_hist2.at(x) ->Scale (result);
		std::cout << result << std::endl;
		int_MC += histos.at( x )->Integral();
		int_MC2 += v_hist2.at( x )->Integral();
	}
	
	for ( int x = 0; x < n_MC; x++ ){
		histos.at(x)->Scale( int_Dat / int_MC );
		histos.at(x)->SetFillColor(x+2);
		v_hist2.at(x)->Scale( int_Dat / int_MC2 );
		v_hist2.at(x)->SetFillColor(x+2);
	}
	
	TLegend *leg_1 = new TLegend(0.2, 0.7, 0.5, 0.9);
	
	for ( int x = 0; x < n_MC; x++ )
		leg_1->AddEntry(histos.at(x), descrips.at(x).c_str(), "f");
	leg_1->AddEntry(histos.back(), descrips.back().c_str(), "lep");
	
	for ( int x = 0; x< histos.size()-1; x++){ 
		histos.at(x)->Sumw2(false);
		v_hist2.at(x)->Sumw2(false);
	}	


	while (v_hist.size() > 0 ){
		double min_int = 999999999999999;
		int it = 0;		
		for ( unsigned int x = 0; x < v_hist.size(); x++ ){
			if (v_hist.at(x)->Integral() < min_int ){
				min_int = v_hist.at(x)->Integral();
				it = x;
			}
		}
		stack ->Add( v_hist.at(it)  );
		stack2->Add( v_hist2.at(it) );
		
		v_hist.erase(  v_hist.begin()  + it );
		v_hist2.erase( v_hist2.begin() + it );
		
	}
	
	

	
	c1->cd();
	histos.back()->Draw("Ep");
	stack->Draw("SAME");
	histos.back()->Draw("Ep SAME");
	leg_1->Draw("SAME");

	c2->cd();
	histos.back()->Draw("Ep");
	stack2->Draw("SAME");
	histos.back()->Draw("Ep SAME");
	leg_1->Draw("SAME");	

	
	
	
	//Print some stats:
	double Integral = 0;
	for ( int x = 0; x < n_MC; x++ )
		Integral += histos.at(x)->Integral();
	for ( int x = 0; x < n_MC; x++ )
		std::cout << "Component " << x << " Contains: " << histos.at(x)->Integral() * 100 / Integral<< std::endl;
		
	
	
}
