#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include <sstream>
#include <iostream>

#include "TFractionFitter.h"
void HistFitter(std::string s_branch = "Bs_M", double min_bin = 0, double max_bin = 0){


	TFile* f_data	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012.root"); // This contains Data and combinatorial
	TFile* f_signal	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL.root"); // This contains Bs->Ds(*)
	
	TTree* T_data 		= (TTree*)f_data->Get("Bs2DsMuNuTuple/DecayTree");
	TTree* T_signal 	= (TTree*)f_signal->Get("Bs2DsMuNuTuple/DecayTree");

	// We can add more later
	
	double low = 2500;
	double high = 7000;
	
	TH1F* h_data 		= new TH1F("h_data", "Data", 30, low, high);	
	
	TH1F* h_combhigh	= new TH1F("h_combH", "Combinatorial High", 30, low, high);	
	TH1F* h_comblow 	= new TH1F("h_combL", "Combinatorial Low", 30, low, high);
	TH1F* h_comb		= new TH1F("h_comb", "Combinatorial", 30, low, high);
	
	TH1F* h_signal 		= new TH1F("h_signal", "Signal", 30, low, high);
	TH1F* h_Dsst		= new TH1F("h_Dsst", "Dsst", 30, low, high);
	
	
	T_data->Draw("Bs_MCORR>>h_data", "Ds_MM > 1940 && Ds_MM < 2000");
	
	T_data->Draw("Bs_MCORR>>h_combH", "Ds_MM > 2000");
	T_data->Draw("Bs_MCORR>>h_combL", "Ds_MM < 1940");
	T_data->Draw("Bs_MCORR>>h_comb", "Ds_MM < 1940||Ds_MM > 2000");
	
	
	T_signal->Draw("Bs_MCORR>>h_Dsst", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433");
	T_signal->Draw("Bs_MCORR>>h_signal", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 531");
	
	
	TObjArray *mc = new TObjArray(3);
	//mc->Add(h_combhigh);
	//mc->Add(h_comblow);
	mc->Add(h_comb);
	mc->Add(h_signal);
	mc->Add(h_Dsst);
	
	TFractionFitter* fit = new TFractionFitter(h_data, mc, "V");
	fit->Constrain(0,0.0,1.0); 
	fit->Constrain(1,0.0,1.0); 
	fit->Constrain(2,0.0,1.0); 
	Int_t status = fit->Fit();   
	std::cout << "fit status: " << status << std::endl;
	
	if (status == 0) {                       // check on fit status
		TH1F* result = (TH1F*) fit->GetPlot();
		h_data->Draw("Ep");
		result->Draw("same");
	}
	TCanvas *c1 = new TCanvas("c1", "c1", 1920, 1080);
	
	THStack* stack =  new THStack("stack", "Solutions to fit");
	
	double result = 0, res_err = 0;
	
	TH1* h0 = fit->GetMCPrediction(0);
	TH1* h1 = fit->GetMCPrediction(1);
	TH1* h2 = fit->GetMCPrediction(2);
	//TH1* h3 = fit->GetMCPrediction(3);


	fit->GetResult(0, result, res_err);
	h0->Scale( result);
	fit->GetResult(1, result, res_err);
	h1->Scale( result);	
	fit->GetResult(2, result, res_err);
	h2->Scale( result);	
	//fit->GetResult(3, result, res_err);
	//h3->Scale( result);	
	
	double n_MC  = h0->Integral() + h1->Integral() + h2->Integral();// + h3->Integral();
	double n_Dat = h_data->Integral(); 

	h0->Scale( n_Dat / n_MC );
	h1->Scale( n_Dat / n_MC );	
	h2->Scale( n_Dat / n_MC );	
	//h3->Scale( n_Dat / n_MC );		

	h0->SetFillColor(2);
	h1->SetFillColor(3);
	h2->SetFillColor(4);
	//h3->SetFillColor(5);
	
	vector<TH1*> v_hist;
	v_hist.push_back(h0);
	v_hist.push_back(h1);
	v_hist.push_back(h2);
	//v_hist.push_back(h3);

	while (v_hist.size() > 0 ){
		double min_int = 999999999999999;
		int it;		
		for ( unsigned int x = 0; x < v_hist.size(); x++ ){
			if (v_hist.at(x)->Integral() < min_int ){
				min_int = v_hist.at(x)->Integral();
				it = x;
			}
		}
		stack->Add( v_hist.at(it) );
		v_hist.erase( v_hist.begin() + it );
		
	}
	
	/*
	stack->Add(h0);
	stack->Add(h1);
	stack->Add(h2);
	stack->Add(h3);
	*/
	TLegend *leg_1 = new TLegend(0.3, 0.7, 0.6, 0.9);
	
	leg_1->AddEntry(h0, "Combinatorial. High Mass", "f");
	leg_1->AddEntry(h1, "Combinatorial. Low Mass", "f");
	leg_1->AddEntry(h2, "B_{s} #rightarrow D_{s} #mu #nu", "f");
	//leg_1->AddEntry(h3, "B_{s} #rightarrow D_{s}^{*} #mu #nu", "f");

	
	stack->Draw();
	h_data->Draw("Ep SAME");
	leg_1->Draw("SAME");
	
	
	
}
