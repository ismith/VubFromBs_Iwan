#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "TH1F.h"
#include "TLine.h"
#include "TF1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include <string>
#include <sstream>
#include <random>
#include <utility>
#include <fstream>

//RooFit Libraries
#include "RooDataSet.h"

#include "RooRealVar.h"

#include "RooBifurGauss.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooGenericPdf.h"

#include "RooProdPdf.h"
#include "RooAddPdf.h"

#include "RooPlot.h"

using namespace RooFit ;

RooProdPdf* FitSig_MC(){

	TFile* File_MC = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL_Prescale0.02.root");
	TTree* Tree_MC = (TTree*)File_MC->Get("DecayTree");
	
	
	RooRealVar* B_M = new RooRealVar("Bs_MCORR", "Bs Corrected Mass", 2500, 7500);
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", 1930, 2010);
	
	RooDataSet* data = new RooDataSet("data", "Monte Carlo Data", RooArgSet(*B_M, *D_M), Import(*Tree_MC));

	//Ds mass distribution	
	RooRealVar* D_DG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 1970, 1960, 1980);
	RooRealVar* D_DG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 5, 1, 9);
	RooRealVar* D_DG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 10, 9, 20);
	RooRealVar* D_DG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.8, 0.0, 1.0);
	
	RooGaussian* D_Gauss1_pdf = new RooGaussian("D_Gauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_DG_mu, *D_DG_s1);
	RooGaussian* D_Gauss2_pdf = new RooGaussian("D_Gauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s2);

	RooAddPdf* D_DoubleGauss_pdf = new RooAddPdf("D_DoubleGauss_pdf", "Ds Double Gaussian", *D_Gauss1_pdf, *D_Gauss2_pdf, *D_DG_f1);
	
	//Bs corrected mass distribution
	RooRealVar* B_CB_mu = new RooRealVar("B_CB_mu", "Bs CrystalBall Mean", 5100, 5000, 6100);
	
	RooRealVar* B_CB_s = new RooRealVar("B_CB_s1", "Bs CrystalBall sigma", 700, 10, 1000);
	RooRealVar* B_CB_n = new RooRealVar("B_CB_n1", "Bs CrystalBall n", 2.5, 0.1, 10);
	RooRealVar* B_CB_a = new RooRealVar("B_CB_a1", "Bs CrystalBall alpha", -2.0, -5.0, 0.0);
	
	//If using 2 x CB
	//RooRealVar* B_CB_s2 = new RooRealVar("B_CB_s2", "Bs CrystalBall sigma", 300, 10, 1000);
	//RooRealVar* B_CB_n2 = new RooRealVar("B_CB_n2", "Bs CrystalBall n", 2.5, 0.1, 10);
	//RooRealVar* B_CB_a2 = new RooRealVar("B_CB_a2", "Bs CrystalBall alpha", -2.0, -5.0, 0.0);
	
	
	//If using CB + BFG
	RooRealVar* B_BG_s1 = new RooRealVar("B_BG_s1", "Bs CrystalBall sigma 2", 700, 10, 1000);
	RooRealVar* B_BG_s2 = new RooRealVar("B_BG_s2", "Bs CrystalBall n 2", 300, 10, 1000);
	
	RooRealVar* B_CB_f1 = new RooRealVar("B_CB_f1", "Bs Crystal Ball Fraction 1", 0.8, 0, 1);
	
	RooCBShape* B_CB_pdf = new RooCBShape("B_CB_pdf", "Bs Crystal Ball pdf", *B_M, *B_CB_mu, *B_CB_s, *B_CB_a, *B_CB_n);
	//RooCBShape* B_CB_pdf2 = new RooCBShape("B_CB_pdf2", "Bs Crystal Ball pdf2", *B_M, *B_CB_mu, *B_CB_s2, *B_CB_a2, *B_CB_n2);
	RooBifurGauss* B_BG_pdf = new RooBifurGauss("B_BG_pdf", "Bs Bifurcated Gaussian pdf", *B_M, *B_CB_mu, *B_BG_s1, *B_BG_s2);
	
	//RooAddPdf* B_DoubleCB = new RooAddPdf("B_DoubleCB", "Bs Double Crystal Ball", *B_CB_pdf, *B_CB_pdf2, *B_CB_f1);
	RooAddPdf* B_DoubleCB = new RooAddPdf("B_DoubleCB", "Bs Crystal Ball + Bifurcated Gauss", *B_CB_pdf, *B_BG_pdf, *B_CB_f1);
	//Final pdf shape
	RooProdPdf* Signal_Mass = new RooProdPdf("Signal_Mass", "Combined Bs Ds fit", *D_DoubleGauss_pdf, *B_DoubleCB);
		
	Signal_Mass->fitTo(*data, NumCPU(8));
	
	TCanvas* c1 = new TCanvas("c1", "Mass Fit to D and B", 3200, 1600);
	c1->Divide(2,2);
	
	RooPlot* Bframe = B_M->frame();
	data->plotOn(Bframe, Binning(100));
	Signal_Mass->plotOn(Bframe);
	
	
	RooPlot* Bframe2 = B_M->frame();
	Bframe2->addObject((TObject*)Bframe->residHist()) ; 
	Bframe2->SetMinimum(-80);
	Bframe2->SetMaximum(+80);
	
	c1->cd(1);
	Bframe->Draw();
	c1->cd(3);
	Bframe2->Draw();
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe, Binning(100));
	Signal_Mass->plotOn(Dframe);
	
	RooPlot* Dframe2 = D_M->frame();
	Dframe2->addObject((TObject*)Dframe->residHist()) ; 
	Dframe2->SetMinimum(-80);
	Dframe2->SetMaximum(+80);
		
	c1->cd(2);
	Dframe->Draw();
	c1->cd(4);
	Dframe2->Draw();
		
	//Fix all the necessary parameters
	D_DG_s1->setConstant(true);
	D_DG_s2->setConstant(true);
	D_DG_f1->setConstant(true);
	B_CB_s->setConstant(true);
	B_CB_n->setConstant(true);
	B_CB_a->setConstant(true);
	B_BG_s1->setConstant(true);
	B_BG_s2->setConstant(true);
	B_CB_f1->setConstant(true);
		
	return(Signal_Mass );
}


RooProdPdf* FitBG_Dat(){
	
	TFile* File_MC = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_Prescale0.01.root");
	TTree* Tree_MC = (TTree*)File_MC->Get("DecayTree");
	
	
	RooRealVar* B_M = new RooRealVar("Bs_MCORR", "Bs Corrected Mass", 2000, 10000);
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", 1915, 2045);
	
	RooDataSet* data = new RooDataSet("data", "Monte Carlo Data", RooArgSet(*B_M, *D_M), Import(*Tree_MC), Cut("Ds_M<1940"/* || Ds_M>2000"*/) );
	
	
	//Model The Background in correted mass
	//Crystal Ball
	RooRealVar* B_CB_mu_b = new RooRealVar("B_CB_mu_b", "Background mean in CB", 5150, 5000, 5500);
	RooRealVar* B_CB_s_b = new RooRealVar("B_CB_s_b", "Background sigma in CB", 1100, 1000, 3000);
	RooRealVar* B_CB_n_b = new RooRealVar("B_CB_n_b", "Background n in CB", 2, 0, 20);
	RooRealVar* B_CB_a_b = new RooRealVar("B_CB_a_b", "Background alpha in CB", -2.0, -5.0, 0.0);
	
	RooCBShape* B_CB_pdf_b = new RooCBShape("B_CB_pdf_b", "Background Crystal Ball", *B_M, *B_CB_mu_b, *B_CB_s_b, *B_CB_a_b, *B_CB_n_b);

	//Crystal Ball 2
	RooRealVar* B_CB_s_b2 = new RooRealVar("B_CB_s_b2", "Background sigma in CB2", 500, 100, 1000);
	RooRealVar* B_CB_n_b2 = new RooRealVar("B_CB_n_b2", "Background n in CB2", 0.5, 0, 100);
	RooRealVar* B_CB_a_b2 = new RooRealVar("B_CB_a_b2", "Background alpha in CB2", 2.0, 0.0, 5.0);
	
	RooCBShape* B_CB_pdf_b2 = new RooCBShape("B_CB_pdf_b2", "Background Crystal Ball2", *B_M, *B_CB_mu_b, *B_CB_s_b2, *B_CB_a_b2, *B_CB_n_b2);

	
	//Bifurcated Gaussian
	RooRealVar* B_BG_s1_b = new RooRealVar("B_BG_s1_b", "Background sigma 1 in Bigur Guass", 1000, 0, 2000);
	RooRealVar* B_BG_s2_b = new RooRealVar("B_BG_s2_b", "Background sigma 1 in Bigur Guass", 200, 0, 2000);
	
	RooBifurGauss* B_BG_pdf_b = new RooBifurGauss("B_BG_pdf_b", "Background Bifur Gauss", *B_M, *B_CB_mu_b, *B_BG_s1_b, *B_BG_s2_b);
	
	RooRealVar* B_CB_f_b = new RooRealVar("B_CB_f_b", "Background CB fraction", 0.5, 0, 1);
	RooRealVar* B_BG_f_b = new RooRealVar("B_BG_f_b", "Background BG fraction", 0.3, 0, 1);
	RooAddPdf* B_MassModel_b = new RooAddPdf("B_MassModel_b", "Background Crystal Ball + Bifur Gauss", RooArgList(*B_CB_pdf_b, *B_BG_pdf_b, *B_CB_pdf_b2), RooArgSet(*B_CB_f_b, *B_BG_f_b));

	//Model the Background in Ds Mass

	RooRealVar* D_m_b = new RooRealVar("D_m_b", "D mass gradient", 0, -0.3, 0.3);
	D_m_b->setConstant(true);
	RooRealVar* D_c_b = new RooRealVar("D_c_b", "D mass c", 400, 300, 500);
	D_c_b->setConstant(true);
	
	RooGenericPdf* D_Slope_b = new RooGenericPdf("D_Slope_b", "D background slope", "(Ds_M - 1975) * D_m_b + D_c_b", RooArgList(*D_M, *D_m_b, *D_c_b));
	
	RooProdPdf* MassModel_b = new RooProdPdf("MassModel_b", "Background Mass Model", *B_MassModel_b, *D_Slope_b);


	MassModel_b->fitTo(*data, NumCPU(8));
	
	
	TCanvas* c2 = new TCanvas("c2", "Mass Fit to D and B", 3200, 1600);
	c2->Divide(2,2);	
	RooPlot* Bframe = B_M->frame();
	data->plotOn(Bframe, Binning(100));
	MassModel_b->plotOn(Bframe);
	
	RooPlot* Bframe2 = B_M->frame();
	Bframe2->addObject((TObject*)Bframe->residHist()) ; 
	Bframe2->SetMinimum(-100);
	Bframe2->SetMaximum(+100);
	
	c2->cd(1);
	Bframe->Draw();
	c2->cd(3);
	Bframe2->Draw();
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe, Binning(100));
	MassModel_b->plotOn(Dframe);
	
	RooPlot* Dframe2 = D_M->frame();
	Dframe2->addObject((TObject*)Dframe->residHist()) ; 	
	Dframe2->SetMinimum(-100);
	Dframe2->SetMaximum(+100);
	
	c2->cd(2);
	Dframe->Draw();
	c2->cd(4);
	Dframe2->Draw();
	
	
	B_CB_s_b->setConstant(true);
	B_CB_n_b->setConstant(true);
	B_CB_a_b->setConstant(true);
	B_CB_s_b2->setConstant(true);
	B_CB_n_b2->setConstant(true);
	B_CB_a_b2->setConstant(true);
	B_BG_s1_b->setConstant(true);
	B_BG_s2_b->setConstant(true);
	B_BG_f_b->setConstant(true);
	B_CB_f_b->setConstant(true);		

	
	return(	MassModel_b );
}

void FitData(){
	
	
	TFile* File_MC = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_Prescale0.01.root");
	TTree* Tree_MC = (TTree*)File_MC->Get("DecayTree");
	
	
	RooRealVar* B_M = new RooRealVar("Bs_MCORR", "Bs Corrected Mass", 2000, 10000);
	RooRealVar* D_M = new RooRealVar("Ds_M", "Ds Mass", 1915, 2045);
	
	RooDataSet* data = new RooDataSet("data", "Monte Carlo Data", RooArgSet(*B_M, *D_M), Import(*Tree_MC)/*, Cut("Ds_M>1955 && Ds_M<1985")*/);
	//RooDataSet* data2 = new RooDataSet("data2", "Monte Carlo Data", RooArgSet(*B_M, *D_M), Import(*Tree_MC), Cut("Ds_M>1955 && Ds_M<1985"));
		
	RooProdPdf* SigModel = FitSig_MC();
	RooProdPdf* BGModel = FitBG_Dat();
	
	RooRealVar* sig_frac = new RooRealVar("sig_frac", "Fraction of signal events", 0.2, 0, 1);
	
	RooAddPdf* MassModel = new RooAddPdf("SBMassModel", "Mass Model For Signal And Background", *SigModel, *BGModel, *sig_frac);
	
	MassModel->fitTo(*data, NumCPU(8));
	
	
	TCanvas* c3 = new TCanvas("c3", "Mass Fit to D and B", 3200, 1600);
	c3->Divide(2,2);
	
	RooPlot* Bframe = B_M->frame();
	data->plotOn(Bframe, Binning(100));
	MassModel->plotOn(Bframe, Components("B_DoubleCB"));
	MassModel->plotOn(Bframe, Components("B_MassModel_b"));
	MassModel->plotOn(Bframe);
	
	RooPlot* Bframe2 = B_M->frame();
	Bframe2->addObject((TObject*)Bframe->residHist()) ; 
	Bframe2->SetMinimum(-500);
	Bframe2->SetMaximum(+500);
	
	c3->cd(1);
	Bframe->Draw();
	c3->cd(3);
	Bframe2->Draw();
	
	RooPlot* Dframe = D_M->frame();
	data->plotOn(Dframe, Binning(100));
	MassModel->plotOn(Dframe);
	MassModel->plotOn(Dframe, Components("D_Slope_b"));
	MassModel->plotOn(Dframe, Components("D_DoubleGauss_pdf"));
	
	RooPlot* Dframe2 = D_M->frame();
	Dframe2->addObject((TObject*)Dframe->residHist()) ; 	
	Dframe2->SetMinimum(+1300);
	Dframe2->SetMaximum(+1900);

	c3->cd(2);
	Dframe->Draw();
	c3->cd(4);
	Dframe2->Draw();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
