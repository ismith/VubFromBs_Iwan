#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include <sstream>
#include <iostream>

#include "TFractionFitter.h"
TCanvas* HistFitter_class(vector<TH1F*> histos, vector<std::string> decsrips){

	int n_MC = histos.size() - 1
	
	TObjArray *mc = new TObjArray(n_MC);

	for ( int x = 0; x < n_MC; x++ )
		mc->Add(histos.at( x ) );
		
	TFractionFitter* fit = new TFractionFitter(histos.back(), mc, "V");
	
	for ( int x = 0; x < n_MC; x++ )
		fit->Constrain(x, 0, 1);
		
	Int_t status = fit->Fit();   
	std::cout << "fit status: " << status << std::endl;
	

	TCanvas *c1 = new TCanvas("c1", "c1", 1920, 1080);
	
	THStack* stack =  new THStack("stack", "Solutions to fit");
	
	double result = 0, res_err = 0;

	double n_MC  = 0;
	double n_Dat = histsos.back()->Integral(); 

	for ( int x = 0; x < n_MC; x++ ){
		fit->GetResult(0, result, res_err);
		histsos.at( x ) -> Scale( result );
		n_MC += histsos.at( x )->Integral();
	}
	
	for ( int x = 0; x < n_MC; x++ ){
		histos.at(x)->Scale( n_Dat / n_MC );
		histps.at(x)->SetFillColor(x+2);
	}
	
	
	vector<TH1*> v_hist = histos;
	v_hist.erase( v_hist.end() );

	while (v_hist.size() > 0 ){
		double min_int = 999999999999999;
		int it;		
		for ( unsigned int x = 0; x < v_hist.size(); x++ ){
			if (v_hist.at(x)->Integral() < min_int ){
				min_int = v_hist.at(x)->Integral();
				it = x;
			}
		}
		stack->Add( v_hist.at(it) );
		v_hist.erase( v_hist.begin() + it );
		
	}
	
	TLegend *leg_1 = new TLegend(0.2, 0.7, 0.5, 0.9);

	for ( int x = 0; x < histos.size(); x++ )
		leg_1->AddEntry(histsos.at(x), descrips.at(x), "f");

	stack->Draw();
	histos.back()->Draw("Ep SAME");
	leg_1->Draw("SAME");
	
	
	
}
