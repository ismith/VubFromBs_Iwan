#include "TH1F.h"
#include "TLine.h"
#include "TF1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include <string>
#include <sstream>
#include <random>
#include <utility>
#include <fstream>


void CompareMultiple(const char* s_branch = "Bs_M", double min_bin = 0, double max_bin = 0){
//I might just give up on this script. It's really complicated. Let's get back to th good stuff.

	vector<std::string>	filenames;
	vector<std::string>	treenames;
	vector<TFile*>		files;
	vector<TTree*> 		Trees;
	vector<std::string>	leafnames;
	vector<TH1F*>		histograms;

	vector<int>			colours;

	std::string dir = "/media/ismith/Seagate Expansion Drive/NTuples/BsKmuNu/";

   filenames.push_back("data_2012.root");			treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );		files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(1);
   filenames.push_back("data_2011.root");			treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );		files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(2);
   filenames.push_back("DTT_Signal.root");			treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(3);
//   filenames.push_back("DTT_Bd2JpsiKst.root");	treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(11);
   filenames.push_back("DTT_Bs2JpsiPhi.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(5);
   filenames.push_back("DTT_Lb2PMuNu_LCSR.root");	treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(6);
   filenames.push_back("DTT_Bu2RhoMuNu.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(7);
   filenames.push_back("DTT_Bu2JpsiK.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(8);
   filenames.push_back("DTT_Bu2D0MuNu.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(9);
   filenames.push_back("DTT_Bs2KstMuNu.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(4);
   filenames.push_back("DTT_Bd2PiMuNu.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(28);
   filenames.push_back("DTT_Bc2D0MuNu.root");		treenames.push_back("Bs2KmuNuTuple/DecayTree");	leafnames.push_back( s_branch );	files.push_back( TFile::Open( ( dir + filenames.back()).c_str() ) );	Trees.push_back( (TTree*)files.back()->Get(treenames.back().c_str() ) );	colours.push_back(15);
                 
	double nentries = Trees.at(0)->GetEntries();
	if ( min_bin == 0 and max_bin == 0 ){
		min_bin = Trees.at(0)->GetMinimum(leafnames.at(0).c_str());
		max_bin = Trees.at(0)->GetMaximum(leafnames.at(0).c_str());
	}
	/*
	while ( double( Trees.at(0)->GetEntries( (leafnames.at(0) + " < " + to_string(max_bin) ).c_str() ) ) / nentries > 0.99){
		max_bin -= (max_bin - min_bin) / 100.0;
		std::cout << "Maximum bin: " << max_bin<< std::endl;
	}
	
	while ( double( Trees.at(0)->GetEntries( (leafnames.at(0) + " > " + to_string(min_bin) ).c_str() ) ) / nentries > 0.98){
		min_bin += (max_bin - min_bin) / 100.0;
		std::cout << "Minimum bin: " << min_bin<< std::endl;
	}
	*/
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1920, 1080);
	
	double maxbinval = 0;
	for ( unsigned int x = 0; x < filenames.size(); x++){
		histograms.push_back( new TH1F( filenames.at(x).c_str(), filenames.at(x).c_str(), 100, min_bin, max_bin ) );
		Trees.at(x)->Draw( ( leafnames.at(x) + " >> " + filenames.at(x) ).c_str() );
		histograms.back()->Sumw2();
		histograms.back()->Scale( 1 / histograms.back()->Integral(1, 100) );
		
		( histograms.back()->GetMaximum() > maxbinval ) ? maxbinval = histograms.back()->GetMaximum() :  maxbinval=maxbinval;
		histograms.back()->SetLineColor(colours.at(x));
		histograms.back()->SetMarkerColor(colours.at(x));
	}
	
	for ( unsigned int x = 0; x < filenames.size(); x++){
		if ( x == 0 ){
			histograms.at(x)->SetMaximum(maxbinval);
			histograms.at(x)->Draw();
		}
		else {
			histograms.at(x)->Draw("SAME");
		}
		
	}
	
	
	
	
}
