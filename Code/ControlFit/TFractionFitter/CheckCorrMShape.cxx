#include <istream>

#include "TH1F.h"
#include "TLine.h"

#include "TTree.h"
#include "TFile.h"

#include "TCanvas.h"

void CheckCorrMShape(){
	
	TFile* f_data = TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_trimmed_norm_s.root");
	TTree* T_data = (TTree*) f_data->Get("reducedTree");
	
	int nbins = 40;
	double bin_low = 3000;
	double bin_high = 6000;
	
	
	double D_width = 5;
	double D_min = 1920; double D_max = 2030;
	
	int it = 0;
	for (double D_it = D_min; D_it <D_max; D_it += D_width){
	
		TH1F* h_D_M = new TH1F("h_D_M", "h_D_M", 100, D_min, D_max);
		TH1F* h_B_M = new TH1F("h_B_M", "h_B_M", nbins, bin_low, bin_high);
		h_D_M->Sumw2();
		h_B_M->Sumw2();
		
		T_data->Draw("Ds_MM >> h_D_M");
		T_data->Draw("Bs_MCORR >> h_B_M", ("Ds_MM > " + std::to_string(D_it) + " && Ds_MM < " + std::to_string(D_it + D_width)).c_str() );
		
		double h_min = h_D_M->GetMinimum();
		double h_max = h_D_M->GetMaximum();
		
		TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);
		
		c1->Divide(1,2);

		c1->cd(1);
		h_D_M->Draw();
		TLine* l_low = new TLine(D_it, h_min, D_it, h_max);
		TLine* l_high = new TLine(D_it+D_width, h_min, D_it+D_width, h_max);

		l_low->Draw();
		l_high->Draw();


		c1->cd(2);
		h_B_M->Draw();
		
		c1->Print( (std::to_string(it) + "_WS.png").c_str() );
		delete h_B_M; delete h_D_M;
		delete c1;
		delete l_low; delete l_high;
		it++;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
