#include "TH1F.h"
#include "TLine.h"
#include "TF1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include <string>
#include <sstream>
#include <random>
#include <utility>
#include <fstream>


void MCDatComp(std::string leafMC = "Bs_CorrMass", std::string leafDat = "Bs_MCORR", std::string cutMC = "1", std::string cutDat = "1", double min_bin = 0, double max_bin = 0,
std::string filenameMC = "/media/ismith/Seagate Expansion Drive/NTuples/BsKmuNu/Bs2DsMuNuMC_CONTROL.root", std::string TreenameMC = "Bs2DsMuNuTuple/DecayTree", 
std::string filenameDat = "/media/ismith/Seagate Expansion Drive/NTuples/BsKmuNu/data_2012.root", std::string TreenameDat = "Bs2DsMuNuTuple/DecayTree"){

	if (leafDat == "") leafDat = leafMC;

	TFile* f_MC = TFile::Open(filenameMC.c_str());
	TFile* f_Dat = TFile::Open(filenameDat.c_str());
	TTree* T_MC = (TTree*)f_MC->Get(TreenameMC.c_str());
	TTree* T_Dat = (TTree*)f_Dat->Get(TreenameDat.c_str());
	

	std::string cut_all = "Bs_DIRA_OWNPV > 0.999";
	
	if (min_bin == max_bin){
		double nentries = T_Dat->GetEntries();
		min_bin = T_Dat->GetMinimum(leafDat.c_str());
		max_bin = T_Dat->GetMaximum(leafDat.c_str());
		while ( double( T_Dat->GetEntries( (leafDat + " < " + to_string(max_bin) ).c_str() ) ) / nentries > 0.99){
			max_bin -= (max_bin - min_bin) / 100.0;
			std::cout << "Maximum bin: " << max_bin<< std::endl;
		}
		
		while ( double( T_Dat->GetEntries( (leafDat + " > " + to_string(min_bin) ).c_str() ) ) / nentries > 0.98){
			std::cout << "Minimum bin: " << min_bin<< std::endl;
			min_bin += (max_bin - min_bin) / 100.0;
			std::cout << "Minimum bin: " << min_bin<< std::endl;
		}
	}
	TH1F* h_Dat = new TH1F("h_Dat", "h_Dat", 100, min_bin, max_bin);
	TH1F* h_MC = new TH1F("h_MC", "h_MC", 100, min_bin, max_bin);
	
	T_Dat->Draw( (leafDat + ">>h_Dat").c_str(), (cut_all + " && " + cutDat).c_str());
	T_MC->Draw( (leafMC + ">>h_MC").c_str(), (cut_all + " && " + cutMC).c_str());

	h_Dat->Scale(1 / h_Dat->Integral(1, 100) );
	h_MC->Scale(1 / h_MC->Integral(1, 100) );
	
	h_Dat->SetMaximum( max(h_Dat->GetMaximum(), h_MC->GetMaximum()) *1.05 );
	h_MC->SetMaximum( max(h_Dat->GetMaximum(), h_MC->GetMaximum()) *1.05 );
	
	TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	h_Dat->SetLineColor(2);
	
	h_MC->Draw();
	h_Dat->Draw("SAME");

	
}
