#include "TFractionFitter.h"

#include <iostream>

#include "TH1F.h"

class Frac_Class{
	
	private:
		
		TObjArray* source_histograms = new TObjArray(10);
		TObjArray* output_histograms = new TObjArray(10);
		
		TObjArray* source_histograms_scaled = new TObjArray(10);
		TObjArray* output_histograms_scaled = new TObjArray(10);
		
		double total_mc_events = 0;
		
		vector<double> limits_low;
		vector<double> limits_high;
		
		vector<double> fit_fractions;
		vector<double> fit_fractions_scaled;
		vector<double> fit_fractions_err;
		vector<double> fit_fractions_scaled_err;
		
		vector<const char*> histogram_names;
		
		int n_source = 0;
		
		TH1F* data_source_histogram;
		TH1F* data_output_histogram;
		
		double total_data_events = 0;
		
		bool data_hist = false;
		
		bool fitter_init = false;
		
		TFractionFitter* fitter;
		
	public:
		
		Frac_Class(){
			
		}
		
		int AddSourceHist( TH1F* h_source ){
			
			int n_out = AddSourceHist( h_source, 0.0, 1.0, "" );
			
			return( n_out );
			
		}				
		
		int AddSourceHist( TH1F* h_source, std::string h_name ){
			
			int n_out = AddSourceHist( h_source, 0.0, 1.0, h_name );
			
			return( n_out );
			
		}		
		
		int AddSourceHist( TH1F* h_source, double limit_low, double limit_high){
			
			int n_out = AddSourceHist( h_source, limit_low, limit_high, "" );
			
			return( n_out );
			
			
		}
		
		int AddSourceHist( TH1F* h_source, double limit_low, double limit_high, std::string h_name ){
			
			source_histograms->Add( h_source );
			
			limits_low.  push_back( limit_low  );
			limits_high. push_back( limit_high );
			
			histogram_names.push_back( h_name.c_str() );
			
			n_source++;
			total_mc_events += h_source->Integral();
			
			return( n_source -1 );
			
		}
		
		
		void AddDataHist( TH1F* h_data ){
			
			data_source_histogram = h_data;
			data_hist = true;
			
			total_data_events = h_data->Integral();
			
		}
		
		
		bool init_fitter(){
			
			if ( ! data_hist or n_source == 0 ){
				std::cerr << "No Histogram in either data or MonteCarlo" << std::endl;
				return false;
			}
			
			fitter = new TFractionFitter(data_source_histogram, source_histograms);
			
			for ( int x = 0; x < n_source; x++ ){
				
				fitter->Constrain(x, limits_low.at(x), limits_high.at(x) );
				
			}
			
			fitter_init = true;
			return true;
			
		}
		
		
		int fit(){
			
			if ( ! fitter_init )
				return -1000;
			
			int fit_out = fitter->Fit();
			
			for ( int x = 0; x < n_source; x++ ){
				output_histograms->Add( fitter->GetMCPrediction( x ) );
				
				double result, result_err;
				fitter->GetResult( x, result, result_err );
				
				
				fit_fractions.push_back( result );
			}
			
			
			data_output_histogram = (TH1F*)fitter->GetPlot();
			
			
			
			return fit_out;	
			
			
		} 
		
		
		
		
		
	
};
