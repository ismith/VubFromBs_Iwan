
#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include <sstream>
#include <iostream>

#include "Frac_Class.h"

void Frac_Class_test(){

	TFile* f_data	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_trimmed_norm_s.root"); // This contains Data and combinatorial
	TFile* f_dataSS	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012SS_trimmed_norm_tightDs.root"); // This contains Data and combinatorial
	TFile* f_signal	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL.root"); // This contains Bs->Ds(*)
	TFile* f_BG1	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/DTT_Bs2DsstDsst.root"); // BsToDsDs*
	
	TTree* T_data 		= (TTree*)f_data->Get("reducedTree");
	TTree* T_dataSS 	= (TTree*)f_dataSS->Get("reducedTree");
	TTree* T_signal 	= (TTree*)f_signal->Get("Bs2DsMuNuTuple/DecayTree");
	TTree* T_BG1	 	= (TTree*)f_signal->Get("Bs2DsMuNuTuple/DecayTree");

	// We can add more later
	 
	double low = 3000;
	double high = 6000;
	int n_bins = 60;
	

	TH1F* h_Dsst	= new TH1F("h_Dsst", "Dsst", n_bins, low, high);
	TH1F* h_Ds		= new TH1F("h_signal", "Signal", n_bins, low, high);
	TH1F* h_DsSS	= new TH1F("h_DsSS", "DsSS", n_bins, low, high);
	TH1F* h_Data	= new TH1F("h_data", "Data", n_bins, low, high);
	
	T_signal->Draw("Bs_MCORR>>h_Dsst", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433");	
	T_signal->Draw("Bs_MCORR>>h_signal", "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 531");
	T_dataSS->Draw("Bs_MCORR>>h_DsSS", "nsig_sw_H");
	T_data->Draw("Bs_MCORR>>h_data", "nsig_sw_E");

	Frac_Class* fitter = new Frac_Class();
	
	
	fitter->AddSourceHist( h_Dsst );
	fitter->AddSourceHist( h_Ds );
	fitter->AddSourceHist( h_DsSS );
	
	fitter->AddDataHist( h_Data );
	
	fitter->init_fitter();
	
	fitter->fit();
}
