#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif

#include <iostream>

#include "TFile.h"
#include "TH1F.h"
#include "TCanvas.h"

#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooAddPdf.h"
#include "RooRealVar.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooPlot.h"



void HistFitter(){
	
	TFile* f_histos = TFile::Open("../Tools/Fit_Histograms.root");
	
	//For this initial fitter we will consider only data = (Bs->Ds) + (Bs->Ds*) + same sign, 
	
	TH1F* h_data	= (TH1F*) f_histos->Get("h_data");
	TH1F* h_Ds		= (TH1F*) f_histos->Get("h_Ds");
	TH1F* h_DsStar	= (TH1F*) f_histos->Get("h_DsStar");
	TH1F* h_DsStar0	= (TH1F*) f_histos->Get("h_DsStar0");
	TH1F* h_DsStar1	= (TH1F*) f_histos->Get("h_DsStar1");
	TH1F* h_DsTau	= (TH1F*) f_histos->Get("h_DsTau");
	TH1F* h_SS		= (TH1F*) f_histos->Get("h_SS");
	
	
	RooRealVar* B_MCORR = new RooRealVar("B_MCORR", "Bs Correscted Mass", 0, 100000); // This will "expand" to the histogram boundaries which are 3000, 6000 but I don't want to hard code in case they change
	
	
	RooDataHist* RDH_data	= new RooDataHist("RDH_data1", "Data Histogram of data", *B_MCORR, h_data);
	RooDataHist* RDH_Ds		= new RooDataHist("RDH_data2", "Data Histogram of Signal", *B_MCORR, h_Ds);
	RooDataHist* RDH_DsStar	= new RooDataHist("RDH_data3", "Data Histogram of Ds*", *B_MCORR, h_DsStar);
	RooDataHist* RDH_DsStar0= new RooDataHist("RDH_data4", "Data Histogram of Ds*0", *B_MCORR, h_DsStar0);
	RooDataHist* RDH_DsStar1= new RooDataHist("RDH_data5", "Data Histogram of Ds*1", *B_MCORR, h_DsStar1);
	RooDataHist* RDH_DsTau	= new RooDataHist("RDH_data6", "Data Histogram of Ds Tau", *B_MCORR, h_DsTau);
	RooDataHist* RDH_SS		= new RooDataHist("RDH_data7", "Data Histogram of Same Sign", *B_MCORR, h_SS);
	
	RooHistPdf* PDF_Ds		= new RooHistPdf("PDF_Ds", "Data Histogram of signal", *B_MCORR, *RDH_Ds);
	RooHistPdf* PDF_DsStar	= new RooHistPdf("PDF_DsStar", "Data Histogram of Ds*", *B_MCORR, *RDH_DsStar);
	RooHistPdf* PDF_DsStar0	= new RooHistPdf("PDF_DsStar0", "Data Histogram of Ds*", *B_MCORR, *RDH_DsStar0);
	RooHistPdf* PDF_DsStar1	= new RooHistPdf("PDF_DsStar1", "Data Histogram of Ds*", *B_MCORR, *RDH_DsStar1);
	RooHistPdf* PDF_DsTau	= new RooHistPdf("PDF_DsTau", "Data Histogram of Ds Tau", *B_MCORR, *RDH_DsTau);
	RooHistPdf* PDF_SS		= new RooHistPdf("PDF_SS", "Data Histogram of Same Sign", *B_MCORR, *RDH_SS);
	
	double n_data = RDH_data->sumEntries();
	
	RooRealVar* n_Ds		= new RooRealVar("n_Ds", "Number of Ds events", n_data * 0.25, 0, n_data);
	RooRealVar* n_DsStar	= new RooRealVar("n_DsStar", "Number of Ds* events", n_data * 0.6, 0, n_data);
	RooRealVar* n_DsStar0	= new RooRealVar("n_DsStar0", "Number of Ds* events", n_data * 0.1, 0, n_data);
	RooRealVar* n_DsStar1	= new RooRealVar("n_DsStar1", "Number of Ds* events", n_data * 0.1, 0, n_data);
	RooRealVar* n_DsTau		= new RooRealVar("n_DsTau", "Number of DsTau events", n_data * 0.1, 0, n_data);
	RooRealVar* n_SS		= new RooRealVar("n_SS", "Number of Same Sign Events events", n_data * 0.15, 0, n_data);
	
	RooAddPdf* Fit_Model = new RooAddPdf("Fit_Model", "Fit Model", RooArgList(*PDF_Ds, *PDF_DsStar, *PDF_DsStar0, *PDF_DsStar1, *PDF_DsTau, *PDF_SS), RooArgList(*n_Ds, *n_DsStar, *n_DsStar0, *n_DsStar1, *n_DsTau, *n_SS) );
	
	Fit_Model->fitTo( *RDH_data );
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	RooPlot* M_frame = B_MCORR->frame();
	
	RDH_data->plotOn(M_frame);
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1, *PDF_DsStar0, *PDF_DsTau, *PDF_SS, *PDF_Ds, *PDF_DsStar	)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(3), RooFit::DrawOption("F"));
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1, *PDF_DsStar0, *PDF_DsTau, *PDF_SS, *PDF_Ds				)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(2), RooFit::DrawOption("F"));
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1, *PDF_DsStar0, *PDF_DsTau, *PDF_SS						)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(1), RooFit::DrawOption("F"));
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1, *PDF_DsStar0, *PDF_DsTau									)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(4), RooFit::DrawOption("F"));
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1, *PDF_DsStar0												)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(5), RooFit::DrawOption("F"));
	Fit_Model->plotOn(M_frame, RooFit::Components(RooArgList(*PDF_DsStar1															)), RooFit::LineColor(kBlack), RooFit::LineStyle(1), RooFit::FillColor(6), RooFit::DrawOption("F"));
	RDH_data->plotOn(M_frame);
	
	M_frame->Draw();
	

}





