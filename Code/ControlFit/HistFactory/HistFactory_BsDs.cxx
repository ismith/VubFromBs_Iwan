

#include <iostream>
#include <stdio.h>

#include "TRandom3.h"
#include "TCanvas.h"
#include "TDatime.h"
#include "TStopwatch.h"
#include "TLegend.h"
#include "TIterator.h"
#include "TH3.h"
#include "TLatex.h"
#include "THStack.h"
#include "TPaveText.h"

#include "RooChi2Var.h"
#include "RooAbsData.h"
#include "RooRealSumPdf.h"
#include "RooPoisson.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooMCStudy.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooHistPdf.h"
#include "RooSimultaneous.h"
#include "RooExtendPdf.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooMsgService.h"
#include "RooParamHistFunc.h"
#include "RooHist.h"
#include "RooRandom.h"

#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/MinNLLTestStat.h"

#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/HistFactoryModelUtils.h"
#include "RooStats/HistFactory/RooBarlowBeestonLL.h"
using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;


map<std::string, pair<double, double>> HistFactory_BsDs(const char* file_name = "../Tools/Source_Histograms.root") {


   /*To build the model we need information:
    * 
    * The number of events in the data -> Make reasonable assumptions for the number of component events.
    * 
    */
    
    

    
   TFile* f_hist = TFile::Open(file_name, "READ");
    
   TH1F* h_data = (TH1F*)f_hist->Get("Data")->Clone();
   h_data->SetDirectory(0);
    
   double n_data = h_data->Integral();
   
   double f_Ds		= 0.3203;
   double f_DsStar	= 0.6015335;
   double f_DsStar1	= 0.005;
   double f_DsTau	= 0.0166;
   double f_DD		= 0.0676054;
   

   //Now generate a vector of pairs to save results in
   

   // Create a simple 1-channel model
   // using c++ and ROOT
   RooStats::HistFactory::Measurement meas("Bs2DsTest", "Bs2DsTest");

   meas.SetOutputFilePrefix("results/Bs_DsMuNu");
   meas.SetExportOnly(true);

   meas.SetPOI("DsFrac_Yield");

   meas.SetLumi(1.0);
   meas.SetLumiRelErr(0.05);

   RooStats::HistFactory::Channel chan("channel");
   
   chan.SetStatErrorConfig(1e-5,"Pois");
   chan.SetData("Data", file_name);


   RooStats::HistFactory::Sample signal("Dsmunu_norm", "Dsmunu_norm", file_name);
   signal.ActivateStatError();//"h_Ds_err_stat_norm", file_name);
   //signal.AddOverallSys("syst1", 0.5, 1.5 );
   signal.SetNormalizeByTheory(false);
   signal.AddNormFactor("DsFrac_Yield", f_Ds * n_data, 0.0, n_data);
   chan.AddSample(signal);     


   RooStats::HistFactory::Sample background1("DsStarmunu_norm", "DsStarmunu_norm", file_name);
   background1.ActivateStatError();//"h_DsStar_err_stat_norm", file_name);
   //background1.AddOverallSys("syst2", 0.5, 1.5 );
   background1.SetNormalizeByTheory(false);
   background1.AddNormFactor("DsStarFrac_Yield", f_DsStar * n_data, 0.0, n_data);
   chan.AddSample(background1);         


   RooStats::HistFactory::Sample background2("DsXtaunu_norm", "DsXtaunu_norm", file_name);
   background2.ActivateStatError();//"h_DsTau_err_stat", file_name);
   //background2.AddHistoSys("h_DsTauSys","h_DsTau_err_sys_l",file_name,"","h_DsTau_err_sys_h",file_name,"");
   //background2.AddOverallSys("syst3", 0.5, 1.5 );
   background2.SetNormalizeByTheory(false);
   background2.AddNormFactor("DsTau_Yield", f_DsTau * n_data, 0.0, n_data);
   chan.AddSample(background2);  


   RooStats::HistFactory::Sample background3("DsStar0munu_norm", "DsStar0munu_norm", file_name);
   background3.SetNormalizeByTheory(false);
   background3.AddNormFactor("DsStar01Frac_Yield", f_DsStar1 * n_data, 0.0, n_data);
   background3.AddNormFactor("DsStar0_frac", 0.5937, 0.0, n_data);
   chan.AddSample(background3);

   RooStats::HistFactory::Sample background305("DsStar1munu_norm", "DsStar1munu_norm", file_name);
   background305.SetNormalizeByTheory(false);
   background305.AddNormFactor("DsStar01Frac_Yield", f_DsStar1 * n_data, 0.0, n_data);
   background305.AddNormFactor("DsStar1_frac", 0.4062, 0.0, n_data);
   chan.AddSample(background305);     

   //Now look at double D modes. This is a little more complicated:
   
   // Bd->DD (D0munu) 1027491
   // Bd->DD (Dsmunu) 1111256
   
   // Bu->DD (D0munu) 1031400
   // Bu->DD (Dsmunu) 1049703
   
   // Bs->DD (Dsmunu) 1093361


   // We have the following fragmentation fractions from: http://arxiv.org/pdf/1111.2357v2.pdf

   // fs/fd =  0.267
   // fs/fu+fd = 0.134
   // thf. fd = fu
   
   // ->  fs = 0.118 fd = 0.441 fu = 0.441 with 10% uncertainty
   

   RooStats::HistFactory::Sample background4("Bd_DD_norm", "Bd_DD_norm", file_name);
   background4.ActivateStatError();
   background4.AddOverallSys("syst4", 0.9, 1.1 );
   background4.SetNormalizeByTheory(false);
   background4.AddNormFactor("DD_Yield", f_DD * n_data, 0.0, n_data);
   background4.AddNormFactor("fd", 0.441, 0.40, 0.48); //fix this
   chan.AddSample(background4);  

   RooStats::HistFactory::Sample background5("Bu_DD_norm", "Bu_DD_norm", file_name);
   background5.ActivateStatError();
   background5.AddOverallSys("syst5", 0.9, 1.1 );
   background5.SetNormalizeByTheory(false);
   background5.AddNormFactor("DD_Yield", f_DD * n_data, 0.0, n_data);
   background5.AddNormFactor("fu", 0.441, 0.40, 0.48); //fix this
   chan.AddSample(background5); 
   
   RooStats::HistFactory::Sample background6("Bs_DD_norm", "Bs_DD_norm", file_name);
   background6.ActivateStatError();
   background6.AddOverallSys("syst6", 0.9, 1.1 );
   background6.SetNormalizeByTheory(false);
   background6.AddNormFactor("DD_Yield", f_DD * n_data, 0.0, n_data);
   background6.AddNormFactor("fs", 0.118, 0.108, 0.128); //fix this
   chan.AddSample(background6);    
   
   meas.AddChannel(chan);

   meas.CollectHistograms();

   meas.PrintTree();

   RooWorkspace* ws = RooStats::HistFactory::MakeModelAndMeasurementFast(meas);
   
    
  ModelConfig* mc = (ModelConfig*) ws->obj("ModelConfig");
  
  
  //Now fix the paraemeters
  ((RooRealVar*)(mc->GetNuisanceParameters()->find("fu")))->setConstant(true);
  ((RooRealVar*)(mc->GetNuisanceParameters()->find("fd")))->setConstant(true);
  ((RooRealVar*)(mc->GetNuisanceParameters()->find("fs")))->setConstant(true);
  ((RooRealVar*)(mc->GetNuisanceParameters()->find("DsStar0_frac")))->setConstant(true);
  ((RooRealVar*)(mc->GetNuisanceParameters()->find("DsStar1_frac")))->setConstant(true);
  
  
  
  RooSimultaneous* model = (RooSimultaneous*) mc->GetPdf(); //ws->pdf("simPdf");
  RooDataSet* data = (RooDataSet*) ws->data("obsData");



  // Now, extract some information from the ModelConfig
  RooRealVar* poi = (RooRealVar*) mc->GetParametersOfInterest()->createIterator()->Next();
  std::cout << "Param of Interest: " << poi->GetName() << std::endl;  


  // Okay, at this point, we're ready to create a modified
  // version of the simultaneous pdf:
  // We simply create a "HistFactorySimultaneous"
  // using our model in the constructor
  HistFactorySimultaneous* model_hf = new HistFactorySimultaneous( *model );
  
  // Using this, we call the standard createNLL method
  // which has been overloaded in HistFactorySimultaneous
  // This version of the NLL is aware of the statistical
  // uncertainty parameters and minimizes them analytically
  // when it is evaluated
   RooAbsReal* nll_hf = model_hf->createNLL( *data/*,Offset(kTRUE)*/ );   //Add external constarints in the argument. 
   
   RooMinuit* minuit_hf = new RooMinuit( *nll_hf ) ;
   minuit_hf->setErrorLevel(0.5);
   minuit_hf->setStrategy(2);
   
   std::cout << "\n\n\n*********************************************" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*    Running Hesse For The First Time       *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*********************************************\n\n\n" << std::endl;
  
   int status_hesse_0 = minuit_hf->hesse(); //try different minimisers
   
   std::cout << "\n\n\n*********************************************" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*   Running Migrad For The First Time       *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*********************************************\n\n\n" << std::endl;
   int status_migrad = minuit_hf->migrad(); //try different minimisers
   int status_simplex = -1;
   if ( status_migrad == 4 ){
	   
	    std::cout << "\n\n\n*********************************************" << std::endl;
	    std::cout << "*                                           *" << std::endl;
	    std::cout << "*         Migrad Failed to Converge         *" << std::endl;
	    std::cout << "*   Running Simplex For The First Time      *" << std::endl;
	    std::cout << "*                                           *" << std::endl;
	    std::cout << "*                                           *" << std::endl;
	    std::cout << "*********************************************\n\n\n" << std::endl;
  
	   status_simplex = minuit_hf->simplex();
	
   }
   std::cout << "\n\n\n*********************************************" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*    Running Hesse For The Second Time      *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*********************************************\n\n\n" << std::endl;  
   
   int status_hesse_1 = minuit_hf->hesse(); //try different minimisers
   
   std::cout << "\n\n\n*********************************************" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*    Running Minos For The First Time       *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*                                           *" << std::endl;
   std::cout << "*********************************************\n\n\n" << std::endl;
   
   int status_minos = minuit_hf->minos(RooArgSet(*poi));
   
   
   std::cout << "Hesse Status: " << status_hesse_0 << std::endl;
   std::cout << "Migrad Status: " << status_migrad << std::endl;
   std::cout << "Hesse Status: " << status_hesse_1 << std::endl;
   std::cout << "Minos Status: " << status_minos << std::endl;
//   RooFitResult* fit_result = minuit_hf->fit("smh"); //try different minimisers
   

   
   
   
   //RooProfileLL* profile_hf = (RooProfileLL*) nll_hf->createProfile(*poi);
   //std::cout << profile_hf->getVal() << std::endl;   

   
   
   //std::cout << "Param of Interest: " << poi->GetName() << std::endl;   
   //std::cout << "Param of Interest Value: " << poi->getVal() << std::endl;   
   //std::cout << "Param of Interest Error: " << poi->getError() << std::endl;   
   
   vector<RooRealVar*> NuisanceVars;
   NuisanceVars.push_back( (RooRealVar*) mc->GetParametersOfInterest()->createIterator()->Next() );
   NuisanceVars.push_back( (RooRealVar*)(mc->GetNuisanceParameters()->find("DsStarFrac_Yield")) );
   NuisanceVars.push_back( (RooRealVar*)(mc->GetNuisanceParameters()->find("DsStar01Frac_Yield")) );
   NuisanceVars.push_back( (RooRealVar*)(mc->GetNuisanceParameters()->find("DD_Yield")) );
   NuisanceVars.push_back( (RooRealVar*)(mc->GetNuisanceParameters()->find("DsTau_Yield")) );
   
   
      
   map<std::string, pair<double, double>> result_tuple;
   
   //Save the results for returning
   
   int n_frac = 0;
   for( RooRealVar* const& nuispar: NuisanceVars){
      std::cout << nuispar->GetName() << "\t" << nuispar->getVal() << "\t" << nuispar->getError() << "\t" << nuispar->getError() / nuispar->getVal() << std::endl;
      pair<double, double> temp_pair((double)nuispar->getVal(), (double)nuispar->getError());
      
      result_tuple[nuispar->GetName()] = temp_pair;
      
      n_frac++;
   
   }
   //std::cout << fit_result->status() << std::endl;
   
   
   pair<double, double> temp_pair(status_hesse_0, 0);
   result_tuple["Status_Hesse_0"] = temp_pair;
   temp_pair.first = status_migrad;
   result_tuple["Status_Migrad"] = temp_pair;
   temp_pair.first = status_simplex;
   result_tuple["Status_Simplex"] = temp_pair;
   temp_pair.first = status_hesse_1;
   result_tuple["Status_Hesse_1"] = temp_pair;
   temp_pair.first = status_minos;
   result_tuple["Status_Minos"] = temp_pair;
   
   
   
   TH1F* h_Dsmunu_norm = (TH1F*)f_hist->Get("Dsmunu_norm")->Clone();
   TH1F* h_DsStarmunu_norm = (TH1F*)f_hist->Get("DsStarmunu_norm")->Clone();
   TH1F* h_DsStar0munu_norm = (TH1F*)f_hist->Get("DsStar0munu_norm")->Clone();
   TH1F* h_DsStar1munu_norm = (TH1F*)f_hist->Get("DsStar1munu_norm")->Clone();
   TH1F* h_Bd_DD_norm = (TH1F*)f_hist->Get("Bd_DD_norm")->Clone();
   TH1F* h_Bu_DD_norm = (TH1F*)f_hist->Get("Bu_DD_norm")->Clone();
   TH1F* h_Bs_DD_norm = (TH1F*)f_hist->Get("Bs_DD_norm")->Clone();
   TH1F* h_DsXtaunu_norm = (TH1F*)f_hist->Get("DsXtaunu_norm")->Clone();
   
   h_Dsmunu_norm->SetDirectory(0);
   h_DsStarmunu_norm->SetDirectory(0);
   h_DsStar0munu_norm->SetDirectory(0);
   h_DsStar1munu_norm->SetDirectory(0);
   h_Bd_DD_norm->SetDirectory(0);
   h_Bu_DD_norm->SetDirectory(0);
   h_Bs_DD_norm->SetDirectory(0);
   h_DsXtaunu_norm->SetDirectory(0);
   
      
   
   
   //Now do some plotting:
   
   vector<TH1F*> plot_hists;
   
   plot_hists.push_back( h_Dsmunu_norm );
   plot_hists.push_back( h_DsStarmunu_norm );
   plot_hists.push_back( new TH1F() );
   *plot_hists.back() = 0.5937* *h_DsStar0munu_norm + 0.40602* *h_DsStar1munu_norm;
   plot_hists.push_back( new TH1F() );
   *plot_hists.back() = 0.441* *h_Bd_DD_norm + 0.441* *h_Bu_DD_norm + 0.118* *h_Bs_DD_norm;
   plot_hists.push_back( h_DsXtaunu_norm );
   
   
   
   
   //h_data->Draw();
   
   THStack* stack = new THStack("Stack", "Fit to data");
   TH1F* h_stack = (TH1F*)h_data->Clone();
   h_stack->Reset();
   h_stack->SetTitle("Residuals");
   for (int x = n_frac-1; x >=0; x--){
      *plot_hists.at(x) = NuisanceVars.at(x)->getVal() * *plot_hists.at(x);
      plot_hists.at(x)->SetFillColor(5-x);
      plot_hists.at(x)->Sumw2(false);
      stack->Add( plot_hists.at(x) );
      *h_stack = *h_stack + *plot_hists.at(x);
   }
   TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);
   
   TPad *pad1 = new TPad("pad1", "The pad 80% of the height",0.0,0.3,1.0,1.0);
   pad1->cd();
   stack->Draw();
   h_data->Draw("SAME");
   
   TPad *pad2 = new TPad("pad2", "The pad 20% of the height",0.0,0.0,1.0,0.3);
   pad2->cd();
   h_stack->Add(h_data, -1.0);
   h_stack->Draw();
   
   c1->cd();
   pad1->Draw();
   pad2->Draw();
   
   return result_tuple;
  
 }



void SaveSourceHistograms(const char* file_name = "../Tools/Source_Histograms.root", std::string saveas = ""){
	
	TCanvas* c1 = new TCanvas("c1", "c1", 2400, 1600);
	
	c1->Divide(3, 2);
	
	THStack* stack_DsStar = new THStack("StackDsStar", "Further Excited D Modes - MC");
	THStack* stack_DD = new THStack("StackDD", "Double D Modes - MC");
	
	TFile* f_in = TFile::Open( file_name, "READ");
	
	TH1F* h_Data = (TH1F*)f_in->Get("Data")->Clone();
	h_Data->SetTitle("D_{s} Yield - Data");
	h_Data->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	h_Data->SetStats(false);
	c1->cd(1);
	h_Data->Draw();
	TPaveText *pt = new TPaveText(.5,.5,1,1);
	pt->AddText("Data");
	pt->Draw();
	
	TH1F* h_Ds = (TH1F*)f_in->Get("Dsmunu_norm")->Clone();
	h_Ds->SetTitle("D_{s}#mu#nu Signal - Monte Carlo");
	h_Ds->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	h_Ds->SetStats(false);
	c1->cd(2);
	h_Ds->Draw();

	
	TH1F* h_DsStar = (TH1F*)f_in->Get("DsStarmunu_norm")->Clone();
	h_DsStar->SetTitle("D_{s}*#mu#nu Background - MC");
	h_DsStar->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	h_DsStar->SetStats(false);
	c1->cd(3);
	h_DsStar->Draw();
	
	TH1F* h_DsStar0 = (TH1F*)f_in->Get("DsStar0munu_norm")->Clone();
	TH1F* h_DsStar1 = (TH1F*)f_in->Get("DsStar1munu_norm")->Clone();
	h_DsStar0->SetTitle("Further excited D_{s} - MC");
	h_DsStar0->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	*h_DsStar0 = 0.5937* *h_DsStar0;
	*h_DsStar1 = 0.4060* *h_DsStar1;
	stack_DsStar->Add(h_DsStar0);
	stack_DsStar->Add(h_DsStar1);
	c1->cd(4);
	stack_DsStar->Draw();	
	stack_DsStar->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	stack_DsStar->Draw();	
	
	TH1F* h_Dstau = (TH1F*)f_in->Get("DsXtaunu_norm")->Clone();
	h_Dstau->SetTitle("D_{s}#tau#nu Background - MC");
	h_Dstau->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	h_Dstau->SetStats(false);
	c1->cd(5);
	h_Dstau->Draw();	
	
	TH1F* h_Bu_DD = (TH1F*)f_in->Get("Bu_DD_norm")->Clone();
	TH1F* h_Bd_DD = (TH1F*)f_in->Get("Bd_DD_norm")->Clone();
	TH1F* h_Bs_DD = (TH1F*)f_in->Get("Bs_DD_norm")->Clone();
	h_Bu_DD->SetTitle("Double Charm Modes - MC");
	h_Bu_DD->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	*h_Bu_DD = 0.441* *h_Bu_DD;
	*h_Bd_DD = 0.441* *h_Bd_DD;
	*h_Bs_DD = 0.118* *h_Bs_DD;
	stack_DD->Add(h_Bs_DD);
	stack_DD->Add(h_Bu_DD);
	stack_DD->Add(h_Bd_DD);
	c1->cd(6);
	stack_DD->Draw();	
	stack_DD->GetXaxis()->SetTitle("B_{s} Corrected Mass [GeV]");
	stack_DD->Draw();	
	
	if ( saveas != ""){
		c1->Print( ( saveas+".pdf").c_str() );
		c1->Print( ( saveas+".C").c_str() );
		
	}
	
	}
