
#include "HistFactory_BsDs.cxx"
#include "../Tools/ToyHistos.cxx"

#include "TH1F.h"
#include "TH2F.h"
#include <random>


#include "TTree.h"
void Bs2Ds_BiasTest(){
	
	
	int n_toys = 500;
	
	
	std::random_device rd;
	std::mt19937 gen(rd());

	double n_Ds		= 781318;
	double n_DsStar	= 1365960;
	double n_DsTau	= 105842;
	double n_DsStar1= 215428;
	double n_DD		= 248042;
	
	
	double in_Ds		= -1000;
	double in_DsStar	= -1000;
	double in_DsTau		= -1000;
	double in_DsStar1	= -1000;
	double in_DD		= -1000;
	
	double n_Ds_fit		= -1000;
	double n_DsStar_fit	= -1000;
	double n_DsTau_fit	= -1000;
	double n_DsStar1_fit= -1000;
	double n_DD_fit		= -1000;
	
	double n_Ds_fit_err		= -1000;
	double n_DsStar_fit_err	= -1000;
	double n_DsTau_fit_err	= -1000;
	double n_DsStar1_fit_err= -1000;
	double n_DD_fit_err		= -1000;	
	
	int status_hesse_0 = -1000;
	int status_migrad = -1000;
	int status_simplex = -1000;
	int status_hesse_1 = -1000;
	int status_minos = -1000;
	
	int toy_number = -1000;
	
	std::normal_distribution<double> dist_Ds(n_Ds, sqrt(n_Ds));
	std::normal_distribution<double> dist_DsStar(n_DsStar, sqrt(n_DsStar));
	std::normal_distribution<double> dist_DsTau(n_DsTau, sqrt(n_DsTau));
	std::normal_distribution<double> dist_DsStar1(n_DsStar1, sqrt(n_DsStar1) );
	std::normal_distribution<double> dist_DD(n_DD, sqrt(n_DD));
	
	
	TFile* f_Tree = TFile::Open("../Tools/Toy_Results.root", "RECREATE");
	TTree* resultsTree = new TTree("Toy_results", "Toy Results");
	
	
	
	resultsTree->Branch("n_Ds",		&in_Ds,		"n_Ds/D");
	resultsTree->Branch("n_DsStar",	&in_DsStar,	"n_DsStar/D");
	resultsTree->Branch("n_DsTau",	&in_DsTau,	"n_DsTau/D");
	resultsTree->Branch("n_DsStar1",&in_DsStar1,"n_DsStar1/D");
	resultsTree->Branch("n_DD",		&in_DD,		"n_DD/D");
	
	resultsTree->Branch("n_Ds_fit",		&n_Ds_fit,		"n_Ds_fit/D");
	resultsTree->Branch("n_DsStar_fit",	&n_DsStar_fit,	"n_DsStar_fit/D");
	resultsTree->Branch("n_DsTau_fit",	&n_DsTau_fit,		"n_DsTau_fit/D");
	resultsTree->Branch("n_DsStar1_fit",&n_DsStar1_fit,		"n_DsStar1_fit/D");
	resultsTree->Branch("n_DD_fit",		&n_DD_fit,			"n_DD_fit/D");
	
	resultsTree->Branch("n_Ds_fit_err",		&n_Ds_fit_err,		"n_Ds_fit_err/D");
	resultsTree->Branch("n_DsStar_fit_err",	&n_DsStar_fit_err,	"n_DsStar_fit_err/D");
	resultsTree->Branch("n_DsTau_fit_err",	&n_DsTau_fit_err,			"n_DsTau_fit_err/D");
	resultsTree->Branch("n_DsStar1_fit_err",&n_DsStar1_fit_err,		"n_DsStar1_fit_err/D");
	resultsTree->Branch("n_DD_fit_err",		&n_DD_fit_err,			"n_DD_fit_err/D");
	
	resultsTree->Branch("Status_Hesse_0", &status_hesse_0, "Status_Hesse_0/I");
	resultsTree->Branch("Status_Migrad", &status_migrad, "Status_Migrad/I");
	resultsTree->Branch("Status_Simplex", &status_simplex, "Status_Simplex/I");
	resultsTree->Branch("Status_Hesse_1", &status_hesse_1, "Status_Hesse_1/I");
	resultsTree->Branch("Status_Minos", &status_minos, "Status_Minos/I");
	
	resultsTree->Branch("Toy_Number", &toy_number, "Toy_Number/I");
	
	for (int toy_num = 0; toy_num < n_toys; toy_num++){
	
		in_Ds		= dist_Ds(gen);
		in_DsStar	= dist_DsStar(gen);
		in_DsTau	= dist_DsTau(gen);
		in_DsStar1	= dist_DsStar1(gen);
		in_DD		= dist_DD(gen);

		
		std::string toy_name = "../Tools/Toy_Histograms.root";
		
		
		ToyHistos("../Tools/Source_Histograms.root", toy_name, 
				"Dsmunu_norm", in_Ds,
				"DsStarmunu_norm", in_DsStar,
				"DsStar0munu_norm", in_DsStar1 * 0.5937,
				"DsStar1munu_norm", in_DsStar1 * 0.4063,
				"DsXtaunu_norm", in_DsTau,
				"Bd_DD_norm", in_DD * 0.441,
				"Bu_DD_norm", in_DD * 0.441,
				"Bs_DD_norm", in_DD * 0.118);
				
		
		map<std::string, pair<double, double>> fit_results = HistFactory_BsDs( toy_name.c_str() );
		
		
		n_Ds_fit		= fit_results["DsFrac_Yield"].first;
		n_DsStar_fit	= fit_results["DsStarFrac_Yield"].first;
		n_DsStar1_fit	= fit_results["DsStar01Frac_Yield"].first;
		n_DsTau_fit		= fit_results["DsTau_Yield"].first;
		n_DD_fit		= fit_results["DD_Yield"].first;
		
		n_Ds_fit_err		= fit_results["DsFrac_Yield"].second;
		n_DsStar_fit_err	= fit_results["DsStarFrac_Yield"].second;
		n_DsStar1_fit_err	= fit_results["DsStar01Frac_Yield"].second;
		n_DsTau_fit_err		= fit_results["DsTau_Yield"].second;
		n_DD_fit_err		= fit_results["DD_Yield"].second;

		status_hesse_0 = fit_results["Status_Hesse_0"].first;
		status_migrad  = fit_results["Status_Migrad"].first;
		status_simplex  = fit_results["Status_Simplex"].first;
		status_hesse_1 = fit_results["Status_Hesse_1"].first;
		status_minos   = fit_results["Status_Minos"].first;
		
		toy_number=toy_num;
		
		resultsTree->Fill();
	}
	f_Tree->cd();
	resultsTree->Write();
	f_Tree->Close();
	
	
	
}



void PlotCorrelations(const char* cut_string = ""){
	
	
	TFile* f_results = TFile::Open("../Tools/Toy_Results.root", "READ");
	TTree* resultsTree = (TTree*)f_results->Get("Toy_results");
	
	
	vector<std::string> names;
	
	names.push_back("Ds");
	names.push_back("DsStar");
	names.push_back("DsStar1");
	names.push_back("DsTau");
	names.push_back("DD");
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	TH2F* h2 = new TH2F("h2", "h2", 10000, -50000, 50000, 10000, -50000, 50000);
	
	std::cout << "\t";
	for (unsigned int p2 = 0; p2 < names.size(); p2++)
		std::cout << names.at(p2) << "\t\t";
	
	for (unsigned int p1 = 0; p1 < names.size(); p1++){
		std::cout << "\n" << names.at(p1) << "\t";
		for (unsigned int p2 = 0; p2 < names.size(); p2++){
		
		std::string p1_s = names.at(p1);
		std::string p2_s = names.at(p2);
		
		std::string Draw_String = "n_" + p1_s + "_fit-n_" + p1_s + ":n_" + p2_s + "_fit-n_" + p2_s + ">>h2";
		
		resultsTree->Draw( Draw_String.c_str(), cut_string );
		
		
		std::cout << round(h2->GetCorrelationFactor()*1000)/1000 << "\t\t";
		
		
		}
	}
	
	std::cout << std::endl;
	
	
	delete h2;
	delete c1;

	}


void PlotPulls(const char* file_name = "../Tools/Toy_Results.root", const char* cut_string = ""){
	
	
	TFile* f_toys = TFile::Open(file_name, "READ");
	TTree* T_result = (TTree*)f_toys->Get("Toy_results");
	
	TCanvas* c1 = new TCanvas("c1", "c1", 2560, 1600);
	TCanvas* c2 = new TCanvas("c2", "c2", 2560, 1600);
	
	c1->Divide(3, 2);
	c2->Divide(3, 2);
	
	vector<std::string> names;
	
	names.push_back("Ds");
	names.push_back("DsStar");
	names.push_back("DsStar1");
	names.push_back("DsTau");
	names.push_back("DD");
	
	for (unsigned int p1 = 0; p1 < names.size(); p1++){
		c1->cd(p1+1);
		TH1F* h_resid = new TH1F("h_resid", "h_resid", 50, -40000, 40000);
		std::string p1_s = names.at(p1);
		std::string Draw_String = "n_" + p1_s + "_fit-n_" + p1_s + ">>h_resid";
		
		h_resid->SetTitle( p1_s.c_str() );
		h_resid->GetXaxis()->SetTitle("n_{Fitted} - n_{Generated}");
		h_resid->SetStats(false);
		T_result->Draw( Draw_String.c_str() );
		
		
		c2->cd(p1+1);
		
		TH1F* h_pull = new TH1F("h_pull", "h_pull", 50, -10, 10);
		Draw_String = "(n_" + p1_s + "_fit-n_" + p1_s + ")/n_" + p1_s + "_fit_err>>h_pull";
		
		
		h_pull->SetTitle( p1_s.c_str() );
		h_pull->GetXaxis()->SetTitle("(n_{Fitted} - n_{Generated})/#sigma_{Fitted}");
		h_pull->SetStats(false);
		c2->cd(p1+1);
				
		T_result->Draw( Draw_String.c_str() );		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
