#include <sstream>
#include <iostream>
#include <vector>

#include "TH1F.h"
#include "TFractionFitter.h"
#include "THStack.h"

#include "TTree.h"
#include "TFile.h"

#include "TCanvas.h"

using namespace std;


void ConvergenceTest(bool saveplots = false){
	
	TFile* f_signal	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/Bs2DsMuNuMC_CONTROL_trimmed_norm.root"); // This contains Bs->Ds(*)
	TFile* f_BG1	= TFile::Open("/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_DsMuNu/DTT_Bs2DsstDsst_trimmed_norm.root"); // BsToDsDs*
	
	TTree* T_signal = (TTree*)f_signal->Get("reducedTree");
	TTree* T_BDD	= (TTree*)f_signal->Get("reducedTree");
	
	int n_BD = 		T_signal -> GetEntries( "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 531" );
	int n_BDst = 	T_signal -> GetEntries( "abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433" );
	int n_BDD = 	T_BDD	 -> GetEntries();

	int nev = min( n_BD, n_BDst);
		//nev = min( nev, n_BDD);

	std::cout << n_BD << "  " << n_BDst << "  " << n_BDD << std::endl; 
	TFile* f_temp = TFile::Open("ROOT_Is_Stupid.root");
	TTree* T_BD		= T_signal->CopyTree("abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 531", "");
	TTree* T_BDst	= T_signal->CopyTree("abs(Ds_TRUEID) == 431 && abs(Ds_MC_MOTHER_ID) == 433", "");
	

	int n_bins = 40;
	double bin_low = 3000, bin_high = 6000;

	
	double Bs_MCORR_BD;		T_BD  -> SetBranchAddress("Bs_MCORR", &Bs_MCORR_BD);
	double Bs_MCORR_BDst;	T_BDst-> SetBranchAddress("Bs_MCORR", &Bs_MCORR_BDst);
	double Bs_MCORR_BDD;	T_BDD -> SetBranchAddress("Bs_MCORR", &Bs_MCORR_BDD);

						
	TH1F* h_BD_fit = new TH1F("h_BD_fit", "B To D for reference fit", n_bins, bin_low, bin_high);	
	TH1F* h_BDst_fit = new TH1F("h_BDst_fit", "B To Dst for reference fit", n_bins, bin_low, bin_high);
	TH1F* h_BDD_fit = new TH1F("h_BDD_fit", "B To D D for reference fit", n_bins, bin_low, bin_high);

	int n_hist_fit = 0;
	for ( int ev = 0; ev < nev; ev++ ){
		T_BD->GetEntry(ev);						
		T_BDst->GetEntry(ev);						
		T_BDD->GetEntry(ev);						
		if(ev % 2 == 0){
			h_BD_fit->Fill( Bs_MCORR_BD );				
			h_BDst_fit->Fill( Bs_MCORR_BDst );				
			h_BDD_fit->Fill( Bs_MCORR_BDD );	
			n_hist_fit++;
		}
	}

	double max_h_val = max( h_BD_fit->GetMaximum(), h_BDst_fit->GetMaximum() );
	max_h_val = max( max_h_val, h_BDD_fit->GetMaximum() );

	const int n_val = 11;

	vector<double> res0;
	vector<double> res1;
	vector<double> res2;
	vector<double> res0_e;
	vector<double> res1_e;
	vector<double> res2_e;
	vector<double> f_0;
	vector<double> f_1;
	vector<double> f_2;
	

	int it = 0;
	for( double f_BD = 0.3; f_BD <= 0.4; f_BD += 0.1){ 
		for( double f_BDst = 0.6; f_BDst <= 1-f_BD; f_BDst += 0.1){
			std::cout << "Filling Histogram Number: " << it << std::endl;
			
			double f_BDD = 1 - f_BD - f_BDst;
			
			/* Use a neat little trick to get the maximum number of events:
			 * One dataset will always be used in full
			 * So  N( In tree X ) / f_x will be the number of events if the full tree X dataset is used.
			 * Take the minimum value as the smallest dataset.
			 */
			double n_1 = (n_BD   - n_hist_fit) / f_BD;
			double n_2 = (n_BDst - n_hist_fit) / f_BDst;
			double n_3 = (n_BDD  - n_hist_fit) / f_BDD;
			
			double n_dat = min(  n_1, n_2);
				n_dat = min(  n_dat, n_3);
				
			
			
			
			TH1F* h_B_dat = new TH1F("h_B_dat", "Pseudodata histogram", n_bins, bin_low, bin_high);
								
			TH1F* h_B_dat0 = new TH1F("h_B_dat0", "Pseudodata histogram", n_bins, bin_low, bin_high);
			TH1F* h_B_dat1 = new TH1F("h_B_dat1", "Pseudodata histogram", n_bins, bin_low, bin_high);
			TH1F* h_B_dat2 = new TH1F("h_B_dat2", "Pseudodata histogram", n_bins, bin_low, bin_high);
			
			
			int c1 =0,  c2 =0,  c3 =0;		
			for ( double ev_r = 0; ev_r < n_dat * f_BD; ev_r++ ){
				T_BD->GetEntry(ev_r);						
				h_B_dat->Fill( Bs_MCORR_BD );				
				h_B_dat0->Fill( Bs_MCORR_BD );
				c1++;
		
					
			}			
			for ( double ev_r = 0; ev_r < n_dat * f_BDst; ev_r++ ){
				T_BDst->GetEntry(ev_r);						
				h_B_dat->Fill( Bs_MCORR_BDst );				
				h_B_dat1->Fill( Bs_MCORR_BDst );
				c2++;
		
			}
			for ( double ev_r = 0; ev_r < n_dat * f_BDD; ev_r++ ){
				T_BDD->GetEntry(ev_r);						
				h_B_dat->Fill( Bs_MCORR_BDD );				
				h_B_dat2->Fill( Bs_MCORR_BDD );		
				c3++;
						
			}			
			
			std::cout << "**************************************\n\n\n\n\n\n\n\n\n\n" << c1 << "  " << c2<< "  " << c3<< "**************************************\n\n\n\n\n\n\n\n\n\n" << std::endl;
			int nhist = 3;
			TObjArray *mc = new TObjArray(nhist);
			mc->Add(h_BD_fit);
			mc->Add(h_BDst_fit);
			mc->Add(h_BDD_fit);
	

			TFractionFitter* fit = new TFractionFitter(h_B_dat, mc, "V"); // initialise
			fit->Constrain(0,0.0,1.0); 
			fit->Constrain(1,0.0,1.0); 
			fit->Constrain(2,0.0,1.0);    
			Int_t status = fit->Fit();               // perform the fit

			f_0.push_back(f_BD);
			f_1.push_back(f_BDst);
			f_2.push_back(1 - f_BD - f_BDst);
			
			res0.push_back(0); res0_e.push_back(0);
			res1.push_back(0); res1_e.push_back(0);
			res2.push_back(0); res2_e.push_back(0);

			fit->GetResult(0, res0.back(), res0_e.back());
			fit->GetResult(1, res1.back(), res1_e.back());
			fit->GetResult(2, res2.back(), res2_e.back());

			std::cout << "B to D  " << int(res0.at(it) * 1000 + 0.5)/1000.0 << " vs " << f_0.at(it) << "    ";
			std::cout << "B to D* " << int(res1.at(it) * 1000 + 0.5)/1000.0 << " vs " << f_1.at(it) << "    "; 
			std::cout << "B to DD " << int(res2.at(it) * 1000 + 0.5)/1000.0 << " vs " << int(f_2.at(it)*100+0.5)/100.0 << std::endl; 

			if ( saveplots ){
				h_B_dat->Sumw2();
				//h_B_dat->Scale( n_hist_fit / h_B_dat->Integral(1, n_bins) );
				
				THStack* stack_fit =  new THStack("stack_fit", "Solutions to fit");
				TH1F h_f_0 = f_0.at(it) * (*h_BD_fit);
				h_f_0.SetFillColor(2);
				stack_fit->Add( &h_f_0 );

				TH1F h_f_1 = f_1.at(it) * (*h_BDst_fit);
				h_f_1.SetFillColor(3);
				stack_fit->Add( &h_f_1 );

				TH1F h_f_2 = f_2.at(it) * (*h_BDD_fit);
				h_f_2.SetFillColor(4);
				stack_fit->Add( &h_f_2 );
				
				THStack* stack_dat = new THStack("stack_dat", "Pseudodata Contributions");
				
				h_B_dat0->Scale( n_hist_fit / n_dat );
				h_B_dat1->Scale( n_hist_fit / n_dat);
				h_B_dat2->Scale( n_hist_fit / n_dat );
				
				stack_dat ->Add(h_B_dat0);
				stack_dat ->Add(h_B_dat1);
				stack_dat ->Add(h_B_dat2);
				
				TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);
				
				stack_fit->SetMaximum(max_h_val * 1.05 );
				stack_fit->Draw();
				stack_dat->Draw("SAME,elp");			
				c1->Print( ("plots/Fit_" + std::to_string(it) + ".pdf").c_str() );
				
				delete stack_fit; delete stack_dat;
				delete c1;
			}
			delete h_B_dat; delete h_B_dat0; delete h_B_dat1; delete h_B_dat2;
			delete mc;
			it++;
		}
	}
	
	std::cout << "\n\n\n\n";	
	for (unsigned int it = 0; it < res0.size(); it++){
		std::cout << "B to D  " << int(res0.at(it) * 1000 + 0.5)/1000.0 << " vs " << f_0.at(it) << "    ";
		std::cout << "B to D* " << int(res1.at(it) * 1000 + 0.5)/1000.0 << " vs " << f_1.at(it) << "    "; 
		std::cout << "B to DD " << int(res2.at(it) * 1000 + 0.5)/1000.0 << " vs " << int(f_2.at(it)*100+0.5)/100.0 << std::endl; 

		
	}
	it = 0;
	for( double f_BD = 0.3; f_BD <= 0.4; f_BD += 0.1){ 

		std::cout << std::endl;
		for( double f_BDst = 0.6; f_BDst <= 1-f_BD; f_BDst += 0.1){
			std::cout << int(res0.at(it) * 1000 + 0.5)/1000.0 << "  " << int(res1.at(it) * 1000 + 0.5)/1000.0  << "  " << int(res2.at(it) * 1000 + 0.5)/1000.0  << ",  ";
			it++;
		}
	}
	
}
