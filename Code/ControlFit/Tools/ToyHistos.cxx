#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"

void ToyHistos( std::string in_file = "Source_Histograms.root",std::string out_file = "Toy_Histograms.root",
                const char* n_hist_0 = "", int n_ev_0 = 0,
                const char* n_hist_1 = "", int n_ev_1 = 0,
                const char* n_hist_2 = "", int n_ev_2 = 0,
                const char* n_hist_3 = "", int n_ev_3 = 0,
                const char* n_hist_4 = "", int n_ev_4 = 0,
                const char* n_hist_5 = "", int n_ev_5 = 0,
                const char* n_hist_6 = "", int n_ev_6 = 0,
                const char* n_hist_7 = "", int n_ev_7 = 0,
                const char* n_hist_8 = "", int n_ev_8 = 0,
                const char* n_hist_9 = "", int n_ev_9 = 0){
	
	vector<const char*> hist_names;
	vector<int> nev;
	vector<TH1F*> Source_Histos;
	int n_hist = 0;
	
	
	if (strlen( n_hist_0 ) != 0){
		hist_names.push_back(  n_hist_0 );
		nev.push_back(n_ev_0);
	}
	if (strlen( n_hist_1 ) != 0){
		hist_names.push_back(  n_hist_1 );
		nev.push_back(n_ev_1);
	}
	if (strlen( n_hist_2 ) != 0){
		hist_names.push_back(  n_hist_2 );
		nev.push_back(n_ev_2);
	}
	if (strlen( n_hist_3 ) != 0){
		hist_names.push_back(  n_hist_3 );
		nev.push_back(n_ev_3);
	}
	if (strlen( n_hist_4 ) != 0){
		hist_names.push_back(  n_hist_4 );
		nev.push_back(n_ev_4);
	}
	if (strlen( n_hist_5 ) != 0){
		hist_names.push_back(  n_hist_5 );
		nev.push_back(n_ev_5);
	}
	if (strlen( n_hist_6 ) != 0){
		hist_names.push_back(  n_hist_6 );
		nev.push_back(n_ev_6);
	}	
	if (strlen( n_hist_7 ) != 0){
		hist_names.push_back(  n_hist_7 );
		nev.push_back(n_ev_7);
	}	
	if (strlen( n_hist_8 ) != 0){
		hist_names.push_back(  n_hist_8 );
		nev.push_back(n_ev_8);
	}	
	if (strlen( n_hist_9 ) != 0){
		hist_names.push_back(  n_hist_9 );
		nev.push_back(n_ev_9);
	}	
	
	if(hist_names.size() == 0)
		return;
	
	//Read in histograms
	TFile* f_in = TFile::Open( in_file.c_str() , "READ");
	
	for( const char* const& histogram_name: hist_names){
		Source_Histos.push_back( (TH1F* )f_in->Get( histogram_name )->Clone() );
		Source_Histos.back()->SetDirectory(0);
	}
	
	TH1F* h_data = (TH1F*)Source_Histos.at(0)->Clone();
	h_data->SetDirectory(0);
	h_data->Reset();
	h_data->SetName("Data");
	h_data->SetTitle("Data Histogram");
	
	for( unsigned int it = 0; it <Source_Histos.size(); it++ ){
		
		if ( nev.at(it) > 0 ){
			for (int ev = 0; ev < nev.at(it); ev++){
				h_data->Fill( Source_Histos.at(it)->GetRandom() );
			}
		}
		else if ( nev.at(it) == -1 )
			*h_data = *h_data + *Source_Histos.at(it);
	}
	
	TFile* f_out = TFile::Open(out_file.c_str(), "RECREATE");
	f_out->cd();
	
	h_data->Write();
	
	for ( TH1F* const& hist: Source_Histos)
		hist->Write();
	
	f_in->Close();
	f_out->Close();
	
	
}


void GetErrComparison( const char* file_name = "Source_Histograms.root" ){
	
	
	TFile* hist_file = TFile::Open(file_name);
	
	TH1F* h_data = (TH1F*)hist_file->Get("Data");
	
	
	
	TH1F* h_errcomp = (TH1F*)h_data->Clone();
	h_errcomp->Sumw2(false);
	for (int bin = 1; bin <=40; bin++){
		
		h_errcomp->SetBinContent( bin, h_data->GetBinError(bin) / sqrt(h_data->GetBinContent(bin) ) );
		
	}
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	h_errcomp->Draw();
	
	
	
	
}
