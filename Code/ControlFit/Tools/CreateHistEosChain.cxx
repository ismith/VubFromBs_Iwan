#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TChain.h"
#include <iostream>

#include "DsYield.cxx"
int n_bins = 40;
double bin_low = 3000;
double bin_high = 6000;

/* ***********************************************
 * 
 * Use root 6.06.02 - 


SetupProject root 6.06.02

* 
* 
* 
* *************************************************/
TH1F* DrawHistFromChain(TChain* chain, const char* name, const char* title, const char* cutstring = ""){
	
	TH1F* h_MCORR = new TH1F(name, title, n_bins, bin_low, bin_high);
	chain->Draw(("Bs_MCORR>>" + std::string(name)).c_str(), cutstring);
	return h_MCORR;
	
	
}

TH1F* CreateHistEosChain(std::string file_path, const char* name, const char* title, const char* cutstring = "", bool use_fit = false, std::string fit_all_out = "", std::string fit_individual_out = "", bool fix_signal = true, bool fix_bg = true){
	
	
	TChain* newchain = new TChain("reducedTree", "reducedTree");
	
	std::cout << "Adding ";
	std::cout << newchain->Add( file_path.c_str() );
	std::cout << " New Files To The Chain." << std::endl;
	
	TH1F* out_hist = new TH1F();
	
	if ( use_fit )
		out_hist = DsYield(	newchain, name, title, n_bins, bin_low, bin_high, fix_signal, fix_bg, fit_all_out, fit_individual_out);
	else
		out_hist = DrawHistFromChain( newchain, name, title, cutstring);
	
	delete newchain;
	return out_hist;
	
	}
	
TH1F* CreateHistEosChain(vector<std::string> file_paths, const char* name, const char* title, const char* cutstring = "", bool use_fit = false, std::string fit_all_out = "", std::string fit_individual_out = "", bool fix_signal = true, bool fix_bg = true){
	
	
	TChain* newchain = new TChain("reducedTree", "reducedTree");
	
	for( std::string& path: file_paths){
		std::cout << "Adding " << path;
		std::cout << " To The Chain." << std::endl;
		newchain->Add( path.c_str() );
	}
	
	TH1F* out_hist = new TH1F();
	
	if ( use_fit )
		out_hist = DsYield(	newchain, name, title, n_bins, bin_low, bin_high, fix_signal, fix_bg, fit_all_out, fit_individual_out);
	else
		out_hist = DrawHistFromChain( newchain, name, title, cutstring);
	
	delete newchain;
	return out_hist;
	
	}



void SaveHistogram(TFile* file, TH1F* Hist){
	
	
	file->cd();
	
	Hist->Sumw2();
	
	Hist->SetBinContent(0, 0);
	Hist->SetBinContent(n_bins+1, 0);
	Hist->SetBinError(0, 0);
	Hist->SetBinError(n_bins+1, 0);
	
	Hist->SetDirectory(0);
	
	for ( int bin = 1; bin <= n_bins; bin++ ){
		if ( Hist->GetBinContent(bin) == 0 ){
			Hist->SetBinContent( bin, 1e-10 );
			Hist->SetBinError( bin, 1e-10 );
		}
	}
	
	double scale_factor = 1.0 / Hist->Integral();
	Hist->Write();
	Hist->Scale( scale_factor );
	Hist->SetName( (std::string( Hist->GetName() ) + "_norm" ).c_str() );
	Hist->Write();
	
	
	}



void CreateHist(const char* out_file = "Source_Histograms.root", std::string additional_cuts = ""){
	
	
	std::string common_cuts = "DeltaMass1>180 && DeltaMass>150 && passJpsiCut && passDplusCut && passLambdaCut && passKstarCut && passDstarCut1";
	std::string data_cuts = "";
	std::string MC_cuts = "";
	std::string channel_cuts = "";
	
	std::string all_cuts = "";
	
	if ( additional_cuts != "" )
		common_cuts = common_cuts + " && " + additional_cuts;
	
	
	vector<TH1F*> Histograms_to_save;
	
	//First Run over data This will take a long time...
	all_cuts = common_cuts + data_cuts + channel_cuts;
	std::string file_name = "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/*_DTT_201*_Reco14Strip21r*p1a_*_SEMILEPTONIC_trimmed_norm.root";
	Histograms_to_save.push_back(CreateHistEosChain(file_name, "Data", "Data", all_cuts.c_str(), true, "Plots/Data_all", "Plots/Data_bins", true, false));
	
	
	vector<std::string> Signal_MC_cocktail_files;
	Signal_MC_cocktail_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_Bs2DsMuNu_13774000_Cocktail_SIGNAL_Down_trimmed_norm.root");
	Signal_MC_cocktail_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_Bs2DsMuNu_13774000_Cocktail_SIGNAL_Up_trimmed_norm.root");
	Signal_MC_cocktail_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bs2DsMuNu_13774000_Cocktail_SIGNAL_Down_trimmed_norm.root");
	Signal_MC_cocktail_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bs2DsMuNu_13774000_Cocktail_SIGNAL_Up_trimmed_norm.root");
	
	//Save Signal Dsmunu
	channel_cuts =  " && Bs_TM_DsMu > 0";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Signal_MC_cocktail_files, "Dsmunu", "Dsmunu", all_cuts.c_str()));
	
	//DsStar background
	channel_cuts =  " && Bs_TM_DsstarMu_Dsg + Bs_TM_DsstarMu_Dspi0 > 0";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Signal_MC_cocktail_files, "DsStarmunu", "DsStarmunu", all_cuts.c_str()));
	
	//DsStar0 background
	channel_cuts =  " && Bs_TM_Dsstar0Mu_Dspi0 + Bs_TM_Dsstar0Mu_Dspi0pi0 + Bs_TM_Dsstar0Mu_Dspipi + Bs_TM_Dsstar0Mu_Dsstarg_Dsg + Bs_TM_Dsstar0Mu_Dsstarg_Dspi0 > 0";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Signal_MC_cocktail_files, "DsStar0munu", "DsStar0munu", all_cuts.c_str()));
	
	//DsStar1 background
	channel_cuts =  " && Bs_TM_Ds12460_Dspi0pi0 + Bs_TM_Ds12460_Dspipi + Bs_TM_Ds12460_Dsstarpi0_Dsg + Bs_TM_Ds12460_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsg + Bs_TM_Ds12536Mu_Dspi0pi0 + Bs_TM_Ds12536Mu_Dspipi + Bs_TM_Ds12536Mu_Dsstarg_Dsg + Bs_TM_Ds12536Mu_Dsstarg_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspipi > 0";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Signal_MC_cocktail_files, "DsStar1munu", "DsStar1munu", all_cuts.c_str()));
	
	//DsTau background
	channel_cuts =  " && Bs_TM_DsTau + Bs_TM_DsstarTau_Dsg + Bs_TM_DsstarTau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0pi0 + Bs_TM_Dsstar0Tau_Dspipi + Bs_TM_Dsstar0Tau_Dsstarg_Dsg + Bs_TM_Dsstar0Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsg + Bs_TM_Ds12536Tau_Dspi0pi0 +Bs_TM_Ds12536Tau_Dspipi + Bs_TM_Ds12536Tau_Dsstarg_Dsg +  Bs_TM_Ds12536Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspipi > 0";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Signal_MC_cocktail_files, "DsXtaunu", "DsXtaunu", all_cuts.c_str()));
	
	vector<std::string> Double_D_Modes_files;
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bd2DsDst_11876001_BKG_Down_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bd2DsDst_11876001_BKG_Up_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bs2DsstDsst_13873201_BKG_Down_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bs2DsstDsst_13873201_BKG_Up_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bu2DsstD0st_12875601_BKG_Down_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC12_Bu2DsstD0st_12875601_BKG_Up_trimmed_norm.root");
	
	
	Double_D_Modes_files.clear();
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_11995200_Down_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_11995200_Up_trimmed_norm.root");
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_11995202_Down_trimmed_norm.root");
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_11995202_Up_trimmed_norm.root");
	
		
	channel_cuts = "";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Double_D_Modes_files, "Bd_DD", "Dd_DD", all_cuts.c_str(), true, "Plots/Bd_DD_all", "Plots/Bd_DD_bins", true, true));
	
	
	Double_D_Modes_files.clear();
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_12995600_Down_trimmed_norm.root");
	//Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_12995600_Up_trimmed_norm.root");
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_12995602_Down_trimmed_norm.root");
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_12995602_Up_trimmed_norm.root");
			
	channel_cuts = "";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Double_D_Modes_files, "Bu_DD", "Bu_DD", all_cuts.c_str(), true, "Plots/Bu_DD_all", "Plots/Bu_DD_bins", true, true));
	
	
	Double_D_Modes_files.clear();
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_13996202_Down_trimmed_norm.root");
	Double_D_Modes_files.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC11_13996202_Up_trimmed_norm.root");
	
	channel_cuts = "";
	all_cuts = common_cuts + MC_cuts + channel_cuts;
	Histograms_to_save.push_back(CreateHistEosChain(Double_D_Modes_files, "Bs_DD", "Bs_DD", all_cuts.c_str(), true, "Plots/Bs_DD_all", "Plots/Bs_DD_bins", true, true));

	
	
	
	TFile* f_out = TFile::Open(out_file, "RECREATE");
	f_out->cd();

	for (TH1F*& hist: Histograms_to_save){
		SaveHistogram( f_out, hist);
	}
	
	f_out->Close();
	
}




void FitData(){
	
	std::string common_cuts = "DeltaMass1>180 && DeltaMass>150 && passJpsiCut && passDplusCut && passLambdaCut && passKstarCut && passDstarCut1";
	std::string data_cuts = "";
	std::string MC_cuts = "";
	std::string channel_cuts = "";
	
	std::string all_cuts = "";
		
	//First Run over data This will take a long time...
	all_cuts = common_cuts + data_cuts + channel_cuts;
	std::string file_name = "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/9*_DTT_201*_Reco14Strip21r*p1a_*_SEMILEPTONIC_trimmed_norm.root";
	TChain* newchain = new TChain("reducedTree", "reducedTree");
	
	std::cout << "Adding ";
	std::cout << newchain->Add( file_name.c_str() );
	std::cout << " New Files To The Chain." << std::endl;
	
	TH1F* out_hist = new TH1F();
	
	FitAndPlot(	newchain, "Data", "Data", n_bins, bin_low, bin_high);

	
	}
