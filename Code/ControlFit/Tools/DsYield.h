#include "TVirtualPad.h"
#include "RooHist.h"

class FitData{
	
	private:
		double l_mass = 1930;
		double h_mass = 2000;	
	
		RooRealVar* D_M; 				//The variable which varies In this case Charm Mass
		
		RooDataHist* data;  			// The Data histogram
		
		RooRealVar* D_DG_mu;			// Mean Ds Mass
		RooRealVar* D_DG_s1;			// Sigma 1 value
		RooRealVar* D_DG_s2;			// Sigma 2 value
		RooGaussian* D_Gauss1_pdf; 		// Gauss 1
		RooGaussian* D_Gauss2_pdf; 		// Gauss 2
		RooRealVar* D_DG_f1; 			// Fraction of Gauss 1
		RooAddPdf* D_DoubleGauss_pdf;	// The DoubleGauss PDF
		RooRealVar* pol_var_1;  		// Background x^0 value
		RooRealVar* pol_var_2;			// Background x^1 value 
		RooPolynomial* poly;			// Polynomial PDF
		RooRealVar* s_nev;				// Number of Signal
		RooRealVar* b_nev;				// Number of Background
		RooAddPdf* data_dist;			// Data Distribution
			
		
			
	public:

		FitData( double low_mass, double high_mass ){
			l_mass = low_mass;
			h_mass = high_mass;	
			
			init();		
		}
		FitData( ){

			init();		
		}		
		
		void init(){
			D_M = new RooRealVar("Ds_M", "Ds Mass", l_mass, h_mass);
			
			D_DG_mu = new RooRealVar("D_mu", "Ds Double Gauss Mean", 1969.72, 1965.3, 1977.3);
		
			D_DG_s1 = new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 10.7097, 1, 13);
			D_DG_s2 = new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 5.51790, 1, 9);
			
			D_Gauss1_pdf = new RooGaussian("D_Gauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_DG_mu, *D_DG_s1);
			D_Gauss2_pdf = new RooGaussian("D_Gauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s2);
			
			D_DG_f1 = new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.237746, 0.0, 1.0);
			D_DoubleGauss_pdf = new RooAddPdf("D_DoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Gauss1_pdf, *D_Gauss2_pdf), RooArgList( *D_DG_f1) );

			
			pol_var_1 = new RooRealVar("pol_var_1", "pol_var_1", 11200, -1000000, 1000000 );
			pol_var_2 = new RooRealVar("pol_var_2", "pol_var_2", 0, -100, 100 );
			
			poly = new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2));

		}
			
		void Fit( TH1* Hist ){
			data = new RooDataHist("data", "data", *D_M, Hist);
			s_nev = new RooRealVar("s_nev", "s_nev", data->sumEntries()*0.17, 0, data->sumEntries());
			b_nev = new RooRealVar("b_nev", "b_nev", data->sumEntries()*0.83, 0, data->sumEntries());

			
			data_dist = new RooAddPdf("data_dist", "data_dist", RooArgList(*D_DoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev));
				
			data_dist->fitTo(*data, RooFit::Extended());
			
			

		}
		void GetSigYield( double* nsig, double* nsig_err ){
			*nsig = s_nev->getValV();
			*nsig_err = s_nev->getError();			
		}
		void GetBGYield( double* nsig, double* nsig_err ){
			*nsig = b_nev->getValV();
			*nsig_err = b_nev->getError();			
		}
				
		void Plot( TVirtualPad* canv){
			RooPlot* Dframe = D_M->frame(RooFit::Title("K K #pi Invariant Mass"));
			data->plotOn(Dframe);
			data_dist->plotOn(Dframe);
			
			TPad *pad1 = new TPad("pad1", "The pad 70% of the height",0.0,0.3,1.0,1.0);
			pad1->cd();
			Dframe->Draw();

			RooHist* hpull = Dframe->pullHist() ;
			RooPlot* DpullFrame = D_M->frame(RooFit::Title("Fit Pulls")) ;
			DpullFrame->addPlotable(hpull,"P") ;

			TPad *pad2 = new TPad("pad2", "The pad 30% of the height",0.0,0.0,1.0,0.3);
			pad2->cd();
			
			DpullFrame->Draw();
			
			canv->cd();
			pad1->Draw();
			pad2->Draw();
			
			
		}
		
		void FixSignal( bool fix ){
			D_DG_mu->setConstant( fix );
			D_DG_s1->setConstant( fix );
			D_DG_s2->setConstant( fix );
			D_DG_f1->setConstant( fix );
		}
		void FixBG( bool fix ){
			pol_var_1->setConstant( fix );
			pol_var_2->setConstant( fix );
		}	
};


