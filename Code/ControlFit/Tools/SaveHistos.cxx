#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"

#include "DsYield.cxx"

void SaveHistos(std::string out_file = "Source_Histograms.root"){
	
	//Bin details
	
	int n_bins = 40;
	double bin_low = 3000;
	double bin_high = 6000;
	
	
	/*------------------------------------------
	Break up Alessio's tags into 
	
	Bs->Ds Mu Nu
	Bs->Ds* Mu Nu
	Bs->Ds*0 Mu Nu
	Bs->Ds1(2460) Mu Nu
	Bs->Ds1(2536) Mu Nu
	
	Bs->Ds(*)(nnnnn) Tau Nu
	 
	*/
	
	vector<TH1F*> Histograms;
	
	
	//Fill the histogram vector with the samples obtained from the Cocktail sample
	
	TFile* f_MC = TFile::Open("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC12_Bs2DsMuNu_13774002_Cocktail_SIGNAL_Down.root");
	TTree* T_MC = (TTree*)f_MC->Get("Bs2KmuNuTuple/DecayTree");
	
	Histograms.push_back( new TH1F("h_Ds", "Bs->Ds Mu Nu Shape", n_bins, bin_low, bin_high) );
	std::string cut_Ds = "Bs_TM_DsMu >0";
	T_MC->Draw("Bs_MCORR >> h_Ds", cut_Ds.c_str() );
	
	Histograms.push_back( new TH1F("h_DsStar", "Bs->Ds* Mu Nu cocktail Shape", n_bins, bin_low, bin_high) );
	std::string cut_DsStar = " Bs_TM_DsstarMu_Dsg + Bs_TM_DsstarMu_Dspi0 > 0";
	T_MC->Draw("Bs_MCORR >> h_DsStar", cut_DsStar.c_str() );
	
	Histograms.push_back( new TH1F("h_DsStar0", "Bs->Ds*0 Mu Nu cocktail Shape", n_bins, bin_low, bin_high) );
	std::string cut_DsStar0 = " Bs_TM_Dsstar0Mu_Dspi0 + Bs_TM_Dsstar0Mu_Dspi0pi0 + Bs_TM_Dsstar0Mu_Dspipi + Bs_TM_Dsstar0Mu_Dsstarg_Dsg + Bs_TM_Dsstar0Mu_Dsstarg_Dspi0 > 0";
	T_MC->Draw("Bs_MCORR >> h_DsStar0", cut_DsStar0.c_str() );
	
	Histograms.push_back( new TH1F("h_DsStar1", "Bs->Ds*1(2460) Mu Nu cocktail Shape", n_bins, bin_low, bin_high) );
	std::string cut_DsStar1 = "Bs_TM_Ds12460_Dspi0pi0 + Bs_TM_Ds12460_Dspipi + Bs_TM_Ds12460_Dsstarpi0_Dsg + Bs_TM_Ds12460_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsg + Bs_TM_Ds12536Mu_Dspi0pi0 + Bs_TM_Ds12536Mu_Dspipi + Bs_TM_Ds12536Mu_Dsstarg_Dsg + Bs_TM_Ds12536Mu_Dsstarg_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspipi > 0 ";
	T_MC->Draw("Bs_MCORR >> h_DsStar1", cut_DsStar1.c_str() );
	
	Histograms.push_back( new TH1F("h_DsTau", "Bs->Ds(*)(J)(nnnn) Tau Nu cocktail Shape", n_bins, bin_low, bin_high) );
	std::string cut_DsTau = "Bs_TM_DsTau + Bs_TM_DsstarTau_Dsg + Bs_TM_DsstarTau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0pi0 + Bs_TM_Dsstar0Tau_Dspipi + Bs_TM_Dsstar0Tau_Dsstarg_Dsg + Bs_TM_Dsstar0Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsg + Bs_TM_Ds12536Tau_Dspi0pi0 +Bs_TM_Ds12536Tau_Dspipi + Bs_TM_Ds12536Tau_Dsstarg_Dsg +  Bs_TM_Ds12536Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspipi > 0";
	T_MC->Draw("Bs_MCORR >> h_DsTau", cut_DsTau.c_str() );
	
	
	//Fill the vector with Histograms made from the data
	
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/data12_Bs2DsMuNu_Down_1.root", "Bs2KmuNuTuple/DecayTree", "h_SS", "Same Sign", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/data12_Bs2DsMuNu_Down_1.root", "Bs2KmuNuTuple/DecayTree", "h_data", "Data", n_bins, bin_low, bin_high) );
	
	//Fill the vector with Histograms made from MC but have a combinatorial component
	
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC11_11995202_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bd_DD", "Bd_DD", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC11_12995602_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bu_DD", "Bu_DD", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC11_13996202_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bs_DD", "Bs_DD", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC12_Bd2DsDst_11876001_BKG_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bd_DsDst", "Bd_DsDst", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC12_Bs2DsstDsst_13873201_BKG_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bd_DsstDsst", "Bd_DsstDsst", n_bins, bin_low, bin_high) );
	Histograms.push_back( DsYield("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsmuNu_Tuples_04May/DTT_MC12_Bu2DsstD0st_12875601_BKG_Down.root", "Bs2KmuNuTuple/DecayTree", "h_Bu_DsstD0st", "Bd_DsstD0st", n_bins, bin_low, bin_high) );
	
	
	TFile* f_out = TFile::Open(out_file.c_str(), "RECREATE");
	f_out->cd();
	
	for( TH1F* const& Hist: Histograms){
		vector<TH1F*> vect_Hists;
		

		Hist->Sumw2();
		
		Hist->SetBinContent(0, 0);
		Hist->SetBinContent(n_bins+1, 0);
		Hist->SetBinError(0, 0);
		Hist->SetBinError(n_bins+1, 0);
		
		Hist->SetDirectory(0);
		
		for ( int bin = 1; bin <= n_bins; bin++ ){
			if ( Hist->GetBinContent(bin) == 0 ){
				Hist->SetBinContent( bin, 1e-10 );
				Hist->SetBinError( bin, 1e-10 );
			}
		}
		
		
		TH1F* Hist_Err_Stat = (TH1F*)Hist->Clone();
		Hist_Err_Stat->SetName( (std::string( Hist_Err_Stat->GetName() ) + "_err_stat" ).c_str() );
		
		for ( int bin = 1; bin <= n_bins; bin++ ){
			Hist_Err_Stat->SetBinContent( bin, Hist_Err_Stat->GetBinError(bin));
			Hist_Err_Stat->SetBinError( bin, 0 );
		}
		
		TH1F* Hist_Err_Sys_l = (TH1F*)Hist->Clone();
		Hist_Err_Sys_l->SetName( (std::string( Hist_Err_Sys_l->GetName() ) + "_err_sys_l" ).c_str() );
		
		for ( int bin = 1; bin <= n_bins; bin++ ){
			Hist_Err_Sys_l->SetBinContent( bin, Hist_Err_Sys_l->GetBinContent(bin) - Hist_Err_Sys_l->GetBinError(bin));
			Hist_Err_Sys_l->SetBinError( bin, 0 );
		}		
		
		
		TH1F* Hist_Err_Sys_h = (TH1F*)Hist->Clone();
		Hist_Err_Sys_h->SetName( (std::string( Hist_Err_Sys_h->GetName() ) + "_err_sys_h" ).c_str() );
		
		for ( int bin = 1; bin <= n_bins; bin++ ){
			Hist_Err_Sys_h->SetBinContent( bin, Hist_Err_Sys_h->GetBinContent(bin) + Hist_Err_Sys_h->GetBinError(bin));
			Hist_Err_Sys_h->SetBinError( bin, 0 );
		}		
		
		vect_Hists.push_back(Hist);
		vect_Hists.push_back(Hist_Err_Stat);
		vect_Hists.push_back(Hist_Err_Sys_l);
		vect_Hists.push_back(Hist_Err_Sys_h);
		
		
		double scale_factor = 1.0 / Hist->Integral();
		for( TH1F* const& Hist_out: vect_Hists){
			
			Hist_out->Write();
			Hist_out->Scale( scale_factor );
			Hist_out->SetName( (std::string( Hist_out->GetName() ) + "_norm" ).c_str() );
			Hist_out->Write();
			
		}
	}

	f_MC->Close();
	f_out->Close();
	
	
}
