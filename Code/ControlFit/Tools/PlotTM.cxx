#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"

void PlotTM(){
	
	TFile* File = TFile::Open("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/DTT_MC12_Bs2DsMuNu_13774002_Cocktail_SIGNAL_Up.root");
	TTree* Tree = (TTree*)File->Get("Bs2KmuNuTuple/DecayTree");
	
	vector<std::string> TM_var;
	
	TM_var.push_back("Bs_TM_Ds12460_Dspi0pi0");
	TM_var.push_back("Bs_TM_Ds12460_Dspipi");
	TM_var.push_back("Bs_TM_Ds12460_Dsstarpi0_Dsg");
	TM_var.push_back("Bs_TM_Ds12460_Dsstarpi0_Dspi0");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsg");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dspi0pi0");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dspipi");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsstarg_Dsg");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsstarg_Dspi0");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0pi0");
	TM_var.push_back("Bs_TM_Ds12536Mu_Dsstarpi0_Dspipi");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsg");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dspi0pi0");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dspipi");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsstarg_Dsg");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsstarg_Dspi0");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0pi0");
	TM_var.push_back("Bs_TM_Ds12536Tau_Dsstarpi0_Dspipi");
	TM_var.push_back("Bs_TM_DsMu");
	TM_var.push_back("Bs_TM_DsTau");
	TM_var.push_back("Bs_TM_Dsstar0Mu_Dspi0");
	TM_var.push_back("Bs_TM_Dsstar0Mu_Dspi0pi0");
	TM_var.push_back("Bs_TM_Dsstar0Mu_Dspipi");
	TM_var.push_back("Bs_TM_Dsstar0Mu_Dsstarg_Dsg");
	TM_var.push_back("Bs_TM_Dsstar0Mu_Dsstarg_Dspi0");
	TM_var.push_back("Bs_TM_Dsstar0Tau_Dspi0");
	TM_var.push_back("Bs_TM_Dsstar0Tau_Dspi0pi0");
	TM_var.push_back("Bs_TM_Dsstar0Tau_Dspipi");
	TM_var.push_back("Bs_TM_Dsstar0Tau_Dsstarg_Dsg");
	TM_var.push_back("Bs_TM_Dsstar0Tau_Dsstarg_Dspi0");
	TM_var.push_back("Bs_TM_DsstarMu_Dsg");
	TM_var.push_back("Bs_TM_DsstarMu_Dspi0");
	TM_var.push_back("Bs_TM_DsstarTau_Dsg");
	TM_var.push_back("Bs_TM_DsstarTau_Dspi0");
	
	TCanvas* c1 = new TCanvas("c1", "c1", 10000, 10000);
	c1->Divide(6,6);
	int canv = 1;
	TH1F* h = new TH1F("h1", "h1", 50, 3000, 6000);
	
	std::string passcuts = " 0 ";
	for (auto const& Truth: TM_var){
		std::cout << Truth << "  ";
		std::cout << Tree->GetEntries( ( Truth + "==1").c_str() ) << std::endl;
		c1->cd(canv);

		Tree->Draw("Bs_MCORR>>h1", ( Truth + "==1").c_str() );
		h->GetXaxis()->SetTitle(Truth.c_str() );
		h->DrawClone();
		
		canv++;
		
		passcuts += "+" + Truth;
		
	}
	TH1F* h_sumT = new TH1F("h_sum", "h_sum", 5, -0.5, 4.5);
	TCanvas* c2 = new TCanvas("c2", "c2", 1600, 1600);
	Tree->Draw( ( passcuts + ">> h_sum").c_str() );
	h_sumT->Draw();
	
	TH1F* h_NoT = new TH1F("noT", "noT", 50, 3000, 6000);
	TCanvas* c3 = new TCanvas("c3", "c3", 1600, 1600);
	Tree->Draw("Bs_MCORR>>noT", ( passcuts + " == 0").c_str() );
	h_NoT->Draw();
	
	
	
}
