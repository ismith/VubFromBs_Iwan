#ifndef __IINT
#include "RooGlobalFunc.h"
#endif
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "TH1.h"
#include "TH1F.h"

#include "TCanvas.h"

#include "RooDataHist.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooPolynomial.h"
#include "RooGaussian.h"
#include "RooPlot.h"

#include "DsYield.h"

TH1F* DsYield(	TTree* T_dat, const char* c_histname = "BMCORR", const char* c_histtitle = "BMCORR", int n_bins = 40, double b_low = 3000, double b_high = 6000, bool fix_signal = true, bool fix_bg = true, std::string fit_all_out = "", std::string fit_individual_out = ""){
	
	
	double D_low = 1930;
	double D_high = 2000;
	
		
	TH1F* h_Ds = new TH1F("h_Ds", "h_Ds", 100, D_low, D_high);
	T_dat->Draw("Ds_M>>h_Ds");
	
	
	FitData* FitClass = new FitData( D_low, D_high);
	FitClass->Fit( h_Ds );

	TCanvas* c_all = new TCanvas("c_all", "Fit To Entire DataSet", 1600, 1600);
	FitClass->Plot( c_all );
	
	double n_sig, n_err;	
	FitClass->GetSigYield( &n_sig, & n_err );
	double n_bg, n_bgerr;	
	FitClass->GetBGYield( &n_bg, & n_bgerr );
	
	std::cout << "DataSet Contains: " << n_sig << " +/- " << n_err << " Data Points, and: " << n_bg << " +/- " << n_bgerr << " Background Points. " << std::endl;
	
	FitClass->FixSignal( fix_signal );
	FitClass->FixBG( fix_bg );
	
	TCanvas* c1 = new TCanvas("c1", "c1", 2560, 1600);	
	int ncol = int (sqrt(n_bins) + 0.9999999 );	
	ncol = 8; //temporarily overide this
	int nrow = ncol;
	while ( ncol * ( nrow - 1 ) >= n_bins )
		nrow--;
	c1->Divide(ncol, nrow);


	TH1F* h_BMCORR = new TH1F("h_BMCORR", "Ds yield Vs BMCORR", n_bins, b_low, b_high);
	h_BMCORR->Sumw2();
	
	for ( int bin = 0; bin < n_bins; bin++ ){
		double bb_low =  b_low + double( bin     ) / double( n_bins ) * ( b_high - b_low );
		double bb_high = b_low + double( bin + 1 ) / double( n_bins ) * ( b_high - b_low );
		
		std::string cut_string = "Bs_MCORR > " + std::to_string( bb_low ) + " && Bs_MCORR < " + std::to_string( bb_high );
		std::cout << cut_string << std::endl;
		TCanvas* c_temp = new TCanvas("c_temp", "c_temp", 1600, 1600);
		TH1F* h_Ds = new TH1F("h_Ds", "h_Ds", 100, D_low, D_high);
		T_dat->Draw("Ds_M>>h_Ds", cut_string.c_str() );
		delete c_temp;
		
		
		double n_sig, n_err;
		
		c1->cd(bin+1);
		
		FitClass->Fit( h_Ds );
		FitClass->Plot( c1->GetPad(bin+1) );
		FitClass->GetSigYield( &n_sig, &n_err );

		h_BMCORR->SetBinContent( bin+1, n_sig);
		h_BMCORR->SetBinError(   bin+1, n_err);

		delete h_Ds;
		
		
	}	
	
	h_BMCORR->SetName( c_histname );
	h_BMCORR->SetTitle( c_histtitle );
	h_BMCORR->SetDirectory( 0 );
	
	//TFile* f_out = TFile::Open( c_rootname, "UPDATE");
	//h_BMCORR->Write();
	
	//f_out->Close();
	
	//Save the canvases
	
	if ( fit_all_out != "" ){
		c_all->Print( (fit_all_out + ".pdf").c_str() );
		c_all->Print( (fit_all_out + ".C").c_str() );
	}
	if ( fit_individual_out != "" ){
		c1->Print( (fit_individual_out + ".pdf").c_str() );
		c1->Print( (fit_individual_out + ".C").c_str() );
	}	
	
	return h_BMCORR;
	
}

TH1F* DsYield(	const char* c_datfile =  "/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_TightSS.root", const char* c_treename = "DecayTree", 
				const char* c_histname = "BMCORR", const char* c_histtitle = "BMCORR", int n_bins = 40, double b_low = 3000, double b_high = 6000, bool fix_signal = true, bool fix_bg = true, std::string fit_all_out = "", std::string fit_individual_out = ""){
	
	TFile* F_dat = TFile::Open( c_datfile );
	TTree* T_dat = (TTree*)F_dat->Get( c_treename );

	TH1F* h_BMCORR  = DsYield( T_dat, c_histname, c_histtitle, n_bins, b_low, b_high, fix_signal, fix_bg, fit_all_out, fit_individual_out);

	F_dat->Close();
	
	return h_BMCORR;
	
}

void FitAndPlot( TTree* T_dat, const char* c_histname = "BMCORR", const char* c_histtitle = "BMCORR", int n_bins = 40, double b_low = 3000, double b_high = 6000){
	
	double D_low = 1920;
	double D_high = 2010;
	
		
	TH1F* h_Ds = new TH1F("h_Ds", "h_Ds", 100, D_low, D_high);
	TCanvas* sacraficialcanvas = new TCanvas("c1", "c1", 1600, 1600); 
	T_dat->Draw("Ds_M>>h_Ds");
	
	
	FitData* FitClass = new FitData( D_low, D_high);
	FitClass->Fit( h_Ds );

	TCanvas* c_all = new TCanvas("c_all", "Fit To Entire DataSet", 1600, 1600);
	FitClass->Plot( c_all );
	
	delete sacraficialcanvas;
	
	
}

