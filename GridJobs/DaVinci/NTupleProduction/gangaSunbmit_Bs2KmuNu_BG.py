import os , re

optsfile = "/afs/cern.ch/user/i/ismith/cmtuser/DaVinci_v36r7p7/Phys/BsToKMuNu/scripts/Bs_TupleMaker_MC_control.py"



"""  Example:
subdict = { "Job Name":{
				"eventtype" : "somenumbers",
				"LFN" : 	["LFN1",
							 "LFN2 etc"]
			}
			
	}



subdict[""] = {
	"eventtype" : "", 
	"BKDesc" : "",
	"LFN": ["",
			"",
			""],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}
"""

subdict = {}
"""


subdict["Vub_Inc_B->Ds"] = {
	"eventtype" : "23103003", 
	"BKDesc" : "Ds_phiph,KK=FromB",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103003/ALLSTREAMS.DST", 
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103003/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : "",
	"Job" : ""
}

subdict["Vub_Inc_D->Ds"] = {
	"eventtype" : "23103002",
	"BKDesc" : "Ds_phipi,KK=TightCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103002/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103002/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103002/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23103002/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : "",
	"Job" : ""						
}

subdict["Vub_Inc_Ds"] = {
	"eventtype" : "23263020", 
	"BKDesc" : "Ds+_K-K+pi+=res,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08f/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08f/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08f/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08f/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/23263020/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""		
}


subdict["Vub_coc_Ds*Ds*"] = {
	"eventtype" : "13873201", 
	"BKDesc" : "Bs_DsstDsst,gDsgDs,phipimunuX=cocktail,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""		
}


subdict["Vub_coc_Lb->LcDspi"] = {
	"eventtype" : "15894301", 
	"BKDesc" : "Lb_DsstLc,DsgmunuX=cocktail,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""		
}


subdict["Vub_coc_Bp->D0Ds"] = {
	"eventtype" : "12875601", 
	"BKDesc" : "Bu_DsstDst,gDsD0pi,phipimunuX=cocktail,D0muInAcc,BRcorr1",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""		
}


subdict["Vub_coc_B0->DmDs"] = {
	"eventtype" : "11876001", 
	"BKDesc" : "Bd_DsDst,phipiDpi,KKmunuX=cocktail,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""			
}


subdict["Vub_D02400->DsKmu"] = {
	"eventtype" : "12775001", 
	"BKDesc" : "Bu_Dst02400munu,DsK,KKpi=DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12775001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12775001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12775001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12775001/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""		
}
			


subdict["Vub_Dm2400->DsKmu"] = {
	"eventtype" : "11774001", 
	"BKDesc" : "Bd_Dstp2400munu,DsKS0,KKpi=DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11774001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11774001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11774001/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11774001/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}


subdict["Vub_Bs->DsDs"] = {
	"eventtype" : "13896201", 
	"BKDesc" : "Bs_DsDs,3body=cocktail,tightcut",
	"LFN": ["/MC/2012/Beam4000GeV-JulSep2012-MagDown-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/13896201/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-JulSep2012-MagUp-Nu2.5-EmNoCuts/Sim06b/Trig0x40990042Flagged/Reco14/Stripping20NoPrescalingFlagged/13896201/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["Vub_Lb->LcDspi"] = {
	"eventtype" : "15894301", 
	"BKDesc" : "Lb_DsstLc,DsgmunuX=cocktail,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}



subdict["Bc -> BsMuNu"] = {
	"eventtype" : "14575010", 
	"BKDesc" : "Bc_Bsmunu,Dspi=BcVegPy,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14575010/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14575010/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["Bc -> BsMuNu"] = {
	"eventtype" : "14175004", 
	"BKDesc" : "Bc_Dsmumu=BcVegPy,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14175004/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-BcVegPy/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/14175004/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["Bs->Ds3pi"] = {
	"eventtype" : "13272000", 
	"BKDesc" : "Bs_Ds3pi,munu=DecProdCut,TightCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08d/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13272000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08d/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13272000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08d/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13272000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08d/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13272000/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["B+ -> Ds mu mu"] = {
	"eventtype" : "12275031", 
	"BKDesc" : "Bu_Ds+mumu=DecProdCut.dec",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/12275031/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/12275031/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["/\b -> /\c Ds"] = {
	"eventtype" : "15296003", 
	"BKDesc" : "Lb_LcDs,pKpi,KKpi=DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15296003/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15296003/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["Bs->Ds tau nu"] = {
	"eventtype" : "13566000", 
	"BKDesc" : "Bs_DsTauNu=DecProdCut,tightcut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13566000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13566000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13566000/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13566000/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["B0->Ds K"] = {
	"eventtype" : "11164609 (11264609)", 
	"BKDesc" : "Bd_Ds-K+,KKpi",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/11164069/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/11164069/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}

subdict["B0 -> Ds Kst mu nu"] = {
	"eventtype" : "11574040", 
	"BKDesc" : "Bd_DsKstmunu,munu=DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/11574040/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/11574040/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}
"""
"""
subdict["B+ ->Ds phi"] = {
	"eventtype" : "12265031", 
	"BKDesc" : "Bu_Dsphi-DDalitz=DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12265031/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12265031/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12265031/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12265031/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}


subdict["/\b -> Ds* /\c"] = {
	"eventtype" : "15894301", 
	"BKDesc" : "Lb_DsstLc,DsgmunuX=cocktail,DecProdCut",
	"LFN": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST",
			"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15894301/ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}
"""
subdict["Bs ->Ds tau nu"] = {
	"eventtype" : "13566001", 
	"BKDesc" : "Bs_DsTauNu=DecProdCut",
	"LFN": ["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-EmNoCuts/Sim05b/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/13566001ALLSTREAMS.DST",
			"/MC/2011/Beam3500GeV-2011-MagUp-Nu2-EmNoCuts/Sim05b/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/13566001ALLSTREAMS.DST"],
	"BKQuery" : [],
	"DataSet" : ""	,
	"Job" : ""	
}


for jobname in subdict:
	print "Job Name:", jobname, "With BookKeeping Description: ", subdict[jobname]["BKDesc"]
	nLFN = 0
	for LFNpath in subdict[jobname]["LFN"]:
		subdict[jobname]["BKQuery"].append( BKQuery( path =  LFNpath ) )
		print "\tPerforming BKQuery on path:", LFNpath
		nLFN += 1
		
	print "\tGetting the dataset for", subdict[jobname]["BKQuery"][0]
	subdict[jobname]["DataSet"] = subdict[jobname]["BKQuery"][0].getDataset()
	for it in range(1, nLFN):
		print "\tGetting the dataset for", subdict[jobname]["BKQuery"][it]
		subdict[jobname]["DataSet"].extend( subdict[jobname]["BKQuery"][it].getDataset() )
			
	subdict[jobname]["Job"] = Job(application=DaVinci(version='v36r7p7') ,name = jobname )
	print "\tSetting Application to DaVinci"
	
	subdict[jobname]["Job"].application.optsfile = [optsfile]
	print "\tSetting optsfile to", optsfile
	
	subdict[jobname]["Job"].inputdata  = subdict[jobname]["DataSet"]
	print "\tAdding InputData to job"
	
	subdict[jobname]["Job"].outputfiles= ["DTT_Bd2JpsiKst.root"]
	subdict[jobname]["Job"].splitter = SplitByFiles(filesPerJob = 20)
	print "\tSplitting the job by files"
	
	subdict[jobname]["Job"].backend    = Dirac()
	print "\tSetting Backend to Dirac"
	
	subdict[jobname]["Job"].comment = subdict[jobname]["eventtype"] + "  " + subdict[jobname]["BKDesc"] 
	
print "Now let's submit all these Jobs. This is going to take Fucking Forever!"
print "Like seriously, if you didn't run this in a screen session you'd better not have any plans tonight!"
print "Anyway Good Luck"
print ":)"

#for jobname in subdict:
#	subdict[jobname]["Job"].submit()

