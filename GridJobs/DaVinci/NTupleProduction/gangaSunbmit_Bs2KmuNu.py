import os , re

###### 2011
# Down: 
#bb :
bb_Dn_2012_a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping21Filtered/10011001/BU2MUNU.STRIP.DST"
bb_Up_2012_b = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping21Filtered/10011001/BU2MUNU.STRIP.DST"
bb_Up_2012_c = "/MC/2012/Beam4000GeV-MayJune2012-MagUp-Nu2.5-EmNoCuts/Sim06b/Trig0x4097003dFlagged/Reco14/Stripping20Filtered/10012009/BS2MUMU.STRIP.DST"
bb_Up_2012_d = "/MC/2012/Beam4000GeV-MayJune2012-MagDown-Nu2.5-EmNoCuts/Sim06b/Trig0x4097003dFlagged/Reco14/Stripping20Filtered/10012009/BS2MUMU.STRIP.DST"

Bs2Dsmunu_Dn_2012_a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
Bs2Dsmunu_Up_2012_b = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
Bs2Dsmunu_Dn_2012_c = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
Bs2Dsmunu_Up_2012_d = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"

Signal_MC12_Dn_p6   = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST"
Signal_MC12_Dn_p8   = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST"
Signal_MC12_Up_p6   = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST"
Signal_MC12_Up_p8   = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST"

B2JpsiK_MC12_Dn_p6a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Dn_p6e = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Dn_p8a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Dn_p8e = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Up_p6a = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Up_p6e = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Up_p8a = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
B2JpsiK_MC12_Up_p8e = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"

Bd2JpsiKst_MC12_Dn_p6a ="/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Dn_p6e ="/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Dn_p8a ="/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Dn_p8e ="/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Up_p6a ="/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Up_p6e ="/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Up_p8a ="/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
Bd2JpsiKst_MC12_Up_p8e ="/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"

Bs2Jpsiphi_MC12_Dn_p6a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Dn_p6e = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Dn_p8a = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST" 
Bs2Jpsiphi_MC12_Dn_p8e = "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Up_p6a = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Up_p6e = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Up_p8a = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
Bs2Jpsiphi_MC12_Up_p8e = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"

data_2012_Down="/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/SEMILEPTONIC.DST"
data_2012_Up  ="/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/SEMILEPTONIC.DST"

data_2011_Down="/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/SEMILEPTONIC.DST"
data_2011_Up  ="/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/SEMILEPTONIC.DST"

####################################################
"""

When ganga becomes faster use this 
list_of_data_sample = [  DstarD_d11, DstarD_u11 ,DsatrD_u12 , DstarD_d12  ]
list_of_data_sample_names = [ 'DstarD_d11', 'DstarD_u11' ,'DsatrD_u12' , 'DstarD_d12' ]
i = 0
for data_sample in list_of_data_sample:

bb_Dn_2012_a
bb_Up_2012_b
bb_Up_2012_c
bb_Up_2012_d

Signal_MC12_Dn_p6
Signal_MC12_Dn_p8
Signal_MC12_Up_p6
Signal_MC12_Up_p8
Signal_MC12_Dn_p6
Signal_MC12_Dn_p8
Signal_MC12_Up_p6
Signal_MC12_Up_p8

B2JpsiK_MC12_Dn_p6a
B2JpsiK_MC12_Dn_p6e
B2JpsiK_MC12_Dn_p8a
B2JpsiK_MC12_Dn_p8e
B2JpsiK_MC12_Up_p6a
B2JpsiK_MC12_Up_p6e
B2JpsiK_MC12_Up_p8a
B2JpsiK_MC12_Up_p8e

Bd2JpsiKst_MC12_Dn_p6a
Bd2JpsiKst_MC12_Dn_p6e
Bd2JpsiKst_MC12_Dn_p8a
Bd2JpsiKst_MC12_Dn_p8e
Bd2JpsiKst_MC12_Up_p6a
Bd2JpsiKst_MC12_Up_p6e
Bd2JpsiKst_MC12_Up_p8a
Bd2JpsiKst_MC12_Up_p8e

Bs2Jpsiphi_MC12_Dn_p8a
Bs2Jpsiphi_MC12_Dn_p8e
Bs2Jpsiphi_MC12_Up_p8a
Bs2Jpsiphi_MC12_Up_p8e

"""

bk_query_a = BKQuery( path =  Bs2Dsmunu_Dn_2012_a )
bk_query_b = BKQuery( path =  Bs2Dsmunu_Up_2012_b )
bk_query_c = BKQuery( path =  Bs2Dsmunu_Dn_2012_c )
bk_query_d = BKQuery( path =  Bs2Dsmunu_Up_2012_d )
#k_query_e = BKQuery(path= B2JpsiK_MC12_Up_p6a )
#k_query_f = BKQuery(path= B2JpsiK_MC12_Up_p6e )
#k_query_j = BKQuery(path= B2JpsiK_MC12_Up_p8a )
#k_query_h = BKQuery(path= B2JpsiK_MC12_Up_p8e )
#
ds      = bk_query_a.getDataset()
ds.extend(bk_query_b.getDataset())
ds.extend(bk_query_c.getDataset())
ds.extend(bk_query_d.getDataset())
#s.extend(bk_query_e.getDataset())
#s.extend(bk_query_f.getDataset())
#s.extend(bk_query_j.getDataset())
#s.extend(bk_query_h.getDataset())
#
print "getting the data sample for : ", ds

job=Job(application=DaVinci(version='v36r7p7') ,
        name = 'DsMuNu_MC_AS_BKG' )# ,backend=Dirac() ) 
#job.application.optsfile = ['/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_TupleMaker_MC_LooseStrp.py']
job.application.optsfile = ['/afs/cern.ch/user/i/ismith/cmtuser/DaVinci_v36r7p7/Phys/BsToKMuNu/scripts/Bs_TupleMaker_MC_control.py']
#job.application.optsfile = ['/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_TupleMaker_data.py']

print "Create job for the jobs: ", job.name
job.inputdata  = ds #myinputdata  
job.outputfiles= [LocalFile(namePattern='*.root')] #['DTT_B02DstD_data.root']

print "Specify input data : super seeds what is in option files"

print "Calls splitter"
job.splitter = SplitByFiles(filesPerJob = 10)# , maxFiles = -1 , ignoremissing = True)

# for LSF :
print "Finally submit the job"
job.backend    = Dirac()
job.submit()
#queues.add(job.submit())
print "job: ", job.name + " submitted" 
#i = i+1

print " Jobs submitted .... bye  "

