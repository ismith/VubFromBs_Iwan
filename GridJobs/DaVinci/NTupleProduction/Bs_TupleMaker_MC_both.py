### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
Year = "2012"
#######################################################################
from Gaudi.Configuration import *
#######################################################################
#
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from PhysSelPython.Wrappers import (DataOnDemand,
                                    Selection,
                                    SelectionSequence)
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco , TupleToolBs2Kmunu , TupleToolBs2Kmunu_UpStream , TupleToolBs2Kmunu_velotracks , LoKi__Hybrid__EvtTupleTool, MCTupleToolInteractions

from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo 

# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html
# otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ... 
# MAKE loose selection :
Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
Muon_particles  = DataOnDemand( Location ='Phys/StdLooseMuons/Particles')

kaons_particles_filter = FilterDesktop("kaons_particles_cut",
                                       Code = "(TRGHOSTPROB < 0.5) & (PIDK-PIDpi> 0.0 ) & (PIDK-PIDp> 0.0 ) & (PIDK-PIDmu> 0.0 ) & (PT> 350.0 *MeV) & (P > 3000.0 *MeV)"
                                       )
kaons_particles_sel = Selection("kaons_particles_cut_sel",
                                Algorithm = kaons_particles_filter,
                                RequiredSelections = [ Kaon_particles ])
Muons_particles_filter= FilterDesktop("Muons_particles_cut",
                                      Code = "(TRGHOSTPROB < 0.5) & (PIDmu-PIDpi> 0. ) & (PIDmu-PIDp> 0.0 ) & (PIDmu-PIDK> 0.0 )& (PT> 350.0 *MeV) & (P > 3000.0 *MeV)"
                                      )
Muons_particles_sel = Selection("Muons_particles_cut_sel",
                                Algorithm = Muons_particles_filter,
                                RequiredSelections = [ Muon_particles ]
                                )


Bs2KMu    = CombineParticles("Bs2KMu",
                             DecayDescriptors = [ '[B_s~0 -> K+ mu-]cc' ] , 
                             CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                             #MotherCut        = 
                             
                             )
Bs2KMu.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
Bs2KMu.MotherCut        = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ [B_s~0]cc => K+ mu- Neutrino ]CC'))"
Bs2KMu_Sel = Selection("Bs2KMu_Sel",
                       Algorithm = Bs2KMu ,
                       RequiredSelections = [ Muons_particles_sel ,  kaons_particles_sel ]
                       )
SeqBs2KmuNu = SelectionSequence('SeqBs2KmuNu', TopSelection = Bs2KMu_Sel)
SeqBs2KmuNu.outputLevel = 6
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle =
                                    'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ]) 

# Add a Loose stripping to MC samples :
#Bs2KMuNuInputLocation =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles"
Bs2KMuNuInputLocation_MC =  "/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles" 
#############################################################
# DecayTreeTuple 
#
seqBsKmunu_gaudi    = GaudiSequencer('seqBsKmunu_gaudi')
Bs_particles        = DataOnDemand( Location = '/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles' )
#Bs_particles_filter.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
Bs_particles_filter = FilterDesktop("Bs_particles_filter")

Bs_particles_filter.Preambulo        = ["from LoKiPhysMC.decorators import *" , "from LoKiPhysMC.functions import mcMatch"]
#Preambulo        = "from LoKiPhysMC.functions import mcMatch",
Bs_particles_filter.Code = "(MAXTREE('D+'==ABSID,M)>1910) & (mcMatch (' [ [B_s~0]cc ==> D_s+ mu- Neutrino ]CC') ) " 

Bs_particles_sel    = Selection("Bs_particles_sel",
                                Algorithm = Bs_particles_filter,
                                RequiredSelections = [ Bs_particles ] )
SeqBs2DsMuNu        = SelectionSequence('SeqBs2DsMuNu', TopSelection = Bs_particles_sel)

seqBsKmunu_gaudi.Members       +=  [SeqBs2DsMuNu.sequence()]
seqBsKmunu_gaudi.Members       +=  [SeqBs2KmuNu.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False

TupleSeq   = GaudiSequencer('TupleSeq')

def configTupleTool(toolinstance):
   #toolinstance.ToolList +=  [
   toolinstance.ToolList =  [
       "TupleToolGeometry"
       ,"TupleToolKinematic"
       , "TupleToolPrimaries"
       , "TupleToolMCBackgroundInfo"
       , "TupleToolTrackInfo"
       , "TupleToolBs2Kmunu"
       #, "TupleToolEventInfo"
       #, "LoKi::Hybrid::TupleTool/LoKiTool"
       ]
   
   toolinstance.ReFitPVs = True
   toolinstance.OutputLevel = 6
   

   
   #toolinstance.addTool(TupleToolDecay, name="Bs")
   #toolinstance.addTool(TupleToolDecay, name="kaon_m")
   #toolinstance.addTool(TupleToolDecay, name="muon_p")

   print toolinstance.Branches
   tt_eventinfo = toolinstance.addTupleTool("TupleToolEventInfo")
   tt_eventinfo.Verbose = True

   LoKiVariables =toolinstance.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables") 
   toolinstance.Bs.LoKiVariables.Preambulo.append("from LoKiPhysMC.decorators import *")
   toolinstance.Bs.LoKiVariables.Preambulo.append("from LoKiPhysMC.functions import mcMatch")
   #LoKiVariables.Preambulo = [
   #    "from LoKiPhysMC.decorators import *",
   #    "from LoKiPhysMC.functions import mcMatch"
   #    ]
   
   LoKiVariables.Variables = {
       "ETA"              : "ETA",
       "PHI"              : "PHI",
       "DOCA"             : "DOCA(1,2)",
       "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
       "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
       "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
       "FD_CHI2_LOKI"     : "BPVVDCHI2",
       "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
       "FD_S"             : "BPVDLS",
       "DsMuNu_TruthMatched" : "switch( mcMatch ('[ [B_s~0]cc ==> D_s+ mu- Neutrino ]CC')   , 1 , 0 )",
       "KMuNu_TruthMatched"  : "switch( mcMatch ('[ [B_s~0]cc ==> K+   mu- Neutrino ]CC')   , 1 , 0 )"
       }
   
   LoKiVariables_K = toolinstance.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
   LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
   LoKiVariables_Mu = toolinstance.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
   LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
   ############################################################################
   toolinstance.Bs.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
   #TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
   toolinstance.Bs.TupleToolTISTOS.VerboseL0 = True
   toolinstance.Bs.TupleToolTISTOS.VerboseHlt1 = True
   toolinstance.Bs.TupleToolTISTOS.VerboseHlt2 = True             
   toolinstance.Bs.TupleToolTISTOS.TriggerList=[
       "L0MuonDecision",
       "L0DiMuonDecision",
       "L0HadronDecision",
       "Hlt1TrackMuonDecision",
       "Hlt1TrackAllL0Decision",
       "Hlt2TopoMu2BodyBBDTDecision",
       "Hlt2TopoMu3BodyBBDTDecision",
       "Hlt2TopoMu4BodyBBDTDecision",
       "Hlt2MuTrackDecision",
       "Hlt2SingleMuonDecision",
       "Hlt2SingleMuonHighPTDecision",
       "Hlt2SingleMuonLowPTDecision",
       #"Hlt2TFBc2JpsiMuXDecision",
       #"Hlt2TFBc2JpsiMuXSignalDecision",
       "Hlt2Topo2BodySimpleDecision",
       #"Hlt2B2HHPi0_MergedDecision",
       "Hlt2Topo3BodySimpleDecision",
       "Hlt2CharmHadD2HHHDecision",
       "Hlt2Topo4BodySimpleDecision",
       #"Hlt2CharmHadD2HHHWideMassDecision",
       "Hlt2Topo2BodyBBDTDecision",
       "Hlt2DiMuonJPsiHighPTDecision",
       "Hlt2Topo3BodyBBDTDecision",
       "Hlt2DiMuonBDecision",
       "Hlt2Topo4BodyBBDTDecision",
       "Hlt2DiMuonZDecision",
       "Hlt2DiMuonDetachedDecision",
       "Hlt2DiMuonDetachedHeavyDecision",
       "Hlt2DiMuonDetachedJPsiDecision",
       "Hlt2TriMuonDetachedDecision",
       "Hlt2TopoE3BodyBBDTDecision",
       "Hlt2TriMuonTauDecision",
       "Hlt2TopoE4BodyBBDTDecision",
       #"Hlt2CharmHadD02HHHHDecision",
       "Hlt2IncPhiDecision",
       #"Hlt2CharmHadD02HHHHWideMassDecision",
       #"Hlt2CharmHadD02HHKsLLDecision",
       "Hlt2B2HHLTUnbiasedDecision",
       #"Hlt2Dst2PiD02PiPiDecision",
       #"Hlt2CharmHadD02HH_D02PiPiDecision",
       #"Hlt2Dst2PiD02MuMuDecision",
       #"Hlt2CharmHadD02HH_D02PiPiWideMassDecision",
       #"Hlt2Dst2PiD02KMuDecision",
       #"Hlt2CharmHadD02HH_D02KKDecision",                                  
       ]
   MCTruth=toolinstance.addTupleTool("TupleToolMCTruth")
   MCTruth.addTupleTool("MCTupleToolHierarchy")
   
   from Configurables import TupleToolConeIsolation
   toolinstance.Bs.addTupleTool("TupleToolConeIsolation")
   toolinstance.Bs.TupleToolConeIsolation.FillAsymmetry = True
   toolinstance.Bs.TupleToolConeIsolation.FillDeltas = True
   toolinstance.Bs.TupleToolConeIsolation.FillComponents = True
   toolinstance.Bs.TupleToolConeIsolation.MinConeSize = 0.5
   toolinstance.Bs.TupleToolConeIsolation.SizeStep    = 0.5
   toolinstance.Bs.TupleToolConeIsolation.MaxConeSize = 2.0
   
   
   from Configurables import TupleToolConeIsolation
   toolinstance.kaon_m.addTupleTool("TupleToolConeIsolation")
   toolinstance.kaon_m.TupleToolConeIsolation.FillAsymmetry = True
   toolinstance.kaon_m.TupleToolConeIsolation.FillDeltas = True
   toolinstance.kaon_m.TupleToolConeIsolation.FillComponents = True
   toolinstance.kaon_m.TupleToolConeIsolation.MaxConeSize = 1.0
   
   #MCTruth.addTupleTool("MCTupleToolInteractions")
   MCTruth.addTupleTool("MCTupleToolReconstructed")
   toolinstance.ToolList +=  [
       "TupleToolPropertime"
       , "TupleToolRecoStats"
       , "TupleToolSLTools"
       , "TupleToolTrackPosition"
       , "TupleToolRICHPid"
       , "TupleToolRecoStats"
       , "TupleToolBs2Kmunu_velotracks"
       , "TupleToolBs2Kmunu_UpStream"
       , "TupleToolMuonPid"
       , "TupleToolGeneration"
       ]



KTuple = DecayTreeTuple("Bs2KmuNuTuple")
KTuple.Inputs = [Bs2KMuNuInputLocation_MC]
KTuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
KTuple.addBranches ( {
    "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K-  mu+]CC" 
    } )
DsTuple = DecayTreeTuple("Bs2DsMuNuTuple")
DsTuple.Inputs = [ SeqBs2DsMuNu.outputLocation() ]
DsTuple.Decay    = "[ [B~0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu-]CC"
DsTuple.addBranches ( {
    "muon_p"   : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu-]CC"
    ,"kaon_m"  : "[ [B~0]cc ->  (D+ -> ^K-  K+  pi+ )  mu-]CC"
    ,"pi_p"    : "[ [B~0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu-]CC"
    ,"kaon_p"  : "[ [B~0]cc ->  (D+ ->  K- ^K+  pi+ )  mu-]CC"
    ,"Ds"      : "[ [B~0]cc -> ^(D+ ->  K-  K+  pi+ )  mu-]CC"
    ,"Bs"      : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ )  mu-]CC"
    } )

configTupleTool(KTuple)
configTupleTool(DsTuple)

TupleSeq.Members += [ KTuple,DsTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

seqBsKmunu_gaudi.Members       +=  [TupleSeq]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
######################################################################
#
# Event Tuple
#
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
#
configIso()
from Configurables import DaVinci
#DaVinci().UserAlgorithms += [etuple, mysel , DsTuple]
DaVinci().UserAlgorithms += [ etuple , seqBsKmunu_gaudi ]
#DaVinci().UserAlgorithms += [ etuple , SeqBs2KmuNu.sequence() , LooseTuple]
DaVinci().MainOptions  = "" 
DaVinci().EvtMax       = 10000
DaVinci().PrintFreq    = 25000
DaVinci().DataType     = Year
# data:
#from Configurables import CondDB 
#CondDB(UseOracle = True) 
#data 2011:
#DaVinci().DDDBtag = 'head-20110914'
#DaVinci().CondDBtag = 'head-20111111' #'sim-20100831-vc-md100'
# data 2012:
DaVinci().DDDBtag       =  "dddb-20120831" # MC12
DaVinci().CondDBtag     =  "sim-20121025-vc-mu100" # MC12

#from Configurables import CondDB
#CondDB().UseLatestTags = ["2012"]
#DaVinci().Lumi=True
# MC
DaVinci().Simulation = True
#DaVinci().DDDBtag   = "Sim08-20130503-1"
#DaVinci().CondDBtag = "Sim08-20130503-1-vc-mu100"
DaVinci().Lumi = False        


#######################################################################
#
DaVinci().TupleFile = "DTT_Bs2DsMuNu.root"
