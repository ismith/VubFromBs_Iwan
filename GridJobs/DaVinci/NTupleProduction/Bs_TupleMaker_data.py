### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
Year = "2011"
#######################################################################
from Gaudi.Configuration import *
#######################################################################
#
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from PhysSelPython.Wrappers import (DataOnDemand,
                                    Selection,
                                    SelectionSequence)
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco , TupleToolBs2Kmunu , TupleToolBs2Kmunu_UpStream , TupleToolBs2Kmunu_velotracks , LoKi__Hybrid__EvtTupleTool

from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo 

# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ... 
# MAKE loose selection :
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle =
                                    'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ]) 

# Add a Loose stripping to MC samples :
Bs2KMuNuInputLocation   =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles"
Bs2KMuNuSSInputLocation =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KSSLine/Particles"
Bs2DsMuNuInputLocation  =  "/Event/Semileptonic/Phys/b2DsMuXB2DMuNuXLine/Particles"
#############################################################
# DecayTreeTuple 
#
seqBsKmunu_gaudi = GaudiSequencer('seqBsKmunu_gaudi')
TupleSeq   = GaudiSequencer('TupleSeq')

tuple = DecayTreeTuple("Bs2KmuNuTuple")
tuple.Inputs = [Bs2KMuNuInputLocation]
tuple.ToolList +=  [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolTrackInfo"
    , "TupleToolBs2Kmunu"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]

tuple.ReFitPVs = True
tuple.OutputLevel = 6

tuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
tuple.Branches = {
    "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K-  mu+]CC" 
     }
tuple.addTool(TupleToolDecay, name="Bs")
tuple.addTool(TupleToolDecay, name="kaon_m")
tuple.addTool(TupleToolDecay, name="muon_p")

LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
LoKiVariables.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    }

LoKiVariables_K = tuple.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = tuple.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
tuple.Bs.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
#TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
tuple.Bs.TupleToolTISTOS.VerboseL0 = True
tuple.Bs.TupleToolTISTOS.VerboseHlt1 = True
tuple.Bs.TupleToolTISTOS.VerboseHlt2 = True             
tuple.Bs.TupleToolTISTOS.TriggerList=[
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0HadronDecision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackAllL0Decision",
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2TopoMu3BodyBBDTDecision",
    "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2MuTrackDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonLowPTDecision",
    #"Hlt2TFBc2JpsiMuXDecision",
    #"Hlt2TFBc2JpsiMuXSignalDecision",
    "Hlt2Topo2BodySimpleDecision",
    #"Hlt2B2HHPi0_MergedDecision",
    "Hlt2Topo3BodySimpleDecision",
    #"Hlt2CharmHadD2HHHDecision",
    "Hlt2Topo4BodySimpleDecision",
    #"Hlt2CharmHadD2HHHWideMassDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2DiMuonJPsiHighPTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2DiMuonBDecision",
    "Hlt2Topo4BodyBBDTDecision",
    "Hlt2DiMuonZDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2DiMuonDetachedHeavyDecision",
    "Hlt2DiMuonDetachedJPsiDecision",
    "Hlt2TriMuonDetachedDecision",
    "Hlt2TopoE3BodyBBDTDecision",
    "Hlt2TriMuonTauDecision",
    "Hlt2TopoE4BodyBBDTDecision",
    #"Hlt2CharmHadD02HHHHDecision",
    "Hlt2IncPhiDecision",
    #"Hlt2CharmHadD02HHHHWideMassDecision",
    #"Hlt2CharmHadD02HHKsLLDecision",
    "Hlt2B2HHLTUnbiasedDecision"
    #"Hlt2Dst2PiD02PiPiDecision",
    #"Hlt2CharmHadD02HH_D02PiPiDecision",
    #"Hlt2Dst2PiD02MuMuDecision",
    #"Hlt2CharmHadD02HH_D02PiPiWideMassDecision",
    #"Hlt2Dst2PiD02KMuDecision",
    #"Hlt2CharmHadD02HH_D02KKDecision",                                  
    ]

from Configurables import TupleToolConeIsolation
tuple.Bs.addTupleTool("TupleToolConeIsolation")
tuple.Bs.TupleToolConeIsolation.FillAsymmetry = True
tuple.Bs.TupleToolConeIsolation.FillDeltas = True
tuple.Bs.TupleToolConeIsolation.FillComponents = True
tuple.Bs.TupleToolConeIsolation.MinConeSize = 0.5
tuple.Bs.TupleToolConeIsolation.SizeStep    = 0.5
tuple.Bs.TupleToolConeIsolation.MaxConeSize = 2.0

from Configurables import TupleToolConeIsolation
tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry = True
tuple.kaon_m.TupleToolConeIsolation.FillDeltas = True
tuple.kaon_m.TupleToolConeIsolation.FillComponents = True
tuple.kaon_m.TupleToolConeIsolation.MinConeSize = 0.5
tuple.kaon_m.TupleToolConeIsolation.SizeStep    = 0.5
tuple.kaon_m.TupleToolConeIsolation.MaxConeSize = 2.0


tuple.ToolList +=  [
      "TupleToolSLTools"
    #, "TupleToolTrackPosition"
    #, "TupleToolRICHPid"
    #, "TupleToolRecoStats"
    , "TupleToolBs2Kmunu_velotracks"
    , "TupleToolBs2Kmunu_UpStream"
    #, "TupleToolMuonPid"
    ]

TupleSeq.Members += [ tuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

KmuNuSSTuple = tuple.clone("KmuNuSSTuple")
KmuNuSSTuple.Inputs = [Bs2KMuNuSSInputLocation]
KmuNuSSTuple.Decay    = "[B_s0 -> ^K+ ^mu+]CC"
KmuNuSSTuple.Branches = {
     "muon_p"  : "[B_s0 ->  K+ ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K+  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K+  mu+]CC" 
     }

TupleSeq.Members += [KmuNuSSTuple]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False


# DsMuNu tuple is quite large we apply a mass cut around the Ds to reduce it :
# Filter the tuple :

Bs_particles = DataOnDemand( Location = '/Event/Semileptonic/Phys/b2DsMuXB2DMuNuXLine/Particles' )
Bs_particles_filter = FilterDesktop("Bs_particles_filter",
                                    Code = "MAXTREE('D+'==ABSID,M)>1910"
                                    )
# "INTREE( (ABSID=='D+') & (MM >1910) )" )
Bs_particles_sel  = Selection("Bs_particles_sel",
                              Algorithm = Bs_particles_filter,
                              RequiredSelections = [ Bs_particles ])
SeqBs2DsMuNu      = SelectionSequence('SeqBs2DsMuNu', TopSelection = Bs_particles_sel)

seqBsKmunu_gaudi.Members       +=  [SeqBs2DsMuNu.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False

DsMuNuTuple          =  tuple.clone("Bs2DsMuNuTuple")
DsMuNuTuple.Inputs   =  [ SeqBs2DsMuNu.outputLocation() ] 
#DsMuNuTuple.Inputs   =  [ '/Event/Semileptonic/Phys/b2DsMuXB2DMuNuXLine/Particles' ] 
DsMuNuTuple.Decay    = "[ [B~0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu-]CC"
DsMuNuTuple.Branches = {
    "muon_p"   : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu-]CC"
    ,"kaon_m"  : "[ [B~0]cc ->  (D+ -> ^K-  K+  pi+ )  mu-]CC"
    ,"pi_p"    : "[ [B~0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu-]CC"
    ,"kaon_p"  : "[ [B~0]cc ->  (D+ ->  K- ^K+  pi+ )  mu-]CC"
    ,"Ds"      : "[ [B~0]cc -> ^(D+ ->  K-  K+  pi+ )  mu-]CC"
    ,"Bs"      : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ )  mu-]CC"
    }


TupleSeq.Members        += [ DsMuNuTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False


DsMuNuSSTuple          =  tuple.clone("Bs2DsMuNuSSTuple")
DsMuNuSSTuple.Inputs   =  [ SeqBs2DsMuNu.outputLocation() ] 
DsMuNuSSTuple.Decay    = "[ [B0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu+]CC"
DsMuNuSSTuple.Branches = {
    "muon_p"   : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu+]CC"
    ,"kaon_m"  : "[ [B0]cc ->  (D+ -> ^K-  K+  pi+ )  mu+]CC"
    ,"pi_p"    : "[ [B0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu+]CC"
    ,"K_p"     : "[ [B0]cc ->  (D+ ->  K- ^K+  pi+ )  mu+]CC"
    ,"Ds"      : "[ [B0]cc -> ^(D+ ->  K-  K+  pi+ )  mu+]CC"
    ,"Bs"      : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ )  mu+]CC"
    }

TupleSeq.Members        += [ DsMuNuSSTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

######################################################################
seqBsKmunu_gaudi.Members       +=  [TupleSeq]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
######################################################################
#
# Event Tuple
#
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
#
configIso()
from Configurables import DaVinci
#DaVinci().UserAlgorithms += [etuple, mysel , tuple]
DaVinci().UserAlgorithms += [ seqBsKmunu_gaudi ]
#DaVinci().UserAlgorithms += [ etuple , SeqBs2KmuNu.sequence() , LooseTuple]
DaVinci().MainOptions  = "" 
DaVinci().EvtMax       = -1
DaVinci().PrintFreq    = 25000
DaVinci().DataType     = Year
# data:
from Configurables import CondDB
CondDB().UseLatestTags = [Year]
DaVinci().Lumi=True
# MC
DaVinci().Simulation = False
#DaVinci().DDDBtag   = "Sim08-20130503-1"
#DaVinci().CondDBtag = "Sim08-20130503-1-vc-mu100"
#DaVinci().Lumi = False        


#######################################################################
#
DaVinci().TupleFile = "DTT_data.root"

importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KmuNu_data_2012.py")
