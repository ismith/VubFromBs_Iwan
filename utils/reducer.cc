#include "reducer.h"
#include <iostream>
#include "TFile.h"
#include "TEntryList.h"
#include "TTree.h"
#include <iostream>
#include <algorithm>
#include <TTreeFormula.h>
#include <TLorentzVector.h>
#include <cstddef>

#ifdef USE_FF_WEIGHTING
#warning "this needs MCTupleToolSemileptonic in the tool list"
#include "FormFactorsRew.h"
#include "dtt.h"
#endif

// =====================================================
// -- constructor
// =====================================================
reducer::reducer() {

  std::cout << "Welcome to reducer v 0.1" << std::endl;

  m_filename    = "";
  m_outFilename = "";
  m_treename    = "";
  m_convertToFloat = false;

}
// =====================================================
//-- destructor
// =====================================================
reducer::~reducer(){}

// =====================================================
//-- the main method: reduce a tree
// =====================================================
void reducer::reduceTree(){

  if( m_filename  == "" || m_outFilename == "" || m_treename  == "" ){
    std::cout << "You need to define the input filename / treename / output filename" << std::endl;
    return;
  }

  if( m_dVals.empty() &&  m_iVals.empty() && m_bVals.empty()){
    std::cout << "You need to set variables for the new tree" << std::endl;
    return;
  }

  std::cout << " ============================= " << std::endl;
  std::cout << " Input filename:      " << m_filename << std::endl;
  std::cout << " treename:            " << m_treename << std::endl;
  std::cout << " Output filename:     " << m_outFilename << std::endl;
  std::cout << " Conversion to float: " << m_convertToFloat << std::endl;
  std::cout << std::endl;
  std::cout << " The variables to copy are: " << std::endl;
  for( std::string str : m_dVals ) std::cout << " " << str << std::endl;
  for( std::string str : m_iVals ) std::cout << " " << str << std::endl;
  for( std::string str : m_bVals ) std::cout << " " << str << std::endl;
  std::cout << " The cuts are: " << std::endl;
  std::cout << " " << m_cuts << std::endl;
  std::cout << " ============================= " << std::endl;


  TFile* file = TFile::Open( m_filename.c_str() );
  if( !file ){
    std::cout << "file " << m_filename << " not found" << std::endl;
    return;
  }

  TTree* tree = (TTree*)file->Get( m_treename.c_str() );
  if( !tree ){
    std::cout << "tree " << m_treename << " not found" << std::endl;
    return;
  }


  /// https://root.cern.ch/phpBB3/viewtopic.php?f=3&t=21165&sid=003e047c399ecb5cb8a303266c0ede18
  TEntryList *entryList = new TEntryList("mysel","mysel",tree);
  tree->Draw(">>mysel",m_cuts,"entrylist");
  tree->SetEntryList(entryList);


  tree->SetBranchStatus("*",1);

  double doubleVars[m_dVals.size()];
  float floatVars[m_dVals.size()];
  int intVars[m_iVals.size()];
  bool boolVars[m_bVals.size()];
  double* doubleVarsAddr[m_dVals.size()];
  /// compiler was not happy with emplace_back for non-pointer TTreeFormula (http://stackoverflow.com/questions/10488348/add-to-vector-without-copying-struct-data/10488436#10488436)
  /// so i don't use std::vector<TTreeFormula> for now, and express my grudge at the end when cleaning up the heap
  std::vector<TTreeFormula*> m_formulas;
  std::vector<float> m_formVars(m_form.size(),0.f);
  for (size_t i = 0 ; i < m_form.size() ; ++i) {
    m_formulas.emplace_back(new TTreeFormula(m_form[i].first.c_str(),m_form[i].second.c_str(),tree));
  }

  std::set<TBranch*> m_branches;
  /// https://root.cern.ch/phpBB3/viewtopic.php?f=3&t=21093&p=91702&sid=ee27773067273159ee449f213433b84b#p91702
  for (auto v : m_dVals) m_branches.insert(tree->GetBranch(v.c_str()));
  for (auto v : m_iVals) m_branches.insert(tree->GetBranch(v.c_str()));
  for (auto v : m_bVals) m_branches.insert(tree->GetBranch(v.c_str()));
  for (auto f : m_formulas) {
    for (size_t v = 0 ; v < f->GetNcodes() ; ++v) {
      m_branches.insert(f->GetLeaf(v)->GetBranch());
    }
  }


//  float* floatVarsAddr[m_dVals.size()];
//  int* intVarsAddr[m_iVals.size()];
//  bool* boolVarsAddr[m_bVals.size()];
//  for (size_t i = 0 ; i < m_dVals.size() ; ++i) doubleVarsAddr[i] = &doubleVars[i];
//  for (size_t i = 0 ; i < m_dVals.size() ; ++i) floatVarsAddr[i] = &floatVars[i];
//  for (size_t i = 0 ; i < m_iVals.size() ; ++i) intVarsAddr[i] = &intVars[i];
//  for (size_t i = 0 ; i < m_bVals.size() ; ++i) boolVarsAddr[i] = &boolVars[i];

//  if( m_dVals.size() > 50 ){
//    std::cout << "Too many double variables. Adapt the reducer class" << std::endl;
//    return;
//  }
//  if( m_iVals.size() > 50 ){
//    std::cout << "Too many integer variables. Adapt the reducer class" << std::endl;
//    return;
//  }
//  if( m_bVals.size() > 50 ){
//    std::cout << "Too many boolean variables. Adapt the reducer class" << std::endl;
//    return;
//  }

  for( unsigned int i = 0; i < m_dVals.size(); ++i ) tree->SetBranchAddress(m_dVals[i].c_str(), &doubleVars[i]);  // most of these will get overwritten by "dtt"
  for( unsigned int i = 0; i < m_iVals.size(); ++i ) tree->SetBranchAddress(m_iVals[i].c_str(), &intVars[i]);     // most of these will get overwritten by "dtt"
  for( unsigned int i = 0; i < m_bVals.size(); ++i ) tree->SetBranchAddress(m_bVals[i].c_str(), &boolVars[i]);    // most of these will get overwritten by "dtt"


#ifdef USE_FF_WEIGHTING
  double w;
  FormFactorsRew rew;
  rew.SetCurrentModel(1.16,1.37,0.845,0.921,"HQET2");//https://svnweb.cern.ch/trac/lhcb/browser/DBASE/tags/Gen/DecFiles/v27r55/dkfiles/Bs_Dsmunu=cocktail,hqet2,DsmuInAcc.dec
  rew.SetNewModel(1.16,0.921,1.37,0.845,"HQET2");

  /// If you feel like knowing a better name for this variable,
  /// feel free to rename it.
  dtt betternamedesired(tree);

  m_branches.insert(betternamedesired.b_Bs_DsMuNu_TruthMatched);
  m_branches.insert(betternamedesired.b_muon_p_TRUEID);
  m_branches.insert(betternamedesired.b_Ds_TRUEID);
  m_branches.insert(betternamedesired.b_Ds_MC_MOTHER_ID);
  m_branches.insert(betternamedesired.b_Bs_Nu_TRUEID);
  m_branches.insert(betternamedesired.b_Bs_TRUEP_X);
  m_branches.insert(betternamedesired.b_Bs_TRUEP_Y);
  m_branches.insert(betternamedesired.b_Bs_TRUEP_Z);
  m_branches.insert(betternamedesired.b_Bs_TRUEP_E);
  m_branches.insert(betternamedesired.b_Bs_Nu_TRUEP_X);
  m_branches.insert(betternamedesired.b_Bs_Nu_TRUEP_Y);
  m_branches.insert(betternamedesired.b_Bs_Nu_TRUEP_Z);
  m_branches.insert(betternamedesired.b_Bs_Nu_TRUEP_E);
  m_branches.insert(betternamedesired.b_muon_p_TRUEP_X);
  m_branches.insert(betternamedesired.b_muon_p_TRUEP_Y);
  m_branches.insert(betternamedesired.b_muon_p_TRUEP_Z);
  m_branches.insert(betternamedesired.b_muon_p_TRUEP_E);
  m_branches.insert(betternamedesired.b_Ds_TRUEP_X);
  m_branches.insert(betternamedesired.b_Ds_TRUEP_Y);
  m_branches.insert(betternamedesired.b_Ds_TRUEP_Z);
  m_branches.insert(betternamedesired.b_Ds_TRUEP_E);
#endif
  for (size_t i = 0 ; i < m_dVals.size() ; ++i) doubleVarsAddr[i] = (double*)tree->GetBranch(m_dVals[i].c_str())->GetAddress();

  TFile* rFile = new TFile( m_outFilename.c_str() ,"RECREATE");
  TTree* rTree2 = new TTree("reducedTree","reducedTree");
#ifdef USE_FF_WEIGHTING
  rTree2->Branch("FFweight",&w,"FFweight/D");
#endif

  // -- do some string replace
  for( std::pair<std::string, std::string> replace : m_stringReplace ) std::cout << " Will replace *" << replace.first << "* with *" << replace.second << "*" << std::endl;
  std::cout << " ============================= " << std::endl;

  for( std::string& bla : m_dVals ) {
    for( std::pair<std::string, std::string> replace : m_stringReplace ){
      if( bla.find(replace.first) == std::string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }
  for( std::string& bla : m_iVals ) {
    for( std::pair<std::string, std::string> replace : m_stringReplace ){
      if( bla.find(replace.first) == std::string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }
  for( std::string& bla : m_bVals ) {
    for( std::pair<std::string, std::string> replace : m_stringReplace ){
      if( bla.find(replace.first) == std::string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }

  // ---------------------------

  if( !m_convertToFloat ){
    for( unsigned int i = 0; i < m_dVals.size(); ++i ) rTree2->Branch(m_dVals[i].c_str(), doubleVarsAddr[i]                                , (m_dVals[i]+"/D").c_str() );
  }else{
    for( unsigned int i = 0; i < m_dVals.size(); ++i ) rTree2->Branch(m_dVals[i].c_str(), &floatVars[i]                                    , (m_dVals[i]+"/F").c_str() );
  }
  for( unsigned int i = 0; i < m_iVals.size(); ++i )   rTree2->Branch(m_iVals[i].c_str(), tree->GetBranch(m_iVals[i].c_str())->GetAddress(), (m_iVals[i]+"/I").c_str() );
  for( unsigned int i = 0; i < m_bVals.size(); ++i )   rTree2->Branch(m_bVals[i].c_str(), tree->GetBranch(m_bVals[i].c_str())->GetAddress(), (m_bVals[i]+"/O").c_str() );
  
  int i_event = 0; rTree2->Branch("EventNumber", &i_event, "EventNumber/I"); // Add counter to NTuple
  
  for (size_t i = 0 ; i < m_form.size() ; ++i) {
    rTree2->Branch(m_form[i].first.c_str(), &(m_formVars[i]), (m_form[i].first + "/F").c_str());
  }

  tree->SetBranchStatus("*",0);
  for (auto b: m_branches) b->SetStatus(1);

  // -- finally, loop through the tree and copy the entries
  std::cout << " Looping over events and copy the entries" << std::endl;
  int percentCounter = 1;

  Long64_t entries = tree->GetEntries();

  for (Long64_t i = 0 ; ; ++i) {
    i_event = i;  
	  
    Long64_t entryNumber = tree->GetEntryNumber(i);
    if (entryNumber < 0) break;
    Long64_t localEntry = tree->LoadTree(entryNumber);
    if (localEntry < 0) break;

    const int percent = (int)(entries/100.0);

    if( i == percent*percentCounter ){
      std::cout << percentCounter << " %" << std::endl;
      percentCounter++;
    }
    for (auto b: m_branches) b->GetEntry(i);

    if(m_convertToFloat){
      for(unsigned int v = 0; v < m_dVals.size(); ++v){
        floatVars[v] = (float)(*doubleVarsAddr[v]);
      }
    }
    for (size_t v = 0 ; v < m_formulas.size() ; ++v) {
      m_formVars[v] = m_formulas[v]->EvalInstance();
    }

#ifdef USE_FF_WEIGHTING
    {
      w = 1.f;
      if (betternamedesired.Bs_DsMuNu_TruthMatched>0.5) {
        w = 1.f;
      } else if (abs(betternamedesired.muon_p_TRUEID)!=13) {
        w = 1.f;
      } else if (abs(betternamedesired.Ds_TRUEID)!=431) {
        w = 1.f;
      } else if (abs(betternamedesired.Ds_MC_MOTHER_ID)!=433) {
        w = 1.f;
      } else {
        ///if (abs(betternamedesired.B_Nu_TRUEID)!=14) continue; /// ???

        TLorentzVector BLab(betternamedesired.Bs_TRUEP_X,betternamedesired.Bs_TRUEP_Y,betternamedesired.Bs_TRUEP_Z,betternamedesired.Bs_TRUEP_E);
        TLorentzVector NuLab(betternamedesired.Bs_Nu_TRUEP_X,betternamedesired.Bs_Nu_TRUEP_Y,betternamedesired.Bs_Nu_TRUEP_Z,betternamedesired.Bs_Nu_TRUEP_E); /// FIXME!!!
        TLorentzVector MuLab(betternamedesired.muon_p_TRUEP_X,betternamedesired.muon_p_TRUEP_Y,betternamedesired.muon_p_TRUEP_Z,betternamedesired.muon_p_TRUEP_E);
        TLorentzVector DLab(betternamedesired.Ds_TRUEP_X,betternamedesired.Ds_TRUEP_Y,betternamedesired.Ds_TRUEP_Z,betternamedesired.Ds_TRUEP_E);
        TLorentzVector DStarLab = BLab - NuLab - MuLab;

        w = rew.GetWeight(BLab,MuLab,DStarLab,DLab);

      }
    }
#endif

    rTree2->Fill();

  }

  rFile->WriteTObject(rTree2);
  rFile->Close();

  delete rFile;
  for (auto i_dont_like_to_clean_up : m_formulas) {
    delete i_dont_like_to_clean_up;
    i_dont_like_to_clean_up = nullptr;
  }
}
// =====================================================
// -- copy everything with cuts
// =====================================================
void reducer::copyTree(){

  if( m_filename  == "" || m_outFilename == "" || m_treename  == "" ){
    std::cout << "You need to define the input filename / treename / output filename" << std::endl;
    return;
  }

  std::cout << " ============================= " << std::endl;
  std::cout << " Input filename:  " << m_filename << std::endl;
  std::cout << " treename:        " << m_treename << std::endl;
  std::cout << " Output filename: " << m_outFilename << std::endl;
  std::cout << " Will copy all variables " << std::endl;
  std::cout << " ============================= " << std::endl;
  std::cout << " Cuts: " << m_cuts << std::endl;
  std::cout << " ============================= " << std::endl;

  TFile* file = TFile::Open( m_filename.c_str() );
  if( !file ){
    std::cout << "file " << m_filename << " not found" << std::endl;
    return;
  }

  TTree* tree = (TTree*)file->Get( m_treename.c_str() );
  if( !tree ){
    std::cout << "tree " << m_treename << " not found" << std::endl;
    return;
  }


  TFile* outfile = new TFile(m_outFilename.c_str(),"RECREATE");
  TTree* rTree1 = tree->CopyTree( m_cuts );
  rTree1->SetName( tree->GetName() );
  outfile->Write();



}
