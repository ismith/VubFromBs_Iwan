/*
 * DalitzRegion.h
 *
 *  Created on: Mar 6, 2015
 *      Author: ldufour
 */

#include <iostream>

#ifndef SRC_SELECTION_REFLECTIONVETO_DSDPLSHAPE_DALITZREGION_H_
#define SRC_SELECTION_REFLECTIONVETO_DSDPLSHAPE_DALITZREGION_H_

#include "stddef.h"

#include <vector>
#include <string>
#include <iostream>

class DalitzRegion {
public:
  static const DalitzRegion *PhiPi;
  static const DalitzRegion *KStarK;
  static const DalitzRegion *NR;
  static const DalitzRegion *KStarKNR;

  static const DalitzRegion *getById(const unsigned int id) {
    switch (id) {
    case 0:
      return PhiPi;
    case 1:
      return KStarK;
    case 2:
      return NR;
    case 3:
      return KStarKNR;
    default:
      std::cout << "Could not find Dalitz region " << id;
      exit(1);
    }
  }

  static void setAll(std::vector<const DalitzRegion *> &dalitzRegionMap) {
    dalitzRegionMap.push_back(PhiPi);
    dalitzRegionMap.push_back(KStarK);
    dalitzRegionMap.push_back(NR);
  }

  const unsigned int id;
  const std::string readibleString;
  const std::string tupleName;

  DalitzRegion(const unsigned int id, const std::string readibleString,
               const std::string tupleName);

  virtual ~DalitzRegion();

  const std::string toString() const;
  const std::string getTupleName() const;
  const unsigned int getId() const;

  bool operator==(const DalitzRegion &other) const {
    return other.getId() == this->getId();
  }
};

#endif /* SRC_SELECTION_REFLECTIONVETO_DSDPLSHAPE_DALITZREGION_H_ */
