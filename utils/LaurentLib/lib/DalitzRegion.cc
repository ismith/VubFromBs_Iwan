/*
 * DalitzRegion.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: ldufour
 */

#include "DalitzRegion.h"

DalitzRegion::DalitzRegion(const unsigned int givenId,
                           const std::string givenReadibleString,
                           const std::string givenTupleName)
    : id(givenId), readibleString(givenReadibleString),
      tupleName(givenTupleName) {}

DalitzRegion::~DalitzRegion() {}

const std::string DalitzRegion::toString() const { return readibleString; }

const std::string DalitzRegion::getTupleName() const { return tupleName; }

const unsigned int DalitzRegion::getId() const { return id; }

const DalitzRegion *DalitzRegion::PhiPi = new DalitzRegion(0, "PhiPi", "PhiPi");
const DalitzRegion *DalitzRegion::KStarK =
    new DalitzRegion(1, "KStarK", "KKPi");
const DalitzRegion *DalitzRegion::NR = new DalitzRegion(2, "NR", "KKPi");
const DalitzRegion *DalitzRegion::KStarKNR =
    new DalitzRegion(3, "KStarKNR", "KKPi");
