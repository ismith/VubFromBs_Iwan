#ifndef CUTLIBRARY_H_1
#define CUTLIBRARY_H_ 1

#include <memory>
#include <map>
#include <sstream>
#include <string>

#include "MassRegister.h"
#include "ParticleDefinitions.h"

typedef std::map<ForPaul::ParticleDefinitions::DsMuNuDecayParticle, float>
    MassHypothesisSet;

namespace ForPaul {
/**
 * Responsible for generating a TFormula which can direclty be used for a given
 *tuple.
 *
 * Ideally you would just run a TSelector and use a more direct C++
 *implementation,
 * but that would not be so portable between projects.
 */
class CutLibrary {
private:
  const ParticleDefinitions particleDefinitions;

  std::string alias_PE;
  std::string alias_PX;
  std::string alias_PY;
  std::string alias_PZ;

public:
  CutLibrary(const ParticleDefinitions &pdef) : particleDefinitions(pdef) {
    alias_PE = "PE";
    alias_PX = "PX";
    alias_PY = "PY";
    alias_PZ = "PZ";
  }

  /**
   * Quick method for computing a TFormula for sqrt((E^2 - p^2)) (i.e. sqrt(p_mu
   *p^mu), i.e. m).
   *
   * The input is expected to be a MassHypothesisSet. Here only particles which
   *are set
   * as keys are taken into account as a pair. A key with as mass value '0' will
   * be considered as the original mass (i.e. Pion -> Pion mass), by using the
   *tuple
   * _PE branch.
   *
   */
  const std::string getStringForMassHypothesisSet(MassHypothesisSet massHypo) {
    std::stringstream hypothesisStream;
    std::string directions[3] = {alias_PX, alias_PY, alias_PZ};

    // energies
    hypothesisStream << "sqrt(";
    hypothesisStream << "(";
    bool already_one_in = false;
    for (auto massHypoIt : massHypo) {
      const std::string internalParticleName =
          this->particleDefinitions.getBranchPrefix((massHypoIt).first);
      if (already_one_in) {
        hypothesisStream << " + ";
      }
      already_one_in = true;

      if ((massHypoIt).second == 0)
        hypothesisStream << internalParticleName + "_" + alias_PE + " ";
      else {
        hypothesisStream << "sqrt(";
        hypothesisStream << (massHypoIt).second << "**2";
        hypothesisStream << " + ";
        hypothesisStream << "" + internalParticleName + "_P**2)";
      }
    }

    hypothesisStream << ")**2 - (";

    // momenta
    for (int directionId = 0; directionId < 3; directionId++) {
      hypothesisStream << "(";
      already_one_in = false;
      for (auto massHypoItx : massHypo) {
        const std::string internalParticleName =
            this->particleDefinitions.getBranchPrefix((massHypoItx).first);
        if (already_one_in) {
          hypothesisStream << " + ";
        }
        already_one_in = true;

        hypothesisStream << internalParticleName + "_" +
                                directions[directionId];
      }

      hypothesisStream << ")**2";

      if (directionId < 2)
        hypothesisStream << " + ";
    }
    hypothesisStream << "))";

    return hypothesisStream.str();
  }

  const std::string getLcMassK2() {
    MassHypothesisSet K2ProtonHypoSet;
    K2ProtonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::SS_KAON] =
        MassRegister::PROTON;
    K2ProtonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::OS_KAON] = 0.;
    K2ProtonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::PION] = 0.;

    return getStringForMassHypothesisSet(K2ProtonHypoSet);
  }

  const std::string getJPsiMassK2() {
    MassHypothesisSet K2MuonHypoSet;
    K2MuonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::SS_KAON] =
        MassRegister::MUON;
    K2MuonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::MUON] = 0.;

    return getStringForMassHypothesisSet(K2MuonHypoSet);
  }

  const std::string getJPsiMassPi() {
    MassHypothesisSet PiMuonHypoSet;
    PiMuonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::PION] =
        MassRegister::MUON;
    PiMuonHypoSet[ParticleDefinitions::DsMuNuDecayParticle::MUON] = 0.;

    return getStringForMassHypothesisSet(PiMuonHypoSet);
  }

  const std::string getDMassK2() {
    MassHypothesisSet KpipiHypothesisSet;
    KpipiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::SS_KAON] =
        MassRegister::PION;
    KpipiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::PION] = 0.;
    KpipiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::OS_KAON] = 0.;

    return getStringForMassHypothesisSet(KpipiHypothesisSet);
  }

  const std::string getKPiMass() {
    MassHypothesisSet KpiHypothesisSet;
    KpiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::SS_KAON] =
        MassRegister::PION;
    KpiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::OS_KAON] = 0.;

    return getStringForMassHypothesisSet(KpiHypothesisSet);
  }

  const std::string getKKMass() {
    MassHypothesisSet KpiHypothesisSet;
    KpiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::SS_KAON] = 0.;
    KpiHypothesisSet[ParticleDefinitions::DsMuNuDecayParticle::OS_KAON] = 0.;

    return getStringForMassHypothesisSet(KpiHypothesisSet);
  }

  const std::string getDeltaMass() { return "Ds_M - " + getKKMass(); }

  const std::string getD_Dalitz_piplus_Kminus_M2() {
    MassHypothesisSet kpi;
    kpi[ParticleDefinitions::DsMuNuDecayParticle::OS_KAON] = 0;
    kpi[ParticleDefinitions::DsMuNuDecayParticle::PION] = 0;

    return getStringForMassHypothesisSet(kpi);
  }

  const std::string getJPsiCut(unsigned int tupleId) {
    std::string cutString =
        "!(" + getJPsiMassPi() + " > 3042 && " + getJPsiMassPi() +
        " < 3147 && " +
        this->particleDefinitions.getBranchPrefix(
            ParticleDefinitions::DsMuNuDecayParticle::PION) +
        "_isMuon)";

    if (tupleId != 0)
      cutString += " && !(" + getJPsiMassK2() + " > 3042 && " +
                   getJPsiMassK2() + " < 3147 && " +
                   this->particleDefinitions.getBranchPrefix(
                       ParticleDefinitions::DsMuNuDecayParticle::SS_KAON) +
                   "_isMuon)";

    return cutString;
  }

  const std::string getDalitzCut(unsigned int tupleId) {
    /// K* and not φ
    if (tupleId == 1)
      return "(" + getD_Dalitz_piplus_Kminus_M2() + " < 985.94 && " +
             getD_Dalitz_piplus_Kminus_M2() + " > 805.94) && !(abs(" +
             getKKMass() + " - 1020) < 20)";

    /// neither K* and nor φ
    if (tupleId == 2)
      return "(!(" + getD_Dalitz_piplus_Kminus_M2() + "  < 985.94 && " +
             getD_Dalitz_piplus_Kminus_M2() + "  > 805.94) && !(" +
             getKKMass() + " > 1000 && " + getKKMass() + " < 1040))";

    /// φ
    if (tupleId == 0)
      return "(abs(" + getKKMass() + " - 1020) < 20)";

    /// not φ
    if (tupleId == 3)
      return "!(abs(" + getKKMass() + " - 1020) < 20)";

    return "(1)";
  }

  const std::string getDplusCut() {
   std::string misid = this->particleDefinitions.getBranchPrefix(
                       ParticleDefinitions::DsMuNuDecayParticle::SS_KAON);
    return "!(abs(" + getDMassK2() + " - 1870) < 20 && "+misid+"_PIDK < 15)";
  }

  const std::string getLambdaCut() {
   std::string misid = this->particleDefinitions.getBranchPrefix(
                       ParticleDefinitions::DsMuNuDecayParticle::SS_KAON);
    return "!(abs(" + getLcMassK2() +
           " - 2288) < 25 && ("+misid+"_PIDp - "+misid+"_PIDK) > -15)";
  }
};
}
#endif
