


void generateReadibleGraph(TString dalitzRegion) {
  gROOT->ProcessLine(".x lhcbStyle.C");
  gStyle->SetEndErrorSize(10);
  gStyle->SetLegendFont(132);
  gROOT->ForceStyle();

  TFile *inputFile =
      TFile::Open("results/generateAsymmetryVsDsMass_vetoes_4023_b_" +
                  dalitzRegion + ".root");

  saveDistribution(inputFile, "", dalitzRegion);
  saveDistribution(inputFile, "_JPsiVeto", dalitzRegion);
  saveDistribution(inputFile, "_JPsiVetoLcDplVeto", dalitzRegion);
  saveDistribution(inputFile, "_JPsiVetoLcDplVetoLooseKstarVeto", dalitzRegion);
  saveDistribution(inputFile, "_JPsiVetoLcDplVetoLooseKstarVetoDstarVeto",
                   dalitzRegion);
  saveDistribution(inputFile,
                   "_JPsiVetoLcDplVetoLooseKstarVetoDstarVetoProbNNVeto",
                   dalitzRegion);

  /*
  saveDistribution ( inputFile, "MC", dalitzRegion );
  saveDistribution ( inputFile, "_ReflectionCut", dalitzRegion );
  saveDistribution ( inputFile, "_ReflectionCutPartialDplPartialLc",
dalitzRegion );
//	saveDistribution ( inputFile, "_sWeighted", dalitzRegion );
  saveDistribution ( inputFile, "_ReflectionProbeCut", dalitzRegion );
  saveDistribution ( inputFile, "_ReflectionInvProbeCut", dalitzRegion );*/
}
