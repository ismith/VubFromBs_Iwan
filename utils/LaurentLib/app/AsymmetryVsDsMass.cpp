#include <TChain.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <map>
#include <TFile.h>
#include <TF1.h>

#include "DalitzRegion.h"
#include "CutLibrary.h"

void saveDistribution(TH2F *asymmetryGraph) {
  TCanvas *c1 = new TCanvas();

  TF1 *lambdaC =
      new TF1("lambdaC", "sqrt((-3961354 + 5234944*x)/(-1 + x))", -1, 1.0);
  TF1 *dplus =
      new TF1("dPlus", "sqrt((-3939780 + 3496900*x)/(-1 + x))", -1, 1.0);

  asymmetryGraph->SetTitle("");
  asymmetryGraph->SetStats(0);
  asymmetryGraph->SetXTitle("#beta");
  asymmetryGraph->SetYTitle("K^{+} K^{-} #pi^{+} mass");
  asymmetryGraph->Smooth();
  asymmetryGraph->Draw("COLZ");

  lambdaC->SetLineWidth(2);
  lambdaC->SetLineStyle(kDashed);
  lambdaC->SetLineColor(kBlack);
  lambdaC->Draw ("SAME");

  dplus->SetLineWidth(2);
  dplus->SetLineStyle(kDashDotted);
  dplus->SetLineColor(kBlack);
  dplus->Draw ("SAME");

  c1->Print("plot.pdf");
}

static int nBins = 100;

void addToSpectrum(TTree *inputTree, TH2F *asymmetryGraph,
                   std::string toDrawString, std::string cutString,
                   const Bool_t useSweights = false) {
  TCanvas *c1 = new TCanvas();

  std::string local_cutstring("(");
  local_cutstring += cutString;
  local_cutstring += ")";
  if (useSweights) {
    local_cutstring += "*yieldSignal_sw";
  }
  std::cout << "attempt to draw " << toDrawString << "  with selection " << local_cutstring << std::endl;
  inputTree->Draw((toDrawString + ">>" + asymmetryGraph->GetName()).c_str(), local_cutstring.c_str(), "COLZ");

}

void addToSpectrum(std::map<std::string, std::string> fileList,
                   TH2F *asymmetryGraph, std::string toDrawString,
                   std::string cutString, const Bool_t useSweights = false) {
  for (std::map<std::string, std::string>::iterator fileNameIt =
           fileList.begin();
       fileNameIt != fileList.end(); fileNameIt++) {
    std::cout << "Analyzing " << fileNameIt->first << std::endl;

    TFile *inputFile = TFile::Open(fileNameIt->first.c_str());
    TTree *tree = (TTree *)inputFile->Get(fileNameIt->second.c_str());

    addToSpectrum(tree, asymmetryGraph, toDrawString, cutString, useSweights);
  }
}

std::string getDrawString(const ForPaul::ParticleDefinitions pDef) {
  std::stringstream toDrawStream;
  toDrawStream
      << "Ds_M:("
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::OS_KAON)
      << "_P + "
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::PION) << "_P - "
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::SS_KAON)
      << "_P)/("
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::OS_KAON)
      << "_P + "
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::PION) << "_P + "
      << pDef.getBranchPrefix(
             ForPaul::ParticleDefinitions::DsMuNuDecayParticle::SS_KAON)
      << "_P)";
  //toDrawStream << ">>(" << nBins << ",-1., 1., " << nBins << ", 1800., 2047.)";
  return toDrawStream.str();
}

void generateAsymmetryVsDsMass(TTree *inputTree,
                               char /*magnetPolarity*/, unsigned int nBins,
                               unsigned int /*year = (2011 + 2012)*/,
                               const std::string &outputSuffix,
                               const ForPaul::ParticleDefinitions &pDef) {
  ForPaul::CutLibrary cutLibrary(pDef);

  TH2F *kaonAsymmetry =
      new TH2F((std::string("kaonAsymmetry_") + outputSuffix).c_str(),
               (std::string("kaonAsymmetry_") + outputSuffix).c_str(), nBins,
               -1., 1., nBins, 1800., 2047.);

//  TH2F *kaonAsymmetry_JPsiVeto =
//      new TH2F((std::string("kaonAsymmetry_JPsiVeto_") + outputSuffix).c_str(),
//               (std::string("kaonAsymmetry_JPsiVeto_") + outputSuffix).c_str(),
//               (Int_t)nBins, (Double_t)-1., (Double_t)1., (Int_t)nBins,
//               (Double_t)1800., (Double_t)2047.);
//  TH2F *kaonAsymmetry_JPsiVetoLcDplVeto = new TH2F(
//      (std::string("kaonAsymmetry_JPsiVetoLcDplVeto_") + outputSuffix).c_str(),
//      (std::string("kaonAsymmetry_JPsiVetoLcDplVeto_") + outputSuffix).c_str(),
//      nBins, -1., 1., nBins, 1800., 2047.);
//  TH2F *kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVeto =
//      new TH2F((std::string("kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVeto_") +
//                outputSuffix).c_str(),
//               (std::string("kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVeto_") +
//                outputSuffix).c_str(),
//               nBins, -1., 1., nBins, 1800., 2047.);
//  TH2F *kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVeto = new TH2F(
//      (std::string("kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVeto_") +
//       outputSuffix).c_str(),
//      (std::string("kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVeto_") +
//       outputSuffix).c_str(),
//      nBins, -1., 1., nBins, 1800., 2047.);
//  TH2F *kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVetoProbNNVeto =
//      new TH2F(
//          (std::string("kaonAsymmetry_"
//                       "JPsiVetoLcDplVetoLooseKstarVetoDstarVetoProbNNVeto_") +
//           outputSuffix).c_str(),
//          (std::string("kaonAsymmetry_"
//                       "JPsiVetoLcDplVetoLooseKstarVetoDstarVetoProbNNVeto_") +
//           outputSuffix).c_str(),
//          nBins, -1., 1., nBins, 1800., 2047.);
//  TH2F *kaonAsymmetry_MC =
//      new TH2F((std::string("kaonAsymmetry_MC_") + outputSuffix).c_str(),
//               (std::string("kaonAsymmetry_MC_") + outputSuffix).c_str(), nBins,
//               -1., 1., nBins, 1800., 2047.);

  /*
   TH2F* kaonAsymmetryReflCutA = (TH2F*)kaonAsymmetry->Clone (
   "kaonAsymmetry_ReflectionCut_" + outputSuffix );
   TH2F* kaonAsymmetryReflCutB = (TH2F*)kaonAsymmetry->Clone (
   "kaonAsymmetry_ReflectionCutPartialDplPartialLc_" + outputSuffix );
   TH2F* kaonAsymmetryReflCutMC = (TH2F*)kaonAsymmetry->Clone (
   "kaonAsymmetryMC_ReflectionCut_" + outputSuffix );
   TH2F* kaonAsymmetryReflProbeCut = (TH2F*)kaonAsymmetry->Clone (
   "kaonAsymmetry_ReflectionProbeCut_" + outputSuffix );
   TH2F* kaonAsymmetryReflInvProbeCut = (TH2F*)kaonAsymmetry->Clone (
   "kaonAsymmetry_ReflectionInvProbeCut_" + outputSuffix );
   */

  std::string toDrawString = getDrawString(pDef);

  std::cout << toDrawString << std::endl;

  /*
   return preCut + getKStarCutString ( tupleId ) + " && " +
   getLoosePionKaonCutString ( tupleId ) + " && " + getJPsiCut ( tupleId ) + "
   && "
   + getDeltaMassCut ();
   */
  std::string cutString = "1";
  std::string cutStringMC = "";

//  std::string cutString_JPsiVeto =
//      cutString + " && " + cutLibrary.getJPsiCut(dalitzRegion->getId());
//  std::string cutString_JPsiVetoDplVeto =
//      cutString_JPsiVeto + " && " + cutLibrary.getDplusCut();
//  std::string cutString_JPsiVetoLcDplVetoLambdaVeto =
//      cutString_JPsiVetoDplVeto + " && " + cutLibrary.getLambdaCut();

//  std::cout << "Generating graph for raw MC" << std::endl;
//  addToSpectrum(fileListMC, kaonAsymmetry_MC, toDrawString, cutStringMC);

  std::cout << "Generating graph for raw data" << std::endl;
  addToSpectrum(inputTree, kaonAsymmetry, toDrawString, cutString);

//  std::cout << "Generating graph for JPsiVeto" << std::endl;
//  addToSpectrum(fileListData, kaonAsymmetry_JPsiVeto, toDrawString,
//                cutString_JPsiVeto);
//
//  std::cout << "Generating graph for JPsiDpl " << std::endl;
//  addToSpectrum(fileListData, kaonAsymmetry_JPsiVetoLcDplVeto, toDrawString,
//                cutString_JPsiVetoDplVeto);
//
//  std::cout << "Generating graph for cutString_JPsiVetoLcDplVetoLambdaVeto "
//            << std::endl;
//  addToSpectrum(fileListData, kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVeto,
//                toDrawString, cutString_JPsiVetoLcDplVetoLambdaVeto);
  saveDistribution(kaonAsymmetry);
  TFile *outputFile =
      TFile::Open((std::string("asymmetryplot_") +
                   outputSuffix + ".root").c_str(),
                  "RECREATE");
  outputFile->WriteTObject(kaonAsymmetry);
//  kaonAsymmetry_JPsiVeto->Write();
//  kaonAsymmetry_JPsiVetoLcDplVeto->Write();
//  kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVeto->Write();
//  kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVeto->Write();
//  kaonAsymmetry_JPsiVetoLcDplVetoLooseKstarVetoDstarVetoProbNNVeto->Write();
  outputFile->Close();
}

void AsymmetryVsDsMass(const char* fname) {
  ForPaul::ParticleDefinitions particleDefinitions("kaon_m", "kaon_p", "pi_p",
                                                   "mu_p");

  TChain* ch = new TChain("reducedTree");
  ch->Add(fname);
  generateAsymmetryVsDsMass(ch, 'b', 500, (2011 + 2012), "",
                            particleDefinitions);
}

int main(int argc, char** argv) {
  if (argc<2) {
    std::cerr << "Please specify a filename..." << std::endl;
    return 666;
  }
  AsymmetryVsDsMass(argv[1]);
  return 0;
}
