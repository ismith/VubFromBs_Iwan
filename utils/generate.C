int generate() {
  TFile* orgfile = TFile::Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/DTT_MC12_Bs2DsMuNu_Cocktail_SIGNAL_13774002.root","read");
  //TTree* orgtree = (TTree*)orgfile->Get("Bs2DsMuNuTuple/DecayTree");
  TTree* orgtree = (TTree*)orgfile->Get("Bs2KMuNuTuple/DecayTree");
  orgtree->MakeClass("dtt");
  orgfile->Close();
  return 0;
}

