#ifndef REDUCER
#define REDUCER

#include <TCut.h>
#include <vector>
#include <algorithm>

class reducer  {

 public:

  reducer();
  virtual ~reducer();

  
 public:
  
  // -- the only essential method
  void reduceTree();
  // -- copy the tree with a selection
  void copyTree();
  
  
  // members
  TCut m_cuts;
  std::string m_filename;
  std::string m_outFilename;
  std::string m_treename;
  bool m_convertToFloat;
  // -- double values
  std::vector<std::string> m_dVals;
  // -- integer values
  std::vector<std::string> m_iVals;
  // -- boolean values
  std::vector<std::string> m_bVals;
  // -- do string replace
  std::vector<std::pair<std::string, std::string>> m_stringReplace;
  // -- compute TTreeFormulas
  std::vector<std::pair<std::string, std::string>> m_form;
  
};

#endif
