#include "iostream"

int main(){
	
	for( int x = 0; x < 20; x++){
		int begin = x * 1000000;
		int end = 999999 + x * 1000000;
		std::cout << "CutNtuple(\"data_2012_trimmed_norm.root\", \"EventNumber >" << begin <<  "&& EventNumber < " << end << "\", \"reducedTree\", \"_cut" << x << ".root\")" << std::endl;
	}
	
	for( int x = 0; x < 20; x++)
		std::cout << "FitSig_MC(\"/media/ismith/2.0TB_Drive/NTuples/BsKmuNu/Bs_KMuNu/data_2012_trimmed_norm_cut" << x << ".root\", \"reducedTree\", true)" << std::endl;
	
	return (0);
	
	
}
