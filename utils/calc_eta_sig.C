//simple script to add ETA variable calculated from P and PZ of particles

#include <TTree.h>
#include <TMath.h>
#include <TFile.h>

void calc_eta_sig(){

    std::string filename = "DTT_MC12_Bs2KMuNu_SIGNAL_13512010";
    std::string path     = "/home/he/kolpin/work/Vub/tuples/"; // change to respective eos/local path

    TFile *infile   = TFile::Open((path+filename+"_trimmed.root").c_str());
    TTree *t        = (TTree*)infile->Get("reducedTree");
    TFile *outfile  = new TFile((path+filename+"_trimmed_ETA.root").c_str(),"RECREATE");
    outfile->cd();
    TTree *t1       = t->CloneTree(-1);

    float Km_P, Km_PZ, mu_P, mu_PZ;
    //read in P and PZ of respective particles
    t1->SetBranchAddress("kaon_m_P",&Km_P);
    t1->SetBranchAddress("kaon_m_PZ",&Km_PZ);
    t1->SetBranchAddress("muon_p_P",&mu_P);
    t1->SetBranchAddress("muon_p_PZ",&mu_PZ);

    float tmp[5];
    //create new branches (naming to correspond to default of PIDCalib
    TBranch *b0 = t1->Branch("kaon_m_TRACK_Eta",&tmp[0],"kaon_m_TRACK_Eta/F");
    TBranch *b1 = t1->Branch("muon_p_TRACK_Eta",&tmp[1],"muon_p_TRACK_Eta/F");


    for (unsigned i; i<t1->GetEntries(); i++){
        t1->GetEntry(i);
        
        tmp[0] = 0.5*log( (Km_P + Km_PZ) / (Km_P - Km_PZ) );
        tmp[1] = 0.5*log( (mu_P + mu_PZ) / (mu_P - mu_PZ) );

        b0->Fill();
        b1->Fill();
    }
    outfile->Write(0,TObject::kOverwrite);
    outfile->Close();
}

